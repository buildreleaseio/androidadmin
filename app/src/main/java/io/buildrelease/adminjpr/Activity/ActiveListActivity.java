package io.buildrelease.adminjpr.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.ActiveListAdapter;
import io.buildrelease.adminjpr.Model.ActionPayload;
import io.buildrelease.adminjpr.Model.ActiveAccountData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import io.buildrelease.adminjpr.interfaces.SelectedIdListInterface;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class ActiveListActivity extends AppCompatActivity implements SelectedIdListInterface {

    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    List<ActiveAccountData.Datum> datumArrayListActive =new ArrayList<>();
    int pageno =1 ,pageCount = 50;
    ActiveListAdapter mAdapter ;
    boolean newData = true;
    Button btn_suspend;
    LinearLayoutManager layoutManager;
    List<String> ids =null;
    boolean isScrolled = false;
    String totalPageCount = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_list);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Active List");
        }
        connnectUI();
        getactiveListData();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ActiveListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }





    public void connnectUI()
    {
        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView_activeList);
        btn_suspend = findViewById(R.id.btn_suspend);
        btn_suspend.setVisibility(View.GONE);
         mAdapter = new ActiveListAdapter(ActiveListActivity.this,datumArrayListActive,this);
        layoutManager = new LinearLayoutManager(ActiveListActivity.this);
         recyclerView.setLayoutManager(layoutManager);
         recyclerView.setAdapter(mAdapter);
         mAdapter.notifyDataSetChanged();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolled = true;
                    loadmoreData();
                }
            }
        });

    }


    public void getactiveListData() {
        ApiRequest apiRequest = new ApiRequest(ActiveListActivity.this);
        Call<JsonObject> call = apiInterface.getActiveAccountList(UtilPref.getAuth(ActiveListActivity.this),pageno,pageCount);
        apiRequest.request(call, getactiveListDataListener);
    }

    FetchDataPaginatedListener getactiveListDataListener =new FetchDataPaginatedListener() {


        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            ActiveAccountData activeAccountData = new Gson().fromJson(data, ActiveAccountData.class);
            totalPageCount = pageCount;
            inflatePairingAdapter(activeAccountData);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(ActiveListActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(ActiveListActivity.this);
        }
    };

    public void inflatePairingAdapter(ActiveAccountData activeAccountData){

        if(newData) {
            datumArrayListActive.clear();
            datumArrayListActive.addAll(activeAccountData.getData());
            mAdapter.notifyDataSetChanged();
        }else
        {
            datumArrayListActive.addAll(activeAccountData.getData());
            mAdapter.notifyDataSetChanged();
        }
        if(activeAccountData.getData()!=null && activeAccountData.getData().size()==0)
        {
            no_data_ll.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            no_data_ll.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void showRemarkDialog(){
        try {
            final MaterialDialog.Builder builder = new MaterialDialog.Builder(ActiveListActivity.this);
            builder.autoDismiss(true);
            builder.cancelable(true);

            builder.title("Remark")
                    .inputRange(0, 255)
                    .positiveColor(getResources().getColor(R.color.colorPrimary))
                    .negativeColor(getResources().getColor(R.color.colorPrimary))
                    .input("Type your comment here", "", false, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            performBulkOperation(input.toString());
                        }
                    })
                    .positiveText("Submit")
//                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//
//                        }
//                    })
                    .negativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            builder.cancelable(true);
                        }
                    })
                    .canceledOnTouchOutside(true)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void performBulkOperation(String s) {
        try {
            ApiRequest apiRequest = new ApiRequest(ActiveListActivity.this);
            ActionPayload actionPayload = new ActionPayload();
            actionPayload.setRemark(s);
            actionPayload.setAccountIds(ids);

               Call<JsonObject>  call = apiInterface.bulkSuspend(UtilPref.getAuth(ActiveListActivity.this),actionPayload);
                apiRequest.request(call, bulkOperationListener);

        }catch (Exception e){
            e.printStackTrace();
        }
    }



    FetchDataListener bulkOperationListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {

            //1 Second Delay to wait for response
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                }
//            }, 1000);
            RequestQueueService.cancelProgressDialog();

            CommonUtils.showSuccessMessage(ActiveListActivity.this,data,"Successfully suspended");
          //  Toasty.success(ActiveListActivity.this,).show();
            reload();



        }

        public void reload()
        {
            newData = true;
            pageno = 1;
            mAdapter.clearSelectedList();
            btn_suspend.setVisibility(View.GONE);
            datumArrayListActive.clear();
            mAdapter.notifyDataSetChanged();
            getactiveListData();
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(ActiveListActivity.this,msg,"Something went wrong, Please try  sometime  later");

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(ActiveListActivity.this);
        }
    };



    @Override
    public void selectedIds(List<String> ids) {
        this.ids = ids;
        if(ids.size()>0)
        {
            btn_suspend.setVisibility(View.VISIBLE);
        }else
        {
            btn_suspend.setVisibility(View.GONE);
        }
    }


    public void suspend(View view) {
        showRemarkDialog();
    }


    public void loadmoreData() {

        //   Log.d(TAG, "onScrolled: " + isScrolled);
        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();
        // Log.d(TAG, "onScrolled: visible count : " + totalChildCurrentlyVisibleCount + " scrolled count : " + totalChildItemScrolledCount + " total count : " + totalListItemCount);

        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;

            //Log.d(TAG, "loadmoreData: total size :"+dataList.size()+" page_no : "+filterTradeHistory.getPage());
          //  if (tradeHistoryTransaction.getCount() > Integer.parseInt(filterTradeHistory.getPer_page()) * page_no) {
            if(totalPageCount!=null)
            {
                int total = Integer.parseInt(totalPageCount);
                Log.d(TAG, "loadmoreData: total"+total+" pageno "+pageno + " pagecount "+pageCount);
                if(total>pageno*pageCount) {
                    pageno += 1;
                    newData = false;
                    getactiveListData();
                }
            }
                // if (koinTransactionHistory.getCount() < per_page_count) {

                // Log.d(TAG, "onScrolled: more data to fetch currentoffset: " + currentOffet + " totaldatasize: " + totalDataSize);
                // } else {
                //   Log.d(TAG, "onScrolled: no more data to fetch");
                //}
//            } else {
//                Log.d(TAG, "onScrolled: no data to fetch");
//            }


        }

    }
}
