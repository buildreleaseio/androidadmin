package io.buildrelease.adminjpr.Activity;

import android.os.Bundle;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;


import io.buildrelease.adminjpr.Adapters.AddonListAdapter;
import io.buildrelease.adminjpr.Model.AddonResponse;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import io.buildrelease.adminjpr.interfaces.SelectedAddonInterface;
import retrofit2.Call;

public class AddonListActivity extends AppCompatActivity implements SelectedAddonInterface {


    final String TAG = getClass().getSimpleName();
    APIInterface apiInterface;
    ApiRequest apiRequest = new ApiRequest(this);
    private RecyclerView recyclerView;
    private AddonListAdapter mAdapter;
    Button btn_proceed;
    List<AddonResponse.Datum> dataList = new ArrayList<>();
    List<AddonResponse.Datum> filerList = new ArrayList<>();
    List<String> typeList = new ArrayList<>();
    SearchView searchView;
    ChipGroup chipGroup;
    String vc_no,stb_no,acc_no,brandid;
    TextView tv_stb,tv_vcnumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addon_list);
        btn_proceed = findViewById(R.id.btn_proceed);
        btn_proceed.setVisibility(View.GONE);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Addon List");
        }

        if(getIntent().getExtras()!=null)
        {
            vc_no = getIntent().getExtras().getString("vc","");
            stb_no = getIntent().getExtras().getString("stbno","");
            acc_no = getIntent().getExtras().getString("accno","");
            brandid = getIntent().getExtras().getString("brandid","");
            connectUI();
            getAddonList();
        }

    }


    public void connectUI()
    {
        chipGroup = findViewById(R.id.filter_chipgroup);
        searchView = findViewById(R.id.searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                mAdapter.getFilter().filter(s);
                return false;
            }
        });

        searchView.setIconified(false);
        searchView.clearFocus();
        tv_stb = findViewById(R.id.tv_stb);
        tv_stb.setText(stb_no);

        tv_vcnumber = findViewById(R.id.tv_vcnumber);
        tv_vcnumber.setText(vc_no);
        recyclerView =  findViewById(R.id.recyclerView);
        mAdapter = new AddonListAdapter(AddonListActivity.this,filerList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }



    public void filterList(String type)
    {

        filerList.clear();
        if(type.equalsIgnoreCase("all"))
        {

            for(AddonResponse.Datum datum : dataList)
            {
                filerList.add(datum);
            }
        }else
        {
            for(AddonResponse.Datum datum : dataList)
            {
                if(datum.getTypeLbl().equalsIgnoreCase(type)) {

                    filerList.add(datum);

                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }

    public void createTypeList()
    {
        for(AddonResponse.Datum datum : dataList)
        {
            if(!typeList.contains(datum.getTypeLbl()))
            {
                typeList.add(datum.getTypeLbl());
            }
        }

        setUpChips();
    }

    public void setUpChips()
    {

        chipGroup.setChipSpacing(20);
        chipGroup.setSingleSelection(true);
        final Chip chip1 = new Chip(AddonListActivity.this);
        chip1.setTag("all");

        chip1.setCheckable(true);
        chip1.setText("  All   ");
        chip1.setClickable(true);
        chip1.setChipBackgroundColorResource(R.color.colorPrimary);
        chip1.setTextColor(getResources().getColor(R.color.colorWhite));

        chip1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterList(view.getTag().toString());
            }
        });
        chipGroup.addView(chip1);
        for(String type: typeList)
        {
            Chip chip = new Chip(AddonListActivity.this);
            chip.setTag(type);
            chip.setCheckable(true);
            chip.setText(type);
            chip.setClickable(true);
            chip.setChipBackgroundColorResource(R.color.colorPrimary);
            chip.setTextColor(getResources().getColor(R.color.colorWhite));
            chip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    filterList(view.getTag().toString());
                }
            });
            chipGroup.addView(chip);
        }

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                AddonListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    public void getAddonList()
    {
        Call<JsonObject> call = apiInterface.getAddoList(UtilPref.getAuth(this),acc_no,brandid);
        apiRequest.request(call,fetchDataListener);
    }


    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            AddonResponse addonResponse = new Gson().fromJson(data,AddonResponse.class);
            dataList.addAll( addonResponse.getData());
            filerList.clear();
            filerList.addAll(dataList);
            createTypeList();
            mAdapter.notifyDataSetChanged();

        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(AddonListActivity.this);
        }
    };



    public void proceed(View view) {
        mAdapter.proceed(dataList,acc_no);

    }

    @Override
    public void selected(List<String> dataList) {


        if(dataList.size()>0) {
            btn_proceed.setVisibility(View.VISIBLE);
        }else
        {
            btn_proceed.setVisibility(View.GONE);
        }
    }
}
