package io.buildrelease.adminjpr.Activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.CartAdapter;
import io.buildrelease.adminjpr.Model.RenewPlanReponse;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.interfaces.SelectedRenewalInterface;

public class BillSummaryActivity extends AppCompatActivity implements SelectedRenewalInterface {

    List<RenewPlanReponse.Datum> dataList = new ArrayList<>();
    List<RenewPlanReponse.Datum> baselist = new ArrayList<>();
    List<RenewPlanReponse.Datum> addonlist = new ArrayList<>();
    List<RenewPlanReponse.Datum> alacartlist = new ArrayList<>();

    String TAG = getClass().getSimpleName();
    String ids;
    String selection;
    CardView cardbase,cardaddon,cardlarcart;

    private RecyclerView recyclerView;
    private CartAdapter mAdapter;

    TextView tv_selected,tv_payable,title;
    String totalvalue,type;
    double amount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_summary);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Bill Summary");
        }
        if(getIntent().getExtras()!=null)
        {
            selection = getIntent().getExtras().getString("selection");
            Log.d(TAG, "onCreate: "+selection);
            ids = getIntent().getExtras().getString("data");
            type = getIntent().getExtras().getString("type");

            getData();
            connectUI();
            calculateCart();
            separteList(dataList);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BillSummaryActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void separteList(List<RenewPlanReponse.Datum> mainList)
    {
        baselist.clear();
        alacartlist.clear();
        addonlist.clear();

        for(RenewPlanReponse.Datum datum : mainList) {

            Log.d(TAG, "separteList: "+datum.getTypeLbl());
            if(datum.getTypeLbl().equalsIgnoreCase("base"))
            {
                baselist.add(datum);
            }else if(datum.getTypeLbl().equalsIgnoreCase("Addon"))
            {
                addonlist.add(datum);
            }else
            {
                alacartlist.add(datum);
            }

        }



        if(baselist.size()>0)
        {

            updateList(baselist,"base");

        }else
        {
            cardbase.setVisibility(View.GONE);
        }

        if(addonlist.size()>0){
            updateList(addonlist,"addon");
        }else
        {
            cardaddon.setVisibility(View.GONE);

        }

        if(alacartlist.size()>0){

            updateList(alacartlist,"Alacarte");
        }else
        {
            cardlarcart.setVisibility(View.GONE);
        }




    }

    public void getData()
    {
        dataList.clear();
        try {
            JSONArray jsonArray = new JSONArray(selection);
            for(int i= 0; i<jsonArray.length();i++)
            {
                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                RenewPlanReponse.Datum renewPlanReponse = new Gson().fromJson(jsonObject.toString(),RenewPlanReponse.Datum.class);
                dataList.add(renewPlanReponse);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void connectUI()
    {


        cardbase = findViewById(R.id.cardbase);
        cardaddon = findViewById(R.id.cardaddon);
        cardlarcart = findViewById(R.id.cardlarcart);

        tv_selected = findViewById(R.id.tv_selected);

        tv_payable = findViewById(R.id.tv_payable);
        title  = findViewById(R.id.title);
        // title.setText(type.toUpperCase());
        recyclerView =  findViewById(R.id.recyclerView);
        mAdapter = new CartAdapter(BillSummaryActivity.this,dataList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void selected(List<String> dataList) {

    }

    public void calculateCart()
    {
        if (dataList.size()>1)
        {
            tv_selected.setText("Selected : "+dataList.size() + " items");
        }
        else
        {
            tv_selected.setText("Selected : "+dataList.size() + " item");
        }
        for(RenewPlanReponse.Datum datum : dataList)
        {
            amount = amount + datum.getRate().get(0).getAmount();
        }

        tv_payable.setText("Amount : "+ CommonUtils.formatAmountComma(amount+""));
    }


    public void updateList(List<RenewPlanReponse.Datum> data , String type) {


        Log.d(TAG, "updateList: "+data.size());



        LinearLayout insertPoint;


        if(type.equalsIgnoreCase("base")) {
            insertPoint = findViewById(R.id.insert);
        }else if(type.equalsIgnoreCase("Addon"))
        {
            insertPoint =  findViewById(R.id.insertaddon);
        }else
        {
            insertPoint =  findViewById(R.id.insertalacart);
        }

        insertPoint.removeAllViews();
//        TextView name = (TextView) v.findViewById(R.id.tv_addon_name);
//        TextView typeName = (TextView) v.findViewById(R.id.type);

        for (RenewPlanReponse.Datum datum : data) {
            LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = vi.inflate(R.layout.row_item_cart_on, null);
            Log.d(TAG, "updateList: " + datum.getId());
            TextView name = (TextView) v.findViewById(R.id.tv_addon_name);
            TextView typeName = (TextView) v.findViewById(R.id.type);
            name.setText(datum.getName());
            typeName.setText(datum.getBoxtypeLbl());
            insertPoint.addView(v);
        }


    }




    public void submit(View view) {
        Intent intent = new Intent(BillSummaryActivity.this,RechargePeriodActivity.class);
        intent.putExtra("data",ids);
        intent.putExtra("addon",false);
        intent.putExtra("type",type);
        startActivity(intent);
    }



}
