package io.buildrelease.adminjpr.Activity;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import io.buildrelease.adminjpr.Fragments.ExpiredAccountSevenDaysFragment;
import io.buildrelease.adminjpr.R;

public class BulkBouquetsRenewalAccountActivity extends AppCompatActivity {


    String BouqoutesId = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk_bouquets_renewal_account);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Expired List");
        }
        if(getIntent().getExtras()!=null)
        {
            BouqoutesId = getIntent().getExtras().getString("bid");
            loadFragment();
        }

    }

    public void loadFragment()
    {
        ExpiredAccountSevenDaysFragment expiredAccountSevenDaysFragment = new ExpiredAccountSevenDaysFragment();
        Bundle bundle = new Bundle();
        bundle.putString("boqid",BouqoutesId);
        expiredAccountSevenDaysFragment.setArguments(bundle);

        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.containter, expiredAccountSevenDaysFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BulkBouquetsRenewalAccountActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        BulkBouquetsRenewalAccountActivity.this.finish();
    }
}
