package io.buildrelease.adminjpr.Activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.BulkAssignmentAdapter;
import io.buildrelease.adminjpr.Adapters.BulkOperationsAdapter;
import io.buildrelease.adminjpr.Model.BulkBouqueListResponse;
import io.buildrelease.adminjpr.Model.BulkRemoveBouqetPayload;
import io.buildrelease.adminjpr.Model.BulkSubcriberAccountResponse;
import io.buildrelease.adminjpr.Model.BulkSubcriberAssignmentResponse;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import io.buildrelease.adminjpr.interfaces.SelectedIdListInterface;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.BULKBOUQUETREMOVAL;
import static io.buildrelease.adminjpr.Utility.Params.BULKBOUQUETSASSIGNMENT;
import static io.buildrelease.adminjpr.Utility.Params.BULKBOUQUETSREPLACEMENT;
import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class BulkOperationsActivity extends AppCompatActivity implements SelectedIdListInterface {

    RelativeLayout select_subscribe_title;
    LinearLayout no_data_ll, ll_bouque_type, ll_active_bouque, ll_account_status, ll_buttonpanel;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    int pageno = 1, pageCount = 50;
    boolean newData = true;
    LinearLayoutManager layoutManager;
    boolean isScrolled = false;
    String totalPageCount = "0";
    String TYPE;
    Spinner spinner_bouque_type, spinner_active_bouque, spinner_account_status;
    Button btn_bulk_remove, btn_bulk_replace, btn_bulk_assign;
    ArrayAdapter<String> spinnerAdapter;
    ArrayAdapter<String> bouqueListAdapter;
    List<String> spinnerList = new ArrayList<>();
    List<String> bouqueList = new ArrayList<>();
    HashMap<String, Integer> bouqueType = new HashMap<>();
    HashMap<String, Integer> activeBouque = new HashMap<>();
    List<BulkSubcriberAccountResponse.DataBean> subscriberList = new ArrayList<>();
    List<BulkSubcriberAssignmentResponse.DataBean> subscriberList2 = new ArrayList<>();
    BulkOperationsAdapter mAdapter;
    BulkAssignmentAdapter mAdapterAssignment;
    int bouqueID;
    List<Integer> bouqID = new ArrayList<>();
    List<String> ids = null;
    List<Integer> accid = new ArrayList<>();
    String type=null;
    CheckBox selectallcheckbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk_operations);
        if (getIntent().getExtras() != null) {
            TYPE = getIntent().getStringExtra("type");
        }
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        connectUI();
        if (TYPE.equals(BULKBOUQUETSASSIGNMENT)) {
            mAdapterAssignment = new BulkAssignmentAdapter(BulkOperationsActivity.this, subscriberList2, this, TYPE);
            recyclerView.setAdapter(mAdapterAssignment);
        } else {
            mAdapter = new BulkOperationsAdapter(BulkOperationsActivity.this, subscriberList, this, TYPE);
            recyclerView.setAdapter(mAdapter);
        }

        layoutManager = new LinearLayoutManager(BulkOperationsActivity.this);
        recyclerView.setLayoutManager(layoutManager);

        bulkINIT();
        initComponentListeners();

    }

    private void connectUI() {

        select_subscribe_title=findViewById(R.id.select_subscribe_title);
        selectallcheckbox = findViewById(R.id.selectallcheckbox);
        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView);

        spinner_bouque_type = findViewById(R.id.spinner_bouque_type);
        spinner_active_bouque = findViewById(R.id.spinner_active_bouque);
        spinner_account_status = findViewById(R.id.spinner_account_status);

        btn_bulk_assign = findViewById(R.id.btn_bulk_assign);
        btn_bulk_remove = findViewById(R.id.btn_bulk_remove);
        btn_bulk_replace = findViewById(R.id.btn_bulk_replace);

        ll_bouque_type = findViewById(R.id.ll_bouque_type);
        ll_active_bouque = findViewById(R.id.ll_active_bouque);
        ll_account_status = findViewById(R.id.ll_account_status);
        ll_buttonpanel = findViewById(R.id.buttonPanel);
        setCheckBoxLisener();
    }

    private void bulkINIT() {
        switch (TYPE) {
            case BULKBOUQUETREMOVAL: {
                getSupportActionBar().setTitle("Bulk Removal");
                ll_bouque_type.setVisibility(View.VISIBLE);
                btn_bulk_remove.setVisibility(View.VISIBLE);
                bouqueType.put("Alacarte", 3);
                bouqueType.put("Addon", 2);
                bouqueType.put("Base", 1);
                spinnerINIT("Select Bouquet Type");
                spinner_bouque_type.setAdapter(spinnerAdapter);
                break;

            }
            case BULKBOUQUETSREPLACEMENT: {
                getSupportActionBar().setTitle("Bulk Replacement");
                ll_active_bouque.setVisibility(View.VISIBLE);
                ll_account_status.setVisibility(View.VISIBLE);
                btn_bulk_replace.setVisibility(View.VISIBLE);
                bouqueType.put("Active", 2);
                bouqueType.put("Expired", 3);
                getBouqueList(0);
                spinnerINIT("Select Account Status");
                spinner_account_status.setAdapter(spinnerAdapter);
                break;
            }
            case BULKBOUQUETSASSIGNMENT: {
                getSupportActionBar().setTitle("Bulk Assignment");
                ll_bouque_type.setVisibility(View.VISIBLE);
                btn_bulk_assign.setVisibility(View.VISIBLE);
                bouqueType.put("Addon", 2);
                bouqueType.put("Alacarte", 3);
                spinnerINIT("Select Bouquet Type");
                spinner_bouque_type.setAdapter(spinnerAdapter);
                break;
            }
        }
    }

    private void spinnerINIT(String firstOption) {
        spinnerList.add(firstOption);
        spinnerList.addAll(bouqueType.keySet());
        spinnerAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, spinnerList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    private void spinnerINIT(BulkBouqueListResponse bulkBouqueListResponse, String firstOption) {
        bouqueList.add(firstOption);
        for (BulkBouqueListResponse.Datum datum : bulkBouqueListResponse.getData()) {
            bouqueList.add(datum.getName());
            activeBouque.put(datum.getName(), datum.getId());
        }
        bouqueListAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_item, bouqueList);
        bouqueListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BulkOperationsActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initComponentListeners() {
        spinner_bouque_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    bouqueList.clear();
                    bouqueID = bouqueType.get(spinner_bouque_type.getSelectedItem().toString());
                    getBouqueList(bouqueID);
                    recyclerView.setVisibility(View.INVISIBLE);
                    ll_active_bouque.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_active_bouque.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    switch (TYPE) {
                        case BULKBOUQUETSREPLACEMENT: {
                            spinner_account_status.setSelection(0);
                            bouqueID = activeBouque.get(spinner_active_bouque.getSelectedItem().toString());
                            select_subscribe_title.setVisibility(View.INVISIBLE);
                            recyclerView.setVisibility(View.INVISIBLE);
                            break;
                        }
                        case BULKBOUQUETSASSIGNMENT: {
                            getSubscriberList(activeBouque.get(spinner_active_bouque.getSelectedItem().toString()));
                            bouqueID = activeBouque.get(spinner_active_bouque.getSelectedItem().toString());
                            select_subscribe_title.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.VISIBLE);
                            break;
                        }
                        case BULKBOUQUETREMOVAL: {
                            getSubscriberList(activeBouque.get(spinner_active_bouque.getSelectedItem().toString()));
                            recyclerView.setVisibility(View.VISIBLE);
                            select_subscribe_title.setVisibility(View.VISIBLE);
                            bouqID.add(activeBouque.get(spinner_active_bouque.getSelectedItem().toString()));
                            break;
                        }
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_account_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    if (activeBouque.get(spinner_active_bouque.getSelectedItem().toString()) != null && bouqueType.get(spinner_account_status.getSelectedItem().toString()) != null) {
                        getSubscriberList(activeBouque.get(spinner_active_bouque.getSelectedItem().toString()), bouqueType.get(spinner_account_status.getSelectedItem().toString()));
                    }
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btn_bulk_assign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type=TYPE;
                Intent intent = new Intent(BulkOperationsActivity.this, BulkRechargePeriodActivity.class);
                intent.putExtra("id", String.valueOf(bouqueID));
                intent.putExtra("type", "ASSIGN");
                intent.putStringArrayListExtra("accountnolist", new ArrayList<>(ids));
                startActivity(intent);
            }
        });

        btn_bulk_replace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type=TYPE;
                Intent intent = new Intent(BulkOperationsActivity.this, BulkReplacementBouquetListActivity.class);
                intent.putExtra("bouqueIDList", String.valueOf(bouqueID));
                intent.putStringArrayListExtra("accountID", new ArrayList<>(ids));
                startActivity(intent);
            }
        });

        btn_bulk_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                type=TYPE;
                showRemarkDialog();
            }
        });
    }

    public void setCheckBoxLisener()
    {
        selectallcheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    Log.d(TAG, "onCheckedChanged: Pressed" );
                    markAllSelected();
                }else
                {
                    Log.d(TAG, "UnCheckedChanged: Pressed" );
                    markAlUnlSelected();
                }
            }
        });
    }

    public void unsetCheckBoxLisener()
    {
        selectallcheckbox.setOnCheckedChangeListener(null);
    }

    public void markAllSelected()
    {
        if(mAdapter!=null || mAdapterAssignment!=null) {
            if(subscriberList.size()>0 || subscriberList2.size()>0 ) {
                switch (TYPE) {
                    case BULKBOUQUETREMOVAL:
                    case BULKBOUQUETSREPLACEMENT: {
                        Log.d(TAG, "markAllSelected: This is Removal" );
                        mAdapter.markSelected();
                        break;
                    }
                    case BULKBOUQUETSASSIGNMENT: {
                        Log.d(TAG, "markAllSelected: This is assignment" );
                        mAdapterAssignment.markSelected();
                        break;
                    }
                }
            }
        }
    }



    public void markAlUnlSelected()
    {
        if(mAdapter!=null || mAdapterAssignment!=null) {
            if(subscriberList.size()>0 || subscriberList2.size()>0 ) {
                switch (TYPE) {
                    case BULKBOUQUETREMOVAL:
                    case BULKBOUQUETSREPLACEMENT: {
                        Log.d(TAG, "markAlUnlSelected: This is Removal" );
                        mAdapter.markUnSelected();
                        break;
                    }
                    case BULKBOUQUETSASSIGNMENT: {
                        Log.d(TAG, "markAlUnlSelected: This is assignment" );
                        mAdapterAssignment.markUnSelected();
                        break;
                    }
                }

            }
        }
    }


    public void unCheckSelectedAll()
    {
        if(selectallcheckbox!=null)
        {
            if(selectallcheckbox.isChecked()) {
                unsetCheckBoxLisener();
                selectallcheckbox.setChecked(false);
                setCheckBoxLisener();
            }
        }


    }


    private void getSubscriberList(int bouqueID) {
        ApiRequest apiRequest = new ApiRequest(BulkOperationsActivity.this);
        Call<JsonObject> call = null;
        if (TYPE.equals(BULKBOUQUETSASSIGNMENT)) {
            call = apiInterface.getBulkAssignmentSubscriberList(UtilPref.getAuth(BulkOperationsActivity.this), bouqueID, pageno, pageCount);
        } else {
            call = apiInterface.getBulkSubscriberList(UtilPref.getAuth(BulkOperationsActivity.this), bouqueID, pageno, pageCount);
        }
        apiRequest.request(call, getSubscriberListDataListener);
    }

    private void getSubscriberList(int bouqueID, int renewableType) {
        ApiRequest apiRequest = new ApiRequest(BulkOperationsActivity.this);
        Call<JsonObject> call = apiInterface.getBulkSubscriberList(UtilPref.getAuth(BulkOperationsActivity.this), bouqueID, renewableType, pageno, pageCount);
        apiRequest.request(call, getSubscriberListDataListener);
    }

    FetchDataPaginatedListener getSubscriberListDataListener = new FetchDataPaginatedListener() {


        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            if (TYPE.equals(BULKBOUQUETSASSIGNMENT)) {
                BulkSubcriberAssignmentResponse bulkSubcriberAccountResponse = new Gson().fromJson(data, BulkSubcriberAssignmentResponse.class);
                totalPageCount = pageCount;
                inflatePairingAdapter(bulkSubcriberAccountResponse);
            } else {
                BulkSubcriberAccountResponse bulkSubcriberAccountResponse = new Gson().fromJson(data, BulkSubcriberAccountResponse.class);
                totalPageCount = pageCount;
                inflatePairingAdapter(bulkSubcriberAccountResponse);
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(BulkOperationsActivity.this, msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(BulkOperationsActivity.this);
        }
    };

    public void inflatePairingAdapter(BulkSubcriberAccountResponse bulkSubcriberAccountResponse) {
        if (newData) {
            subscriberList.clear();
            subscriberList.addAll(bulkSubcriberAccountResponse.getData());
            mAdapter.notifyDataSetChanged();
        } else {
            subscriberList.addAll(bulkSubcriberAccountResponse.getData());
            mAdapter.notifyDataSetChanged();
        }
        if (bulkSubcriberAccountResponse.getData() != null && bulkSubcriberAccountResponse.getData().size() == 0) {
            no_data_ll.setVisibility(View.VISIBLE);
            select_subscribe_title.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        } else {
            no_data_ll.setVisibility(View.GONE);
            select_subscribe_title.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void inflatePairingAdapter(BulkSubcriberAssignmentResponse bulkSubcriberAssignmentResponse) {
        if (newData) {
            subscriberList.clear();
            subscriberList2.addAll(bulkSubcriberAssignmentResponse.getData());
            mAdapterAssignment.notifyDataSetChanged();
        } else {
            subscriberList2.addAll(bulkSubcriberAssignmentResponse.getData());
            mAdapterAssignment.notifyDataSetChanged();
        }
        if (bulkSubcriberAssignmentResponse.getData() != null && bulkSubcriberAssignmentResponse.getData().size() == 0) {
            no_data_ll.setVisibility(View.VISIBLE);
            select_subscribe_title.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
        } else {
            no_data_ll.setVisibility(View.GONE);
            select_subscribe_title.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void getBouqueList(int type) {
        ApiRequest apiRequest = new ApiRequest(BulkOperationsActivity.this);
        Call<JsonObject> call = urlRetriever(TYPE, type);
        apiRequest.request(call, getBouqueListDataListener);
    }

    FetchDataListener getBouqueListDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            BulkBouqueListResponse bulkBouqueListResponse = new Gson().fromJson(data, BulkBouqueListResponse.class);
            spinnerINIT(bulkBouqueListResponse, "Select Active Bouquet");
            spinner_active_bouque.setAdapter(bouqueListAdapter);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(BulkOperationsActivity.this, msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(BulkOperationsActivity.this);
        }
    };


    @Override
    public void selectedIds(List<String> ids) {
        this.ids = ids;
        if (ids.size() > 0) {
            ll_buttonpanel.setVisibility(View.VISIBLE);

        } else {
            ll_buttonpanel.setVisibility(View.GONE);

        }
    }

    public void loadmoreData() {

        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();

        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;

            if (totalPageCount != null) {
                int total = Integer.parseInt(totalPageCount);
                Log.d(TAG, "loadmoreData: total" + total + " pageno " + pageno + " pagecount " + pageCount);
                if (total > pageno * pageCount) {
                    pageno += 1;
                    newData = false;
                    getSubscriberList(bouqueID);
                }
            }

        }
    }


    public void showRemarkDialog() {
        try {
            final MaterialDialog.Builder builder = new MaterialDialog.Builder(BulkOperationsActivity.this);
            builder.autoDismiss(true);
            builder.cancelable(true);

            builder.title("Remark")
                    .inputRange(0, 255)
                    .positiveColor(getResources().getColor(R.color.colorPrimary))
                    .negativeColor(getResources().getColor(R.color.colorPrimary))
                    .input("Type your comment here", "", false, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            performBulkOperation(input.toString());
                        }
                    })
                    .positiveText("Submit")
//                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//
//                        }
//                    })
                    .negativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            builder.cancelable(true);
                        }
                    })
                    .canceledOnTouchOutside(true)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void performBulkOperation(String remark) {
        ApiRequest apiRequest = new ApiRequest(BulkOperationsActivity.this);

        for (String s : ids) accid.add(Integer.valueOf(s));

        Call<JsonObject> call = apiInterface.bulkBouquetRemove(UtilPref.getAuth(BulkOperationsActivity.this), new BulkRemoveBouqetPayload(accid, remark, bouqID));
        apiRequest.request(call, bulkRemoveBouquetListener);
    }

    FetchDataListener bulkRemoveBouquetListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);


            CommonUtils.showSuccessMessage(BulkOperationsActivity.this, data, "Account Removed Sucessfully!");
            //CommonUtils.showToasty(,"Successful", Params.TOASTY_SUCCESS);
            BulkOperationsActivity.this.finish();
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();

            CommonUtils.showFailureMessage(BulkOperationsActivity.this, msg, "Something went wrong, Please try  sometime  later");

            //  CommonUtils.showToasty(BulkOperationsActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(BulkOperationsActivity.this);
        }
    };

    Call<JsonObject> urlRetriever(String bulkType, int type) {
        Call<JsonObject> call = null;
        switch (bulkType) {
            case BULKBOUQUETREMOVAL: {
                call = apiInterface.getBulkBouqueList(UtilPref.getAuth(BulkOperationsActivity.this), type);
                break;
            }
            case BULKBOUQUETSREPLACEMENT: {
                call = apiInterface.getBulkBouqueList(UtilPref.getAuth(BulkOperationsActivity.this));
                break;
            }
            case BULKBOUQUETSASSIGNMENT: {
                call = apiInterface.getBulkBouqueList(UtilPref.getAuth(BulkOperationsActivity.this), type);
                break;
            }

        }
        return call;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(type!=null)
        {
            deInit();
            bulkINIT();
        }

    }


    private void deInit()
    {
        switch (TYPE) {
            case BULKBOUQUETREMOVAL: {
                ll_bouque_type.setVisibility(View.GONE);
                btn_bulk_remove.setVisibility(View.GONE);
                subscriberList.clear();
                mAdapter.notifyDataSetChanged();
                break;

            }
            case BULKBOUQUETSREPLACEMENT: {
                ll_active_bouque.setVisibility(View.GONE);
                ll_account_status.setVisibility(View.GONE);
                btn_bulk_replace.setVisibility(View.GONE);
                subscriberList.clear();
                mAdapter.notifyDataSetChanged();
                break;
            }
            case BULKBOUQUETSASSIGNMENT: {
                ll_bouque_type.setVisibility(View.GONE);
                btn_bulk_assign.setVisibility(View.GONE);
                subscriberList2.clear();
                mAdapterAssignment.notifyDataSetChanged();
                bouqueList.clear();
                ll_active_bouque.setVisibility(View.GONE);
                break;
            }
        }
        ids.clear();
        bouqueType.clear();
        spinnerList.clear();
        ll_buttonpanel.setVisibility(View.GONE);
    }
}

