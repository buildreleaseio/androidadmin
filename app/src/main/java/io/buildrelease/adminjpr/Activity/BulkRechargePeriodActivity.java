package io.buildrelease.adminjpr.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.buildrelease.adminjpr.Adapters.RechargeListAdapter;
import io.buildrelease.adminjpr.Model.RechargeResponse;
import io.buildrelease.adminjpr.Model.SavePayloadForBulkAssignment;
import io.buildrelease.adminjpr.Model.SavePayloadForBulkPackChange;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import io.buildrelease.adminjpr.interfaces.SelectedPeriodInterfaceAddon;
import retrofit2.Call;

public class BulkRechargePeriodActivity extends AppCompatActivity  implements SelectedPeriodInterfaceAddon {

    final String TAG = getClass().getSimpleName();
    APIInterface apiInterface;
    ApiRequest apiRequest = new ApiRequest(this);
    private RecyclerView recyclerView;
    private RechargeListAdapter mAdapter;
    List<RechargeResponse.Data.Rp0> dataList = new ArrayList<>();
    List<String> ids =null;
    List<Integer> ids2 =new ArrayList<>();
    List<Integer> bouqueIDList =new ArrayList<>();
    String id;
    int bouqueid;
    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk_recharge_period);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getIntent().getExtras()!=null)
        {
            type=getIntent().getStringExtra("type");
            ids = getIntent().getExtras().getStringArrayList("accountnolist");
            bouqueid = Integer.parseInt(getIntent().getExtras().getString("id"));
        }
//        if(getIntent().getStringExtra("accno")!=null)
//        {
//            id=getIntent().getStringExtra("accno");
//        }
        connectUI();
        callRechargePeriod();
    }

    private void callRechargePeriod() {
        if(bouqueid!=0)
        {
            Call<JsonObject> call = apiInterface.getBulkRechargePeriod(UtilPref.getAuth(this),bouqueid);
            apiRequest.request(call,fetchDataListener);
        }
    }

    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);
            RechargeResponse rechargeResponse = new Gson().fromJson(data,RechargeResponse.class);
            dataList.clear();
            dataList.addAll(rechargeResponse.getData().getRp0());
            mAdapter.notifyDataSetChanged();

        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(BulkRechargePeriodActivity.this);
        }
    };
    public void connectUI() {

        recyclerView = findViewById(R.id.recyclerView);
        mAdapter = new RechargeListAdapter(BulkRechargePeriodActivity.this, dataList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void selectedID(String id) {
        Call<JsonObject> call=null;
        for(String s : ids) ids2.add(Integer.valueOf(s));
        if(type.equals("ASSIGN"))
        {
            bouqueIDList.add(bouqueid);
            SavePayloadForBulkAssignment savePayload;
            savePayload = new SavePayloadForBulkAssignment();
            savePayload.setAccountIds(ids2);
            savePayload.setRemark(Params.CHANGE_BASE_PLAN);
            savePayload.setRperiodId(id);
            savePayload.setNewbouqueIds(bouqueIDList);
            call = apiInterface.bulkAssign(UtilPref.getAuth(this), savePayload);
            apiRequest.request(call, fetchDataListenerSave);
        }
        else
        {
            SavePayloadForBulkPackChange savePayload;
            savePayload = new SavePayloadForBulkPackChange();
            savePayload.setAccountIds(ids2);
            savePayload.setRemark(Params.CHANGE_BASE_PLAN);
            savePayload.setRperiodId(id);
            savePayload.setNewbouqueIds(bouqueid);
            call = apiInterface.bulkreplaceBasePlan(UtilPref.getAuth(this), savePayload);
            apiRequest.request(call, fetchDataListenerSave);
        }
    }

    FetchDataListener fetchDataListenerSave = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);
            if(type.equals("ASSIGN"))
            {

                CommonUtils.showSuccessMessage(BulkRechargePeriodActivity.this,data,"Bulk Assign Successful");
               // Toasty.success(BulkRechargePeriodActivity.this,).show();
                BulkRechargePeriodActivity.this.finishAffinity();
            }
            else if(type.equals(Params.CHANGE_BASE_PLAN))
            {
                CommonUtils.showSuccessMessage(BulkRechargePeriodActivity.this,data,"Base Plan Changed Successfully");

               // Toasty.success(BulkRechargePeriodActivity.this,"Base Plan Changed Successfully").show();
                BulkRechargePeriodActivity.this.finishAffinity();
            }
            {
                CommonUtils.showSuccessMessage(BulkRechargePeriodActivity.this,data,"Bulk Replacement Successful");

               // Toasty.success(BulkRechargePeriodActivity.this,"Bulk Replacement Successful").show();
                BulkRechargePeriodActivity.this.finishAffinity();
            }

        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();
            Toasty.error(BulkRechargePeriodActivity.this,msg).show();

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(BulkRechargePeriodActivity.this);
        }
    };
}
