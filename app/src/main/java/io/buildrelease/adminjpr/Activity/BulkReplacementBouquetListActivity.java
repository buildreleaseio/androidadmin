package io.buildrelease.adminjpr.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.ChangeBasePlanListAdapter;
import io.buildrelease.adminjpr.Model.NewBasePlan;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class BulkReplacementBouquetListActivity extends AppCompatActivity {

    final String TAG = getClass().getSimpleName();
    RecyclerView recyclerView;
    APIInterface apiInterface;
    int pageno = 1, pageCount = 50;
    boolean newData = true;
    LinearLayoutManager layoutManager;
    ApiRequest apiRequest = new ApiRequest(this);
    boolean isScrolled = false;
    String totalPageCount = "0";
    String TYPE;
    List<NewBasePlan.Datum> dataList = new ArrayList<>();
    ChangeBasePlanListAdapter mAdapter ;
    int bouqueID;
    List<String> ids =null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bulk_replacement);
        if (getIntent().getExtras() != null) {
            bouqueID = Integer.parseInt(getIntent().getStringExtra("bouqueIDList"));
            ids = getIntent().getStringArrayListExtra("accountID");
            setUpRecyclerView();
        }
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Bulk Replace");
        }
         getReplacementBouquetList();
    }


    public void setUpRecyclerView()
    {
        recyclerView =  findViewById(R.id.recyclerView);
        mAdapter = new ChangeBasePlanListAdapter(BulkReplacementBouquetListActivity.this,dataList,ids,"bulk");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                BulkReplacementBouquetListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getReplacementBouquetList() {
        Call<JsonObject> call = null;
        call = apiInterface.getReplacementBulkBouqueList(UtilPref.getAuth(BulkReplacementBouquetListActivity.this),bouqueID);
        apiRequest.request(call,fetchDataListenerBasePLan);
    }

    FetchDataListener fetchDataListenerBasePLan = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);
            RequestQueueService.cancelProgressDialog();
            NewBasePlan newBasePlan = new Gson().fromJson(data,NewBasePlan.class);
            dataList.addAll( newBasePlan.getData());
            mAdapter.notifyDataSetChanged();
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchFailure: "+ msg);

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(BulkReplacementBouquetListActivity.this);
        }
    };
}
