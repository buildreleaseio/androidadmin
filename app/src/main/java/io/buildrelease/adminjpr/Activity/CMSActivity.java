package io.buildrelease.adminjpr.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.OpenComplaintAdapter;
import io.buildrelease.adminjpr.Model.ComplainPayload;
import io.buildrelease.adminjpr.Model.OpenComplaintsData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class CMSActivity extends AppCompatActivity {

    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    int pageno = 1, pageCount = 50;
    OpenComplaintAdapter mAdapter;
    boolean newData = true;
    LinearLayoutManager layoutManager;
    List<OpenComplaintsData.Datum> cmsList = new ArrayList<>();
    boolean isScrolled = false;
    String totalPageCount = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cms);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(" Complain List");
        }

        connnectUI();
        getListData();
    }


    @Override
    protected void onResume() {
        super.onResume();
        reload();
    }

    public void reload()
    {
        pageCount = 50;
        pageno =1 ;
        newData = true;
        getListData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CMSActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void connnectUI() {


        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView);
        mAdapter = new OpenComplaintAdapter(
                CMSActivity.this, cmsList);
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(CMSActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter.notifyDataSetChanged();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolled = true;
                    loadmoreData();
                }
            }
        });

    }

    public void getListData() {

        ApiRequest apiRequest = new ApiRequest(CMSActivity.this);
        Call<JsonObject> call = apiInterface.getCMSlist(UtilPref.getAuth(CMSActivity.this),getIntent().getStringExtra("accountid"),pageno,pageCount);
        apiRequest.request(call, getListDataListener);
    }


    FetchDataPaginatedListener getListDataListener = new FetchDataPaginatedListener() {

        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            OpenComplaintsData openComplaintsData = new Gson().fromJson(data, OpenComplaintsData.class);
            inflatePairingAdapter(openComplaintsData);
            totalPageCount = pageCount;
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(CMSActivity.this, msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CMSActivity.this);
        }
    };


    public void inflatePairingAdapter(OpenComplaintsData openComplaintsData) {
        if (newData) {
            cmsList.clear();
            cmsList.addAll(openComplaintsData.getData());
            mAdapter.notifyDataSetChanged();
        } else {
            cmsList.addAll(openComplaintsData.getData());
            mAdapter.notifyDataSetChanged();
        }
        if(openComplaintsData.getData()!=null && openComplaintsData.getData().size()==0)
        {
            no_data_ll.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            no_data_ll.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }


    public void closeComplaint(String remark, String id) {
        try {
            ApiRequest apiRequest = new ApiRequest(CMSActivity.this);
            Call<JsonObject> call = null;
            ComplainPayload complainPayload = new ComplainPayload();
            complainPayload.setClosingRemark(remark);
            complainPayload.setStatus(2);
            call = apiInterface.replyComplain(UtilPref.getAuth(CMSActivity.this), id, complainPayload);
            apiRequest.request(call, closeComplaintListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    FetchDataListener closeComplaintListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();

            CommonUtils.showSuccessMessage(CMSActivity.this,data,"Reply sent");

           // Toasty.success(CMSActivity.this, "Reply sent").show();

        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(CMSActivity.this,msg,"Something went wrong, Please try  sometime  later");
            //CommonUtils.showToasty(CMSActivity.this, msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CMSActivity.this);
        }
    };


    public void loadmoreData() {

        //   Log.d(TAG, "onScrolled: " + isScrolled);
        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();
        // Log.d(TAG, "onScrolled: visible count : " + totalChildCurrentlyVisibleCount + " scrolled count : " + totalChildItemScrolledCount + " total count : " + totalListItemCount);

        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;
            if(totalPageCount!=null)
            {
                int total = Integer.parseInt(totalPageCount);
                Log.d(TAG, "loadmoreData: total"+total+" pageno "+pageno + " pagecount "+pageCount);
                if(total>pageno*pageCount) {
                    pageno += 1;
                    newData = false;
                    getListData();
                }
            }
        }
    }
}
