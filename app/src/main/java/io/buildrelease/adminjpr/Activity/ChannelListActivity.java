package io.buildrelease.adminjpr.Activity;

import android.os.Bundle;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.ChannelListAdapter;
import io.buildrelease.adminjpr.Model.ChannelListData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;

import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class ChannelListActivity extends AppCompatActivity {

    final String TAG = getClass().getSimpleName();
    APIInterface apiInterface;
    ApiRequest apiRequest = new ApiRequest(this);
    private RecyclerView recyclerView;
    private ChannelListAdapter mAdapter;
    SearchView searchView;
    List<ChannelListData.Channel> mainChannelList = new ArrayList<>();
//    List<ChannelListData.ChannelList> channelsList = new ArrayList<>();
    List<ChannelListData.ChannelList> filteredchannelsList = new ArrayList<>();
    String bouqueId = "1";
    ChannelListData channelListData;
    TextView tv_title;

    ChipGroup chipGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_list);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Channel details");
        }

        if(getIntent().getExtras()!=null)
        {
            bouqueId = getIntent().getExtras().getString("id");
        }
        connectUI();
        getChannel("1");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ChannelListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    public void filterList(String id)
    {

       filteredchannelsList.clear();
        if(id.equalsIgnoreCase("all"))
        {
            for(ChannelListData.Channel channel : mainChannelList)
            {
               List<ChannelListData.ChannelList> channelLists  = channel.getChannelList();
                for(ChannelListData.ChannelList channelList : channelLists) {
                    filteredchannelsList.add(channelList);
                }
            }
        }else
        {
            for(ChannelListData.Channel channel : mainChannelList)
            {
                if(channel!=null && channel.getGenreId()!=null) {
                    if (channel.getGenreId().equalsIgnoreCase(id)) {
                        List<ChannelListData.ChannelList> channelLists = channel.getChannelList();
                        for (ChannelListData.ChannelList channelList : channelLists) {
                            filteredchannelsList.add(channelList);
                        }
                    }
                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }





    public void setUpChips()
    {

        chipGroup.setChipSpacing(20);
        chipGroup.setSingleSelection(true);
        Chip chip1 = new Chip(ChannelListActivity.this);
        chip1.setTag("all");

        chip1.setCheckable(true);
       // chip1.setChecked(true);
        chip1.setText("  All   ");
        chip1.setClickable(true);
        chip1.setChipBackgroundColorResource(R.color.colorPrimary);
        chip1.setTextColor(getResources().getColor(R.color.colorWhite));
        chip1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterList(view.getTag().toString());
            }
        });
        chipGroup.addView(chip1);
        for(ChannelListData.Channel channel : mainChannelList)
        {
            Chip chip = new Chip(ChannelListActivity.this);
            chip.setTag(channel.getGenreId());
            chip.setCheckable(true);
            chip.setText(channel.getGenreName());
            chip.setClickable(true);
            chip.setChipBackgroundColorResource(R.color.colorPrimary);
            chip.setTextColor(getResources().getColor(R.color.colorWhite));
            chip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    filterList(view.getTag().toString());
                }
            });
            chipGroup.addView(chip);
        }

    }



    public void connectUI()
    {

        tv_title = findViewById(R.id.tv_title);
        searchView = findViewById(R.id.searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                mAdapter.getFilter().filter(s);
                return false;
            }
        });


        searchView.setIconified(false);
        searchView.clearFocus();
        chipGroup = findViewById(R.id.filter_chipgroup);
        recyclerView = findViewById(R.id.recyclerView);
        mAdapter = new ChannelListAdapter(ChannelListActivity.this,filteredchannelsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        GridLayoutManager manager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
       // recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

    public void getChannel(String id)
    {
        Call<JsonObject> call = apiInterface.getChannelList(UtilPref.getAuth(ChannelListActivity.this),  bouqueId);
        apiRequest.request(call,fetchDataListener);
    }


    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {

            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);
            channelListData = new Gson().fromJson(data,ChannelListData.class);
            tv_title.setText(channelListData.getData().getName());
            mainChannelList.clear();
            mainChannelList.addAll(channelListData.getData().getChannels());
            filterList("all");
            setUpChips();

        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();

        }

        @Override
        public void onFetchStart() {

                RequestQueueService.showProgressDialog(ChannelListActivity.this);



        }
    };
}
