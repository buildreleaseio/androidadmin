package io.buildrelease.adminjpr.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.ClosedComplaintAdapter;
import io.buildrelease.adminjpr.Model.ClosedComplaintsData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class ClosedComplainActivity extends AppCompatActivity {

    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    int pageno =1 ,pageCount = 50;
    ClosedComplaintAdapter mAdapter ;
    boolean newData = true;
    LinearLayoutManager layoutManager;
    List<ClosedComplaintsData.Datum> closedComplainList = new ArrayList<>();
    boolean isScrolled = false;
    String totalPageCount = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closed_complain);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Closed Complain List");
        }

        connnectUI();
        getListData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ClosedComplainActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void connnectUI()
    {

        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView);
        mAdapter = new ClosedComplaintAdapter(closedComplainList,ClosedComplainActivity.this);
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(ClosedComplainActivity.this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolled = true;
                    loadmoreData();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void getListData() {

        ApiRequest apiRequest = new ApiRequest(ClosedComplainActivity.this);
        Call<JsonObject> call = apiInterface.getClosedComplaintlist(UtilPref.getAuth(ClosedComplainActivity.this),pageno,pageCount);
        apiRequest.request(call, getListDataListener);
    }




    FetchDataPaginatedListener getListDataListener =new FetchDataPaginatedListener() {
        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            totalPageCount = pageCount;
            ClosedComplaintsData closedComplaintsData = new Gson().fromJson(data, ClosedComplaintsData.class);
            inflatePairingAdapter(closedComplaintsData);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(ClosedComplainActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(ClosedComplainActivity.this);
        }
    };


    public void inflatePairingAdapter(ClosedComplaintsData closedComplaintsData ){
        if(newData) {
            closedComplainList.clear();
            closedComplainList.addAll(closedComplaintsData.getData());
            mAdapter.notifyDataSetChanged();
        }else
        {
            closedComplainList.addAll(closedComplaintsData.getData());
            mAdapter.notifyDataSetChanged();
        }
        if(closedComplaintsData.getData()!=null && closedComplaintsData.getData().size()==0)
        {
            no_data_ll.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            no_data_ll.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void loadmoreData() {

        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();

        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;
            if(totalPageCount!=null)
            {
                int total = Integer.parseInt(totalPageCount);
                Log.d(TAG, "loadmoreData: total"+total+" pageno "+pageno + " pagecount "+pageCount);
                if(total>pageno*pageCount) {
                    pageno += 1;
                    newData = false;
                    getListData();
                }
            }

        }
    }
}
