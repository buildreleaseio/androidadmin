package io.buildrelease.adminjpr.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Model.ComplainMainCategory;
import io.buildrelease.adminjpr.Model.ComplainSubCategory;
import io.buildrelease.adminjpr.Model.CreateComplainPayload;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;

import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class CreateComplainActivity extends AppCompatActivity {

    final String TAG = getClass().getSimpleName();
    EditText remarks;
    TextView stb_no,tv_vc;
    Spinner subspinner,mainspinner;
    APIInterface apiInterface;
    ApiRequest apiRequest = new ApiRequest(this);
    List<ComplainMainCategory.Datum> mainList = new ArrayList<>();
    List<String> mainNameList = new ArrayList<>();
    List<ComplainSubCategory.Datum> subList = new ArrayList<>();
    List<String> subNameList = new ArrayList<>();
    Integer categoryId , subCategory;
    String stbno,vcno,accno;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_complain);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Create Complaint");
        }

        if(getIntent().getExtras()!=null)
        {
            stbno  = getIntent().getExtras().getString("stbno");
            vcno = getIntent().getExtras().getString("vcno");
            accno = getIntent().getExtras().getString("accno");
            connectUI();
            setUpLisener();
            getMainCategory();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CreateComplainActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        remarks.clearFocus();
    }

    private void connectUI() {

        remarks = findViewById(R.id.remarks);
        remarks.setFocusable(true);
        remarks.requestFocusFromTouch();
        remarks.clearFocus();
        stb_no = findViewById(R.id.stb_no);
        tv_vc = findViewById(R.id.tv_vc);
        subspinner = findViewById(R.id.subspinner);
        mainspinner = findViewById(R.id.mainspinner);

        stb_no.setText(stbno);
        tv_vc.setText(vcno);

    }


    private void setUpLisener()
    {
        subspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                subCategory = subList.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mainspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getSubCategory(mainList.get(i).getId()+"");
                categoryId = mainList.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }



    public void setUPMainSpinner(List<String> mainNameList)
    {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mainNameList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        mainspinner.setAdapter(dataAdapter);
    }

    public void getMainCategory()
    {
        Call<JsonObject> call = apiInterface.getMainComplainCategory(UtilPref.getAuth(CreateComplainActivity.this));
        apiRequest.request(call,fetchDataListener);
    }

    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            ComplainMainCategory complainMainCategory = new Gson().fromJson(data,ComplainMainCategory.class);
            mainList.clear();
            mainList.addAll(complainMainCategory.getData());
            mainNameList.clear();
            for(ComplainMainCategory.Datum datum : mainList)
            {
                mainNameList.add(datum.getName());
            }

            setUPMainSpinner(mainNameList);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchFailure: "+msg);

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CreateComplainActivity.this);

        }
    };



    public void setUPSubSpinner(List<String > subNameList)
    {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, subNameList);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        subspinner.setAdapter(dataAdapter);
    }

    public void getSubCategory(String id)
    {
        Call<JsonObject> call = apiInterface.getSubComplainCategory(UtilPref.getAuth(CreateComplainActivity.this),id);
        apiRequest.request(call,fetchDataListener2);
    }

    FetchDataListener fetchDataListener2 = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            ComplainSubCategory complainSubCategory = new Gson().fromJson(data,ComplainSubCategory.class);
            subList.clear();
            subList.addAll(complainSubCategory.getData());
            subNameList.clear();
            for(ComplainSubCategory.Datum datum : subList)
            {
                subNameList.add(datum.getName());
            }

            setUPSubSpinner(subNameList);
        }

        @Override
        public void onFetchFailure(String msg) {

        }

        @Override
        public void onFetchStart() {

        }
    };


    public void submit(View view) {

        if(remarks.getText().toString().trim()!=null && !remarks.getText().toString().isEmpty())
        {
            CreateComplainPayload createComplainPayload = new CreateComplainPayload();
            Log.d(TAG, "submit: category"+categoryId);
            Log.d(TAG, "submit: sub cat"+subCategory);
            createComplainPayload.setCategoryId(categoryId);
            createComplainPayload.setSubcategoryId(subCategory);
            createComplainPayload.setOpeningRemark(remarks.getText().toString());
            createComplainPayload.setAccountId(Integer.parseInt(accno));
            Call<JsonObject> call = apiInterface.postComplain(UtilPref.getAuth(CreateComplainActivity.this),createComplainPayload);
            apiRequest.request(call,fetchDataListener3);
        }else
        {
            Toast.makeText(CreateComplainActivity.this,"Please mention your remarks",Toast.LENGTH_SHORT).show();
        }

    }

    FetchDataListener fetchDataListener3 = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            Log.d(TAG, "onFetchComplete: "+data);
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showSuccessMessage(CreateComplainActivity.this,data,"Complain Submited");
           // Toast.makeText(CreateComplainActivity.this,"Complain Submited",Toast.LENGTH_SHORT).show();
            CreateComplainActivity.this.finish();

        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(CreateComplainActivity.this,msg,"Something went wrong, Please try  sometime  later");
            Log.d(TAG, "onFetchComplete: "+msg);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CreateComplainActivity.this);

        }
    };
}
