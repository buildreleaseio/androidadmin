package io.buildrelease.adminjpr.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.ChangeBasePlanListAdapter;
import io.buildrelease.adminjpr.Model.ActiveBasePlan;
import io.buildrelease.adminjpr.Model.NewBasePlan;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class CustomerChangePackActivity extends AppCompatActivity {

    final String TAG = getClass().getSimpleName();
    APIInterface apiInterface;
    ApiRequest apiRequest = new ApiRequest(this);
    private RecyclerView recyclerView;
    private ChangeBasePlanListAdapter mAdapter;
    List<NewBasePlan.Datum> dataList = new ArrayList<>();
    TextView tv_deactivationDate,tv_baseplan_status,tv_basename,tv_stb,tv_vcnumber;
    SearchView searchView;
    String accountno,bouqueID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_change_pack);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Change Base Plan");
        }
        connectUI();
        if(getIntent().getExtras()!=null)
        {
            accountno=getIntent().getStringExtra("accno");
            setUpRecyclerView();
            tv_stb.setText(getIntent().getStringExtra("stbno"));
            tv_vcnumber.setText(getIntent().getStringExtra("vc"));
        }
        getActiveBasePlan(accountno);
    }


    public void connectUI()
    {

        tv_deactivationDate = findViewById(R.id.tv_deactivationDate);
        tv_basename = findViewById(R.id.tv_basename);
        tv_stb = findViewById(R.id.tv_stb);
        searchView = findViewById(R.id.searchView);
        tv_vcnumber = findViewById(R.id.tv_vcnumber);

        tv_baseplan_status = findViewById(R.id.tv_baseplan_status);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                mAdapter.getFilter().filter(s);
                return false;
            }
        });

        searchView.setIconified(false);
        searchView.clearFocus();

    }

    public void setUpRecyclerView()
    {
        recyclerView =  findViewById(R.id.recyclerView);
        mAdapter = new ChangeBasePlanListAdapter(CustomerChangePackActivity.this,dataList,accountno);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CustomerChangePackActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();


    }

    public void getActiveBasePlan(String accountno)
    {
        Call<JsonObject> call = null;
        call = apiInterface.getActiveBasePlan(UtilPref.getAuth(CustomerChangePackActivity.this),accountno);
        apiRequest.request(call,fetchDataListenerBasePLan);
    }

    FetchDataListener fetchDataListenerBasePLan = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);
            ActiveBasePlan activeBasePlan = new Gson().fromJson(data,ActiveBasePlan.class);
            if(activeBasePlan.getData().size()>0) {
                tv_deactivationDate.setText(CommonUtils.dateFormat(activeBasePlan.getData().get(0).getDeactivationDate()));
                tv_basename.setText(activeBasePlan.getData().get(0).getName());
                tv_baseplan_status.setText(activeBasePlan.getData().get(0).getStatusLbl());
                Log.d(TAG, "onFetchComplete:--"+activeBasePlan.getData().get(0).getStatusLbl()+"--");
                tv_baseplan_status.setTextColor(CommonUtils.statusColor(activeBasePlan.getData().get(0).getStatusLbl().trim()));
                if(activeBasePlan.getData().get(0).getBouqueId()!=null)
                {
                    if(accountno!=null)
                    {
                        getBaseList(activeBasePlan.getData().get(0).getBouqueId());
                    }

                }
            }else
            {
                Toast.makeText(CustomerChangePackActivity.this,"No current base plan to renew",Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchFailure: "+ msg);

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerChangePackActivity.this);
        }
    };



    public void getBaseList(int bouqueid)
    {
        Call<JsonObject> call = apiInterface.getNewBasePlan(UtilPref.getAuth(this),bouqueid);
        apiRequest.request(call,fetchDataListener);
    }


    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            NewBasePlan newBasePlan = new Gson().fromJson(data,NewBasePlan.class);
            dataList.addAll( newBasePlan.getData());
            mAdapter.notifyDataSetChanged();

        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerChangePackActivity.this);
        }
    };



}
