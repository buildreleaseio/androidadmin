package io.buildrelease.adminjpr.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.CustomerVCListAdapter;
import io.buildrelease.adminjpr.Model.CustomerDetail;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class CustomerDetailActivity extends AppCompatActivity {

    APIInterface apiInterface;
    String TAG = getClass().getSimpleName();
    ApiRequest apiRequest = new ApiRequest(CustomerDetailActivity.this);
    TextView tv_customer_name,tv_customer_id,tv_customer_status;
    RecyclerView recyclerView;
    List<CustomerDetail.AccountList> accountLists = new ArrayList<>();
    CustomerVCListAdapter mAdapter;
    LinearLayout ll_profile,ll_uploadkyc,ll_update;
    CustomerDetail customerDetail;
    LinearLayout ll_bottom;
//    String customer_id = "",smartcardno = "",stbno="",mobile_no="";
    private String mobile_no ="",customer_id ="",smartcardno="",stbno="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_detail);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Customer Details");
        }
        parseIntent();

//        if(getIntent().getExtras()!=null)
//        {
//            customer_id = getIntent().getExtras().getString("customer_id","");
//            smartcardno = getIntent().getExtras().getString("smartcardno","");
//            stbno = getIntent().getExtras().getString("stbno","");
//            mobile_no = getIntent().getExtras().getString("mobile_no","");
//
//        }


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CustomerDetailActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void parseIntent() {
        Intent intent = getIntent();
        if(intent.hasExtra("mobile_no")){
            mobile_no = intent.getStringExtra("mobile_no");
        }if(intent.hasExtra("customer_id")){
            customer_id = intent.getStringExtra("customer_id");
        }if(intent.hasExtra("smartcardno")){
            smartcardno = intent.getStringExtra("smartcardno");
        }if(intent.hasExtra("stbno")){
            stbno = intent.getStringExtra("stbno");
        }connectUI();
        searchCustomer();
    }


    public void connectUI()
    {


        ll_bottom = findViewById(R.id.ll_bottom);
        tv_customer_id = findViewById(R.id.tv_customer_id);
        tv_customer_name = findViewById(R.id.tv_customer_name);
        tv_customer_status = findViewById(R.id.tv_customer_status);
        ll_profile = findViewById(R.id.ll_profile);
        ll_uploadkyc = findViewById(R.id.ll_uploadkyc);
        ll_update = findViewById(R.id.ll_update);

        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(customerDetail!=null && accountLists!=null)
                {
                    Intent intent = new Intent(CustomerDetailActivity.this,CustomerProfileActivity.class);
                    intent.putExtra("subid",customerDetail.getData().get(0).getId());
                    startActivity(intent);
                }
            }
        });

        ll_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(customerDetail!=null && accountLists!=null)
                {
                    Intent intent = new Intent(CustomerDetailActivity.this,CustomerProfileEditActivity.class);
                    intent.putExtra("subid",customerDetail.getData().get(0).getId());
                    startActivity(intent);
                }
            }
        });

        ll_uploadkyc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomerDetailActivity.this,CustomerKYCUploadActivity.class);
                intent.putExtra("subid",customerDetail.getData().get(0).getId());
                startActivity(intent);
            }
        });




    }

    public void updateUI(CustomerDetail customerDetail)
    {


        if(customerDetail!=null && customerDetail.getData().size()>0) {

            ll_bottom.setVisibility(View.VISIBLE);
            recyclerView = findViewById(R.id.recyclerView);
            mAdapter = new CustomerVCListAdapter(CustomerDetailActivity.this,accountLists,customerDetail.getData().get(0).getName(),customerDetail.getData().get(0).getCustomerId());
            LinearLayoutManager layoutManager = new LinearLayoutManager(CustomerDetailActivity.this);
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            tv_customer_id.setText(customerDetail.getData().get(0).getCustomerId());
            tv_customer_name.setText(customerDetail.getData().get(0).getName());
            tv_customer_status.setText(customerDetail.getData().get(0).getStatusLbl());
            if(customerDetail.getData().get(0).getStatusLbl().toLowerCase().equals("active"))
            {
                tv_customer_status.setTextColor(CustomerDetailActivity.this.getResources().getColor(R.color.colorGreen));
            }
            else
            {
                tv_customer_status.setTextColor(CustomerDetailActivity.this.getResources().getColor(R.color.colorRed));
            }

            accountLists.clear();
            accountLists.addAll(customerDetail.getData().get(0).getAccountList());

            Log.d(TAG, "updateUI: "+accountLists.size());
            mAdapter.notifyDataSetChanged();
        }else
        {
            ll_bottom.setVisibility(View.GONE);
            CommonUtils.showToasty(CustomerDetailActivity.this,"No data found for data", Params.TOASTY_ERROR);
        }
    }



    public void searchCustomer()
    {
        Call<JsonObject> call = null;
//        call = apiInterface.getCustomerDetail(UtilPref.getAuth(CustomerDetailActivity.this),"jpr2143186","","","");
//        enable this to connect prev page
//        call = apiInterface.getCustomerDetail(UtilPref.getAuth(CustomerDetailActivity.this),customer_id,smartcardno,stbno,mobile_no);
        call = apiInterface.getCustomerDetail(UtilPref.getAuth(CustomerDetailActivity.this),customer_id,smartcardno,stbno,mobile_no);
        apiRequest.request(call,fetchDataListener);
    }

    FetchDataListener fetchDataListener = new FetchDataListener() {

        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete:  "+data);
            customerDetail = new Gson().fromJson(data,CustomerDetail.class);
            updateUI(customerDetail);

        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(CustomerDetailActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerDetailActivity.this);
        }
    };

}
