package io.buildrelease.adminjpr.Activity;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import io.buildrelease.adminjpr.BuildConfig;
import io.buildrelease.adminjpr.Model.CustomerProfileData;
import io.buildrelease.adminjpr.Model.EditProfielPayload;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class CustomerKYCUploadActivity extends AppCompatActivity {
    String subid;

    APIInterface apiInterface;
    String TAG = getClass().getSimpleName();
    ApiRequest apiRequest = new ApiRequest(this);
    ImageView closeIV, cancelPan, cancelSelf;
    LinearLayout panContainer, selfPicContainer, panFileContainer, selfFileContainer;
    String imageType = "";
    //    boolean disableClick = true;
    TextView title, subtitle, panFile, selfFile;
    ImageView photoImage, pancardImage;
    private String responseString = null;
    private static File file = null;
    private String pictureImagePath;
    private double bytes;
    private double megabytes;
    private double kilobytes;
    public Bitmap bitmap;
    private String selectedImagePath, imageUrl;
    long totalSize = 0;
    private String docType = "",id_proof ="id_proof",address_proof = "address_proof";
    private String imageFileName = "";
    private Uri outputFileUri;
    private CustomerProfileData profile;
    private EditProfielPayload.UploadProof uploadProofData = new EditProfielPayload.UploadProof();
    private EditProfielPayload editProfielPayload;
    private String filename1= "test",filename2 = "test3";
    EditText et_idproof,et_address_proof;
    List<EditProfielPayload.UploadProof> uploadProofList =new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_kycupload);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if (getIntent().getExtras() != null) {
            subid = getIntent().getExtras().getString("subid");
            connectUI();
        }
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Upload KYC");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CustomerKYCUploadActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void connectUI() {
//        closeIV = findViewById(R.id.new_deposit_close_iv);
//        nestedScrollView = findViewById(R.id.nested);
        et_idproof = findViewById(R.id.et_idproof);
        et_address_proof = findViewById(R.id.et_address_proof);
        panContainer = findViewById(R.id.ll_PAN_photo_container);
        selfPicContainer = findViewById(R.id.ll_self_photo_container);
        panFileContainer = findViewById(R.id.ll_PAN_file_container);
        selfFileContainer = findViewById(R.id.ll_self_file_container);
        cancelPan = findViewById(R.id.close_pan_iv);
        cancelSelf = findViewById(R.id.close_self_iv);
        panFile = findViewById(R.id.et_pan_file_name);
        selfFile = findViewById(R.id.et_self_file_name);
        photoImage = findViewById(R.id.photo_iv);
        pancardImage = findViewById(R.id.pancard_iv);


        panContainer.setOnClickListener(panContainerListener);
        selfPicContainer.setOnClickListener(selfPicContainerListener);


        selfFileContainer.setVisibility(View.GONE);
        panFileContainer.setVisibility(View.GONE);
    }

    //upload image code for PAN card
    View.OnClickListener panContainerListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            startDialog("idproof");
            docType = "id_proof";

        }
    };

    //upload image code for self
    View.OnClickListener selfPicContainerListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startDialog("address");
            docType = "address_proof";

        }
    };



    public boolean validate()
    {
        if(!et_address_proof.getText().toString().trim().isEmpty() && et_address_proof.getText()!=null)
        {
            if(!et_idproof.getText().toString().trim().isEmpty() && et_idproof.getText()!=null)
            {
                return  true;

            }else {
                Toasty.warning(CustomerKYCUploadActivity.this,"Please enter name for the Id Proof");
                return  false;
            }
        }else {
            Toasty.warning(CustomerKYCUploadActivity.this,"Please enter name for the Address Proof");
            return false;
        }
       
    }
    //sattu

    private void startDialog(String imageTypeStr) {
        imageType = imageTypeStr;
        if (ContextCompat.checkSelfPermission(CustomerKYCUploadActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                &&
                ContextCompat.checkSelfPermission(CustomerKYCUploadActivity.this,
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED

        ) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(CustomerKYCUploadActivity.this);
            alertDialog.setTitle("Upload File Option");
            alertDialog.setMessage("How do you want provide the file?");
            alertDialog.setPositiveButton("Choose File", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(Intent.createChooser(intent, "Choose File"), Params.GALLERY_PICTURE_EDIT_PROFILE);
                }
            });
            alertDialog.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {

                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        imageFileName = timeStamp + ".jpg";
                        File storageDir = Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES);
                        pictureImagePath = storageDir.getAbsolutePath() + "/" + imageFileName;
                        file = new File(pictureImagePath);
                        String packageId = getPackageName();
                        // Uri outputFileUri = FileProvider.getUriForFile(getActivity(),packageId,
                        //       file);

                        if (android.os.Build.VERSION.SDK_INT > 23) {
                            outputFileUri = FileProvider.getUriForFile(CustomerKYCUploadActivity.this, BuildConfig.APPLICATION_ID + ".provider", file);
                            // Do something for lollipop and above versions
                            Log.d("ImageCapture23+", outputFileUri.getPath());

                        } else {
                            outputFileUri = Uri.fromFile(file);
                            Log.d("ImageCapture", outputFileUri.getPath());
                            // do something for phones running an SDK before lollipop
                        }
                        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                        startActivityForResult(cameraIntent, Params.CAMERA_REQUEST_EDIT_PROFILE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            alertDialog.show();
        } else {
            askWriteExternalStoragePermission();
        }
    }

    public void askWriteExternalStoragePermission() {
        if (ActivityCompat.checkSelfPermission(CustomerKYCUploadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ||
                ActivityCompat.checkSelfPermission(CustomerKYCUploadActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(CustomerKYCUploadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(CustomerKYCUploadActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Params.MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
            } else if (ActivityCompat.shouldShowRequestPermissionRationale(CustomerKYCUploadActivity.this, Manifest.permission.CAMERA)) {
                ActivityCompat.requestPermissions(CustomerKYCUploadActivity.this, new String[]{Manifest.permission.CAMERA}, Params.MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
            } else {
                ActivityCompat.requestPermissions(CustomerKYCUploadActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, Params.MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE);
            }
        }
    }

    public void closePanFile(View view) {
        panFileContainer.setVisibility(View.GONE);
    }

    public void closeSelfFile(View view) {
        selfFileContainer.setVisibility(View.GONE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @NonNull Intent data) {
        Log.d("ReqCode", String.valueOf(requestCode));
        Log.d("ResultCode", String.valueOf(resultCode));
        Log.d("++IMAGE++", String.valueOf(requestCode));
//        Log.d("++IMAGE++D", data.toString());
        bitmap = null;
        selectedImagePath = null;
        Bitmap imageBitmap;
        Uri tempUri = null;
        if (pictureImagePath != null && resultCode == RESULT_OK && requestCode == Params.CAMERA_REQUEST_EDIT_PROFILE) {

            File imgFile = new File(pictureImagePath);
            imageBitmap = ConvertImageSize(imgFile.getAbsolutePath());
            File f = new File(Environment.getExternalStorageDirectory().toString());
            tempUri = getImageUri(CustomerKYCUploadActivity.this, imageBitmap);
            selectedImagePath = tempUri.getPath();
            if (tempUri != null) {
                file = new File(getRealPathFromURI(tempUri));
                f = file;
                if (file != null) {
                    bytes = file.length();
                    kilobytes = (bytes / 1024);
                    megabytes = (kilobytes / 1024);
                    Log.d(Params.TAG, "onActivityResult:=======>  " + megabytes + "Mb");
//                    pancardImage.setImageBitmap(ConvertToBitmap(file.getAbsolutePath()));
                    if (docType.equalsIgnoreCase("id_proof")) {
                        ShowImage(imageBitmap, pancardImage);
                        panFileContainer.setVisibility(View.VISIBLE);
                    } else {
                        ShowImage(imageBitmap, photoImage);
                        selfFileContainer.setVisibility(View.VISIBLE);
                    }
                }
            }
            if (!f.exists() || megabytes > 20) {
                Toast.makeText(CustomerKYCUploadActivity.this, "Image size should be less than 20 MB", Toast.LENGTH_SHORT).show();
//                displayToast("Error while capturing image");
                return;
            } else {
                try {
                    if (CommonUtils.checkNetStatusResult(CustomerKYCUploadActivity.this)) {
                        /*
                        "ext": "png",
                        "name": "Screenshot from 2018-12-31 20-03-53.png",
                        "proof_type": "id_proof",
                        "type": "image/png"
                         */
                        uploadProofData.setData(getStringFile(f));
                        uploadProofData.setExt("jpg");
                        uploadProofData.setProofType(docType);
                        uploadProofData.setType("image/jpg");
                        uploadProofData.setName(filename1);
                        uploadProofList.add(uploadProofData);
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        } else if (data != null && resultCode == RESULT_OK && requestCode == Params.GALLERY_PICTURE_EDIT_PROFILE) {
            try {
                tempUri = data.getData();
                if (tempUri != null) {
                    if (docType.equalsIgnoreCase("id_proof")) {
                        ShowImageURI(tempUri, pancardImage);
                        panFileContainer.setVisibility(View.VISIBLE);
                    } else {
                        ShowImageURI(tempUri, photoImage);
                        selfFileContainer.setVisibility(View.VISIBLE);
                    }
                    file = new File(getRealPathFromURI(tempUri));
                    if (file != null) {
//                        profileImg.setVisibility(View.VISIBLE);
                        if (CommonUtils.checkNetStatusResult(CustomerKYCUploadActivity.this)) {
                            try {
                                Log.d("???", "onActivityResult: " + file.length());
                                bytes = file.length();
                                kilobytes = (bytes / 1024);
                                megabytes = (kilobytes / 1024);
                                if (file != null && megabytes <= 20) {
                                    uploadProofData.setData(getStringFile(file));
                                    uploadProofData.setExt("jpg");
                                    uploadProofData.setProofType(docType);
                                    uploadProofData.setType("image/jpg");
                                    uploadProofData.setName(filename2);
                                    uploadProofList.add(uploadProofData);
                                } else {
                                    Toast.makeText(CustomerKYCUploadActivity.this, "Image size should be less than 20 MB", Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
//                                RequestQueueService.cancelProgressDialog();
                        Log.d(Params.TAG, "onActivityResult: file -->null");
                    }
                } else {
//                    profileImg.setDefaultImageResId(R.drawable.ic_profile_img);
                    Log.d(Params.TAG, "onActivityResult: tempuri -->null");
                }
            } catch (Exception e) {
                Toast.makeText(CustomerKYCUploadActivity.this, "Error uploading image", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        } else {
            Toast.makeText(CustomerKYCUploadActivity.this, "Image uploading cancelled", Toast.LENGTH_SHORT).show();
        }
    }


    private Uri getImageUri(Context context, Bitmap image) {
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), image, "Title", null);
        Log.d("++IMAGE++", "getImageUri: " + path);
        return Uri.parse(path);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    //=======================new cam code
    private Bitmap ConvertImageSize(String filePath) {
        Bitmap bm = null;
        try {
            //   bm = MediaStore.Images.Media.getBitmap(updatePMAY.getActivity().getContentResolver(), data.getData());
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(filePath, options);
            final int REQUIRED_SIZE = 500;
            int scale = 1;
            while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                    && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;
            options.inSampleSize = scale;
            options.inJustDecodeBounds = false;
            // bm = BitmapFactory.decodeFile(selectedImagePath);
            bm = BitmapFactory.decodeFile(filePath, options);
            bm = modifyOrientation(bm, filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bm;
    }

    private Bitmap ConvertToBitmap(String filePath) {
        Bitmap bm = null;
        try {
            //   bm = MediaStore.Images.Media.getBitmap(updatePMAY.getActivity().getContentResolver(), data.getData());
            File sd = Environment.getExternalStorageDirectory();
            File image = new File(filePath, imageFileName);
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
            bm = bitmap;
//            bitmap = Bitmap.createScaledBitmap(bitmap,bmOptions.getWidth(),parent.getHeight(),true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bm;
    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }


    public void getProfile() {
        Call<JsonObject> call = apiInterface.getCustomerProfile(UtilPref.getAuth(CustomerKYCUploadActivity.this), subid);
        apiRequest.request(call, fetchDataListener2);
    }


    FetchDataListener fetchDataListener2 = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            profile = new Gson().fromJson(data, CustomerProfileData.class);
            update();
            Log.d(TAG, "onFetchComplete: " + profile.getData().getProfile().getMobileNo());
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerKYCUploadActivity.this);
        }
    };

    //submit
    public void buttonClick(View view) {
        if (uploadProofList.size()>0) {
            getProfile();
        } else {
            CommonUtils.showToasty(this, "Please upload proof", Params.TOASTY_WARN);
        }
    }


    public void ShowImage(Bitmap bitmap, ImageView imageView) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        Glide.with(this)
                .load(stream.toByteArray())
//                .asBitmap()
                .error(R.drawable.ic_history)
                .placeholder(R.color.colorBlue)
//                .transform(new )
                .into(imageView);
    }

    public void ShowImageURI(Uri outputFileUri, ImageView imageView) {
        Glide.with(this)
                .load(outputFileUri)
                .error(R.drawable.ic_history)
                .placeholder(R.color.colorBlue)
                .into(imageView);
    }


    public boolean validateForm(  List<EditProfielPayload.UploadProof> uploadProofList)
    {

        if(et_idproof.getText()!=null && !et_idproof.getText().toString().trim().isEmpty()) {
            if(et_address_proof.getText()!=null && !et_address_proof.getText().toString().trim().isEmpty()) {
                for (EditProfielPayload.UploadProof uploadProof : uploadProofList) {
                    if(uploadProof.getProofType().equalsIgnoreCase("id_proof"))
                    {
                        uploadProof.setName(et_idproof.getText().toString().trim());
                    }else {
                        uploadProof.setName(et_address_proof.getText().toString().trim());
                    }
                    return true;
                }
            }else {
                Toasty.warning(CustomerKYCUploadActivity.this,"Please enter the name of the Address Proof").show();
                return false;
            }
            return  false;
        }else {
            Toasty.warning(CustomerKYCUploadActivity.this,"Please enter the name of the ID Proof").show();
            return false;
        }

    }


    public void update() {

        if(validateForm(uploadProofList)) {
            editProfielPayload = new EditProfielPayload();
            EditProfielPayload.BillingAddress billingAddress = new EditProfielPayload.BillingAddress();
            editProfielPayload.setUploadProof(uploadProofList);
            String address = profile.getData().getProfile().getBillingAddress().getAddr();
            billingAddress.setAddr(address);
            billingAddress.setPincode(profile.getData().getProfile().getPincode());
            editProfielPayload.setBillingAddress(billingAddress);
            editProfielPayload.setDob(profile.getData().getProfile().getDob());
            editProfielPayload.setEmail(profile.getData().getProfile().getEmail());
            editProfielPayload.setFname(profile.getData().getProfile().getFname());
            editProfielPayload.setLname(profile.getData().getProfile().getLname());
            editProfielPayload.setMobileNo(profile.getData().getProfile().getMobileNo());
            editProfielPayload.setFormno(profile.getData().getProfile().getFormno());
            editProfielPayload.setPincode(profile.getData().getProfile().getPincode());
            editProfile(editProfielPayload);
            Log.d(TAG, "update: sucess");
        }
    }


    public void editProfile(EditProfielPayload editProfielPayload) {
        Call<JsonObject> call = apiInterface.updateProfile(UtilPref.getAuth(CustomerKYCUploadActivity.this), subid, editProfielPayload);
        apiRequest.request(call, fetchDataListeneredit);
    }


    FetchDataListener fetchDataListeneredit = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);

            CommonUtils.showSuccessMessage(CustomerKYCUploadActivity.this,data,"Updated Successfully");
           // Toasty.success(CustomerKYCUploadActivity.this, "Updated Successfully").show();
            CustomerKYCUploadActivity.this.finish();


        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(CustomerKYCUploadActivity.this,msg,"Something went wrong, Please try  sometime  later");
            //Toasty.error(CustomerKYCUploadActivity.this, msg).show();

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerKYCUploadActivity.this);


        }
    };


    // Converting File to Base64.encode String type using Method
    public String getStringFile(File file1) {
        InputStream inputStream = null;
        String encodedFile = "", lastVal;
        try {
            inputStream = new FileInputStream(file1.getAbsolutePath());

            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile = output.toString();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        Log.d(TAG, "getStringFile: "+lastVal);
        return lastVal;
    }

    // Converting Bitmap image to Base64.encode String type
    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
