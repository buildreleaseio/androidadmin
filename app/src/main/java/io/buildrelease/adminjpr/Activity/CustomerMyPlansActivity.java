package io.buildrelease.adminjpr.Activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.MyPlansAdapter;
import io.buildrelease.adminjpr.Model.CustomerMyPlansResponse;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class CustomerMyPlansActivity extends AppCompatActivity {

    APIInterface apiInterface;
    int accno;
    List<CustomerMyPlansResponse.DataBean> datumArrayListMyPlans =new ArrayList<>();
    int pageno =1 ,pageCount = 50;
    boolean newData = true;
    LinearLayoutManager layoutManager;
    boolean isScrolled = false;
    String totalPageCount = "0";
    LinearLayout ll_myplans;
    LinearLayout linearLayoutSubscription;
    TextView tv_sub_nodata;
    RecyclerView rv_my_plans;
    MyPlansAdapter myPlansAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_plans);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("My Plans");
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        if(getIntent().getExtras()!=null)
        {
            accno=Integer.parseInt(getIntent().getStringExtra("accno"));
        }
        connnectUI();
        getMyPlansData();
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CustomerMyPlansActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }





    public void connnectUI()
    {

         myPlansAdapter = new MyPlansAdapter(CustomerMyPlansActivity.this,datumArrayListMyPlans);
        tv_sub_nodata = findViewById(R.id.tv_sub_nodata);
        linearLayoutSubscription = findViewById(R.id.insert);
        rv_my_plans=findViewById(R.id.rv_my_plans);
        ll_myplans=findViewById(R.id.ll_my_plans);
        layoutManager = new LinearLayoutManager(CustomerMyPlansActivity.this);
        rv_my_plans.setLayoutManager(layoutManager);
        rv_my_plans.setAdapter(myPlansAdapter);
        rv_my_plans.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolled = true;
                    loadmoreData();
                }
            }
        });

    }


    public void getMyPlansData() {
        ApiRequest apiRequest = new ApiRequest(CustomerMyPlansActivity.this);
        Call<JsonObject> call = apiInterface.getMyPlans(UtilPref.getAuth(CustomerMyPlansActivity.this),accno,pageno,pageCount);
        apiRequest.request(call, getactiveListDataListener);
    }

    FetchDataPaginatedListener getactiveListDataListener =new FetchDataPaginatedListener() {


        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            CustomerMyPlansResponse activeAccountData = new Gson().fromJson(data, CustomerMyPlansResponse.class);
            totalPageCount = pageCount;
            inflatePairingAdapter(activeAccountData);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(CustomerMyPlansActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerMyPlansActivity.this);
        }
    };

    public void inflatePairingAdapter(CustomerMyPlansResponse activeAccountData){

        if(newData) {
            datumArrayListMyPlans.clear();
            datumArrayListMyPlans.addAll(activeAccountData.getData());
            myPlansAdapter.notifyDataSetChanged();
        }else
        {
            datumArrayListMyPlans.addAll(activeAccountData.getData());
            myPlansAdapter.notifyDataSetChanged();
        }
        if(activeAccountData.getData()!=null && activeAccountData.getData().size()==0)
        {
            tv_sub_nodata.setVisibility(View.VISIBLE);
            rv_my_plans.setVisibility(View.GONE);
        }
        else
        {
            tv_sub_nodata.setVisibility(View.GONE);
            rv_my_plans.setVisibility(View.VISIBLE);
        }
    }

    public void loadmoreData() {
        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();
        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;
            if(totalPageCount!=null)
            {
                int total = Integer.parseInt(totalPageCount);
                Log.d(TAG, "loadmoreData: total"+total+" pageno "+pageno + " pagecount "+pageCount);
                if(total>pageno*pageCount) {
                    pageno += 1;
                    newData = false;
                    getMyPlansData();
                }
            }
        }
    }
}
