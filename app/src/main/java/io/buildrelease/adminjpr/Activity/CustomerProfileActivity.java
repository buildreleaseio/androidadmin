package io.buildrelease.adminjpr.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.buildrelease.adminjpr.Model.CustomerProfileData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class CustomerProfileActivity extends AppCompatActivity {


    APIInterface apiInterface;
    String TAG = getClass().getSimpleName();
    ApiRequest apiRequest = new ApiRequest(this);
    TextView tv_name, tv_customer_id, tv_Gender ,tv_email ,tv_mobile ,tv_address ,tv_birthdate,tv_formno;
    String subid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_profile);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        if(getIntent().getExtras()!=null)
        {
            subid = getIntent().getExtras().getString("subid");
            connectUI();
            getProfile();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

    }

    public void connectUI()
    {
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("My Profile");
        }
        tv_name = findViewById(R.id.tv_name);
        tv_customer_id = findViewById(R.id.tv_customer_id);
        tv_Gender = findViewById(R.id.tv_Gender);
        tv_email = findViewById(R.id.tv_email);
        tv_mobile = findViewById(R.id.tv_mobile);
        tv_address = findViewById(R.id.tv_address);
        tv_birthdate = findViewById(R.id.tv_birthdate);
        tv_formno = findViewById(R.id.tv_formno);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CustomerProfileActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void getProfile()
    {
        Call<JsonObject> call = apiInterface.getCustomerProfile(UtilPref.getAuth(CustomerProfileActivity.this),subid);
        apiRequest.request(call,fetchDataListener2);
    }


    FetchDataListener fetchDataListener2 = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);

            CustomerProfileData profile = new Gson().fromJson(data,CustomerProfileData.class);
            updateProfile(profile);
            Log.d(TAG, "onFetchComplete: "+profile.getData().getProfile().getMobileNo());
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerProfileActivity.this);


        }
    };

    private void updateProfile(CustomerProfileData profile) {

        if(profile.getData().getProfile().getEmail()!=null)
        {
            tv_email.setText(profile.getData().getProfile().getEmail());
        }
        tv_name.setText(profile.getData().getProfile().getName());
        tv_formno.setText(profile.getData().getProfile().getFormno()+"");
        tv_customer_id.setText(profile.getData().getProfile().getCustomerId()+"");
        tv_address.setText(profile.getData().getProfile().getBillingAddress().getAddr()+ " " +profile.getData().getProfile().getBillingAddress().getPincode());
        tv_mobile.setText(profile.getData().getProfile().getMobileNo());
        tv_Gender.setText(profile.getData().getProfile().getGender());
        tv_birthdate.setText(CommonUtils.dateFormat(profile.getData().getProfile().getDob()));

    }


}
