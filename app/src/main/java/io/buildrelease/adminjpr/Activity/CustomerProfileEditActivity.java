package io.buildrelease.adminjpr.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.buildrelease.adminjpr.Model.CustomerProfileData;
import io.buildrelease.adminjpr.Model.EditProfielPayload;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class CustomerProfileEditActivity extends AppCompatActivity {

    APIInterface apiInterface;
    String TAG = getClass().getSimpleName();
    ApiRequest apiRequest = new ApiRequest(this);
    EditText et_firstname,et_lastname,et_email,et_mobile,et_address,et_pincode,et_formno,et_dob;
    String subid;

    CustomerProfileData profile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_profile_edit);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Profile Edit");
        }
        if(getIntent().getExtras()!=null)
        {
            subid = getIntent().getExtras().getString("subid");
            connectUI();
            getProfile();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CustomerProfileEditActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void connectUI()
    {
        et_firstname = findViewById(R.id.et_firstname);
        et_lastname = findViewById(R.id.et_lastname);
        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
        et_address = findViewById(R.id.et_address);
        et_pincode = findViewById(R.id.et_pincode);
        et_formno = findViewById(R.id.et_formno);
        et_dob = findViewById(R.id.et_dob);


    }


    public void getProfile()
    {
        Call<JsonObject> call = apiInterface.getCustomerProfile(UtilPref.getAuth(CustomerProfileEditActivity.this),subid);
        apiRequest.request(call,fetchDataListener2);
    }


    FetchDataListener fetchDataListener2 = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);

             profile = new Gson().fromJson(data,CustomerProfileData.class);
            updateProfile(profile);
            Log.d(TAG, "onFetchComplete: "+profile.getData().getProfile().getMobileNo());
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerProfileEditActivity.this);


        }
    };

    private void updateProfile(CustomerProfileData profile) {

        if(profile.getData().getProfile().getEmail()!=null)
        {
            et_email.setText(profile.getData().getProfile().getEmail());
        }
        et_firstname.setText(profile.getData().getProfile().getFname());
        et_lastname.setText(profile.getData().getProfile().getLname());
        et_pincode.setText(profile.getData().getProfile().getPincode());
        et_formno.setText(profile.getData().getProfile().getFormno()+"");
        et_address.setText(profile.getData().getProfile().getBillingAddress().getAddr()+ " " +profile.getData().getProfile().getBillingAddress().getPincode());
        et_mobile.setText(profile.getData().getProfile().getMobileNo());
        et_dob.setText(profile.getData().getProfile().getDob());

    }

    public void editProfile(EditProfielPayload editProfielPayload)
    {
        Call<JsonObject> call = apiInterface.updateProfile(UtilPref.getAuth(CustomerProfileEditActivity.this),subid,editProfielPayload);
        apiRequest.request(call,fetchDataListeneredit);
    }


    FetchDataListener fetchDataListeneredit = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);


            CommonUtils.showSuccessMessage(CustomerProfileEditActivity.this,data,"Updated Successfully");

           // Toasty.success(CustomerProfileEditActivity.this,"Updated Successfully").show();
            CustomerProfileEditActivity.this.finish();


        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(CustomerProfileEditActivity.this,msg,"Something went wrong, Please try  sometime  later");
         ///   Toasty.error(CustomerProfileEditActivity.this,msg).show();

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerProfileEditActivity.this);


        }
    };


    public void update(View view) {


        EditProfielPayload editProfielPayload = new EditProfielPayload();
        EditProfielPayload.BillingAddress billingAddress = new EditProfielPayload.BillingAddress();
        EditProfielPayload.UploadProof uploadProof = new EditProfielPayload.UploadProof();
        billingAddress.setAddr(et_address.getText().toString());
        billingAddress.setPincode(et_pincode.getText().toString());

        editProfielPayload.setBillingAddress(billingAddress);
        editProfielPayload.setDob(et_dob.getText().toString());
        editProfielPayload.setEmail(et_email.getText().toString());
        editProfielPayload.setFname(et_firstname.getText().toString());
        editProfielPayload.setLname(et_lastname.getText().toString());
        editProfielPayload.setMobileNo(et_mobile.getText().toString());
        editProfielPayload.setFormno(et_mobile.getText().toString());
        editProfielPayload.setPincode(et_pincode.getText().toString());

        editProfile(editProfielPayload);

    }
}
