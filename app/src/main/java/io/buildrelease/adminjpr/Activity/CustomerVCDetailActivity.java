package io.buildrelease.adminjpr.Activity;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.buildrelease.adminjpr.Model.CustomerDetail;
import io.buildrelease.adminjpr.Model.Remarks;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class CustomerVCDetailActivity extends AppCompatActivity {


    APIInterface apiInterface;
    String TAG = getClass().getSimpleName();
    ApiRequest apiRequest = new ApiRequest(CustomerVCDetailActivity.this);
    String name,custid;
    CustomerDetail.AccountList accountList;
    TextView tv_customer_name,tv_status,tv_vc_number,tv_customer_id,tv_current_package,tv_stb_number;
    LinearLayout ll_renew,ll_addon,ll_cms,ll_suspend,ll_complaint,ll_remove_bo,ll_refresh,ll_change_bouque,ll_resume,ll_my_plans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_vcdetail);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Account Details");
        }

        connectUI();
        if(getIntent().getExtras()!=null)
        {
            name = getIntent().getExtras().getString("name","");
            custid = getIntent().getExtras().getString("id","");
            String data = getIntent().getExtras().getString("data",null);
            if(data!=null)
            {
                accountList = new Gson().fromJson(data,CustomerDetail.AccountList.class);
                updateUI(accountList);
            }

        }



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                CustomerVCDetailActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void connectUI()
    {
        tv_customer_name = findViewById(R.id.tv_customer_name);
        tv_status = findViewById(R.id.tv_status);
        tv_vc_number = findViewById(R.id.tv_vc_number);
        tv_customer_id = findViewById(R.id.tv_customer_id);
        tv_current_package = findViewById(R.id.tv_current_package);
        tv_stb_number = findViewById(R.id.tv_stb_number);
        ll_renew = findViewById(R.id.ll_renew);
        ll_addon = findViewById(R.id.ll_addon);
        ll_cms = findViewById(R.id.ll_cms);
        ll_suspend = findViewById(R.id.ll_suspend);
        ll_complaint = findViewById(R.id.ll_complaint);
        ll_remove_bo = findViewById(R.id.ll_remove_bo);
        ll_refresh = findViewById(R.id.ll_refresh);
        ll_change_bouque = findViewById(R.id.ll_change_bouque);
        ll_resume = findViewById(R.id.ll_resume);
        ll_my_plans = findViewById(R.id.ll_my_plans);



        ll_renew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(accountList!=null) {
                    Intent intent = new Intent(CustomerVCDetailActivity.this, RenewalActivity.class);
                    intent.putExtra("vc", accountList.getSmartcardno());
                    intent.putExtra("stbno", accountList.getStbno());
                    intent.putExtra("accno", accountList.getAccountId());
                    CustomerVCDetailActivity.this.startActivity(intent);
                }
            }
        });

        ll_my_plans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(accountList!=null) {
                    Intent intent = new Intent(CustomerVCDetailActivity.this, CustomerMyPlansActivity.class);
                    intent.putExtra("accno", accountList.getAccountId());
                    CustomerVCDetailActivity.this.startActivity(intent);
                }
            }
        });

        ll_resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReplyDialog("resume");
            }
        });


        ll_addon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(CustomerVCDetailActivity.this, AddonListActivity.class);
                intent.putExtra("vc", accountList.getSmartcardno());
                intent.putExtra("stbno", accountList.getStbno());
                intent.putExtra("accno", accountList.getAccountId());
                intent.putExtra("brandid",accountList.getStbbrandId()+"");
                Log.d("brandid", "onClick: "+accountList.getStbbrandId());
                CustomerVCDetailActivity.this.startActivity(intent);
            }
        });

        ll_cms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(CustomerVCDetailActivity.this,CMSActivity.class);
                intent.putExtra("accountid",accountList.getAccountId());
                startActivity(intent);
            }
        });

        ll_suspend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReplyDialog("suspend");
            }
        });

        ll_complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(CustomerVCDetailActivity.this, CreateComplainActivity.class);
                intent.putExtra("vcno", accountList.getSmartcardno());
                intent.putExtra("stbno", accountList.getStbno());
                intent.putExtra("accno", accountList.getAccountId());
                intent.putExtra("brandid",accountList.getStbbrandId()+"");
                CustomerVCDetailActivity.this.startActivity(intent);

            }
        });
        ll_remove_bo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerVCDetailActivity.this, SingleRemoveBouquetsActivity.class);
                intent.putExtra("vc", accountList.getSmartcardno());
                intent.putExtra("stbno", accountList.getStbno());
                intent.putExtra("accno", accountList.getAccountId());
                intent.putExtra("brandid",accountList.getStbbrandId()+"");
                CustomerVCDetailActivity.this.startActivity(intent);
            }
        });

        ll_change_bouque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CustomerVCDetailActivity.this, CustomerChangePackActivity.class);
                intent.putExtra("vc", accountList.getSmartcardno());
                intent.putExtra("stbno", accountList.getStbno());
                intent.putExtra("accno", accountList.getAccountId());
                intent.putExtra("brandid",accountList.getStbbrandId()+"");
                CustomerVCDetailActivity.this.startActivity(intent);
            }
        });


        ll_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Call<JsonObject> call = apiInterface.refreshCommand(UtilPref.getAuth(CustomerVCDetailActivity.this),accountList.getAccountId());
                apiRequest.request(call, fetchDataListenerRefresh);
            }

            FetchDataListener fetchDataListenerRefresh = new FetchDataListener() {
                @Override
                public void onFetchComplete(String data) {
                    RequestQueueService.cancelProgressDialog();
                    Log.d(TAG, "onFetchComplete:  "+data);
                    CommonUtils.showSuccessMessage(CustomerVCDetailActivity.this,data,"Account Refreshed Sucessfully!");


                }

                @Override
                public void onFetchFailure(String msg) {
                    RequestQueueService.cancelProgressDialog();
                    CommonUtils.showFailureMessage(CustomerVCDetailActivity.this,msg,"Something went wrong, Please try  sometime  later");
                }

                @Override
                public void onFetchStart() {
                    RequestQueueService.showProgressDialog(CustomerVCDetailActivity.this);
                }
            };
        });


    }

    public void updateUI(CustomerDetail.AccountList accountList)
    {
        tv_current_package.setText(accountList.getBoxtypeLbl());
        tv_vc_number.setText(accountList.getSmartcardno());
        tv_customer_name.setText(name);
        tv_stb_number.setText(accountList.getStbno());
        tv_status.setText(accountList.getStatusLbl());
        tv_customer_id.setText(custid);
        if(accountList.getStatusLbl().trim().equalsIgnoreCase("active"))
        {
            tv_status.setTextColor(CustomerVCDetailActivity.this.getResources().getColor(R.color.colorGreen));
        }
        else {
            tv_status.setTextColor(CustomerVCDetailActivity.this.getResources().getColor(R.color.colorRed));
        }
    }



    public void showReplyDialog(final String type)
    {
        final AlertDialog dialogBuilder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.commnt_dialog, null);

        final EditText editText = (EditText) dialogView.findViewById(R.id.edt_comment);
        Button button1 = (Button) dialogView.findViewById(R.id.buttonSubmit);
        Button button2 = (Button) dialogView.findViewById(R.id.buttonCancel);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.dismiss();
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!editText.getText().toString().trim().isEmpty()) {
                    Remarks remarks = new Remarks();
                    remarks.setRemark(editText.getText().toString());
                    Call<JsonObject> call=null;
                    if(type.equals("suspend"))
                    {
                       call = apiInterface.suspend( UtilPref.getAuth(CustomerVCDetailActivity.this), accountList.getAccountId(),remarks);
                    }
                    else
                    {
                        call = apiInterface.singleResume( UtilPref.getAuth(CustomerVCDetailActivity.this), accountList.getAccountId(),remarks);
                    }
                    apiRequest.request(call, fetchDataListenerSuspend);
                    dialogBuilder.dismiss();
                }else
                {
                    Toast.makeText(CustomerVCDetailActivity.this,"Please enter your remark",Toast.LENGTH_SHORT).show();
                }
                // DO SOMETHINGS

            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();
    }


    FetchDataListener fetchDataListenerSuspend = new FetchDataListener() {

        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete:  "+data);
            CommonUtils.showSuccessMessage(CustomerVCDetailActivity.this,data,"Action Successful");
//            CommonUtils.showToasty(CustomerVCDetailActivity.this,"Account Suspended", Params.TOASTY_SUCCESS);
            CustomerVCDetailActivity.this.finish();
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(CustomerVCDetailActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(CustomerVCDetailActivity.this);
        }
    };
}
