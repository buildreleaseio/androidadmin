package io.buildrelease.adminjpr.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.ActiveListAdapter;
import io.buildrelease.adminjpr.Adapters.ClosedComplaintAdapter;
import io.buildrelease.adminjpr.Adapters.Expire7DaysListAdapter;
import io.buildrelease.adminjpr.Adapters.InactiveListAdapter;
import io.buildrelease.adminjpr.Adapters.OpenComplaintAdapter;
import io.buildrelease.adminjpr.Adapters.RecentlyExpiredListAdapter;
import io.buildrelease.adminjpr.Adapters.SuspendedListAdapter;
import io.buildrelease.adminjpr.Model.ActiveAccountData;
import io.buildrelease.adminjpr.Model.ClosedComplaintsData;
import io.buildrelease.adminjpr.Model.Expire7DaysData;
import io.buildrelease.adminjpr.Model.InactiveAccountData;
import io.buildrelease.adminjpr.Model.OpenComplaintsData;
import io.buildrelease.adminjpr.Model.RecentlyExpiredData;
import io.buildrelease.adminjpr.Model.SuspendedAccountData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class DashboardListingActivity extends AppCompatActivity {
    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    static APIInterface apiInterface;
    TextView no_data_tv;
    static String listingType = "";
    static ArrayList<String> accountIdList = new ArrayList<>();
    boolean isScrolled = false;
    boolean loadNewData = true;
    int page_no = 1;
    int per_page = 100;
    public static Button bulk_button;
    private LinearLayoutManager layoutManager;
    List<ActiveAccountData.Datum> datumArrayListActive =new ArrayList<>();
    List<InactiveAccountData.Datum> datumArrayListInactive =new ArrayList<>();
    List<SuspendedAccountData.Datum> datumArrayListSuspended =new ArrayList<>();
    List<Expire7DaysData.Datum> datumArrayListExp7 =new ArrayList<>();
    List<RecentlyExpiredData.Datum> datumArrayListRExp =new ArrayList<>();
    List<OpenComplaintsData.Datum> datumArrayListOpenC =new ArrayList<>();
    List<ClosedComplaintsData.Datum> datumArrayListClosedC =new ArrayList<>();
    private String comment="";
    static DashboardListingActivity dashboardListingActivity;
    ActionBar toolbar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_listing);
        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView_activeList);
        no_data_tv = findViewById(R.id.no_data_tv);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        bulk_button = findViewById(R.id.bulk_button);
        dashboardListingActivity=this;
        if(getSupportActionBar()!=null) {
            toolbar=getSupportActionBar();
            toolbar.setDisplayHomeAsUpEnabled(true);
        }



        getIntentData();
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//
//                }
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if(dy>0){
//                    isScrolled = true;
//                    loadmoreData();
//                }
//            }
//        });



        bulk_button.setVisibility(View.GONE);
        bulk_button.setOnClickListener(bulkButtonListener);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("listingType")) {
                listingType = intent.getStringExtra("listingType");
            }
        }
        getListingData();
    }


    //    -----------------------Active Lisitng---------
    public void getListingData() {
        ApiRequest apiRequest = new ApiRequest(DashboardListingActivity.this);
        Call<JsonObject> call = null;
        switch (listingType) {
            case Params.LIST_ACTIVE:
                call = apiInterface.getActiveAccountList(UtilPref.getAuth(DashboardListingActivity.this),page_no,per_page);
                apiRequest.request(call, getListDataListener);
                break;
            case Params.LIST_INACTIVE:
                call = apiInterface.getInactiveAccountList(UtilPref.getAuth(DashboardListingActivity.this),page_no,per_page);
                apiRequest.request(call, getListDataListener);
                break;
//            case Params.LIST_EXPIRED:
////                call = apiInterface.getInactiveAccountList(UtilPref.getAuth(DashboardListingActivity.this));
////                apiRequest.request(call, getListDataListener);
//                break;
            case Params.LIST_EXPIREDTODAY:
//                call = apiInterface.getExpiredTodayAccountList(UtilPref.getAuth(DashboardListingActivity.this), CommonUtils.getCurrentDate() + " 00:00;00", CommonUtils.getCurrentDate() + " 23:59:59");
                call = apiInterface.getExpiredtList(UtilPref.getAuth(DashboardListingActivity.this),page_no,per_page);
                apiRequest.request(call, getListDataListener);
                break;
            case Params.LIST_EXPIRED7DAYS:
//                call = apiInterface.get7DaysExpireAccountList(UtilPref.getAuth(DashboardListingActivity.this), CommonUtils.getCurrentDateTime(), CommonUtils.getCurrentDate(7, "future")+" 23:59:59",page_no,per_page);
//                apiRequest.request(call, getListDataListener);
                break;
            case Params.LIST_SUSPENDED:
                call = apiInterface.getSuspendedAccountList(UtilPref.getAuth(DashboardListingActivity.this),page_no,per_page);
                apiRequest.request(call, getListDataListener);
                break;
            case Params.LIST_OPEN_COMPLAINTS:
//                call = apiInterface.getOpenComplaintlist(UtilPref.getAuth(DashboardListingActivity.this));
//                apiRequest.request(call, getListDataListener);
                break;
            case Params.LIST_CLOSED_COMPLAINTS:
                call = apiInterface.getClosedComplaintlist(UtilPref.getAuth(DashboardListingActivity.this),page_no,per_page);
                apiRequest.request(call, getListDataListener);
                toolbar.setTitle("Closed Complaints");
                break;


            default:
                no_data_ll.setVisibility(View.VISIBLE);
                break;
        }


    }


    FetchDataListener getListDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            parseData(data);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(DashboardListingActivity.this, msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(DashboardListingActivity.this);
        }
    };

    private void parseData(String data) {
        switch (listingType) {
            case Params.LIST_ACTIVE:
                try {
                    ActiveAccountData activeAccountData = new Gson().fromJson(data, ActiveAccountData.class);
                    datumArrayListActive.addAll(activeAccountData.getData());
                    if (activeAccountData.getData().size() > 0) {
                        recyclerView.setVisibility(View.VISIBLE);
                        no_data_ll.setVisibility(View.GONE);
                        inflateActiveAdapter(activeAccountData.getData());
                    }
                    else
                    {
                        recyclerView.setVisibility(View.GONE);
                        no_data_ll.setVisibility(View.VISIBLE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            case Params.LIST_INACTIVE:
                InactiveAccountData inactiveAccountData = new Gson().fromJson(data, InactiveAccountData.class);
                if (inactiveAccountData.getData().size() > 0)
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    no_data_ll.setVisibility(View.GONE);
                    inflateInactiveAdapter(inactiveAccountData);
                }
                else
                {
                    recyclerView.setVisibility(View.GONE);
                    no_data_ll.setVisibility(View.VISIBLE);
                }
                break;
            case Params.LIST_EXPIREDTODAY:
                RecentlyExpiredData recentlyExpiredData = new Gson().fromJson(data, RecentlyExpiredData.class);
                if (recentlyExpiredData.getData().size() > 0)
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    no_data_ll.setVisibility(View.GONE);
                    inflateRecentlyExpiredAdapter(recentlyExpiredData);
                }
                else
                {
                    recyclerView.setVisibility(View.GONE);
                    no_data_ll.setVisibility(View.VISIBLE);
                }
                break;
            case Params.LIST_EXPIRED7DAYS:
                Expire7DaysData expire7DaysData = new Gson().fromJson(data, Expire7DaysData.class);
                if (expire7DaysData.getData().size() > 0)
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    no_data_ll.setVisibility(View.GONE);
                    inflateExpiration7Adapter(expire7DaysData);
                }
                else
                {
                    recyclerView.setVisibility(View.GONE);
                    no_data_ll.setVisibility(View.VISIBLE);
                }
                break;
            case Params.LIST_SUSPENDED:
                SuspendedAccountData suspendedAccountData = new Gson().fromJson(data, SuspendedAccountData.class);
                if (suspendedAccountData.getData().size() > 0)
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    no_data_ll.setVisibility(View.GONE);
                    inflateSuspendedAdapter(suspendedAccountData);
                }
                else
                {
                    recyclerView.setVisibility(View.GONE);
                    no_data_ll.setVisibility(View.VISIBLE);
                }
                break;
            case Params.LIST_OPEN_COMPLAINTS:
                OpenComplaintsData openComplaintsData = new Gson().fromJson(data, OpenComplaintsData.class);
                if (openComplaintsData.getData().size() > 0)
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    no_data_ll.setVisibility(View.GONE);
                    inflateOpenComplaintAdapter(openComplaintsData);
                }
                else
                {
                    recyclerView.setVisibility(View.GONE);
                    no_data_ll.setVisibility(View.VISIBLE);
                }
                break;
            case Params.LIST_CLOSED_COMPLAINTS:
                ClosedComplaintsData closedComplaintsData = new Gson().fromJson(data, ClosedComplaintsData.class);
                if (closedComplaintsData.getData().size() > 0)
                {
                    recyclerView.setVisibility(View.VISIBLE);
                    no_data_ll.setVisibility(View.GONE);
                    inflateCloseComplaintAdapter(closedComplaintsData);
                }
                else
                {
                    recyclerView.setVisibility(View.GONE);
                    no_data_ll.setVisibility(View.VISIBLE);
                }
                break;
            default:
                no_data_ll.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                break;
        }
    }


    public void inflateActiveAdapter(List<ActiveAccountData.Datum> datumArrayListActive) {
        ActiveListAdapter mAdapter =
                new ActiveListAdapter(
                        datumArrayListActive,
                        getSupportFragmentManager(),
                        recyclerView,
                        DashboardListingActivity.this);
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(DashboardListingActivity.this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();
    }

    public void inflateInactiveAdapter(InactiveAccountData inactiveAccountData) {
        InactiveListAdapter mAdapter =
                new InactiveListAdapter(
//                        TODO CHECK FOR VALID DECIMAL
                        inactiveAccountData,
                        getSupportFragmentManager(),
                        recyclerView,
                        DashboardListingActivity.this);
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(DashboardListingActivity.this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();
    }

    public void inflateRecentlyExpiredAdapter(RecentlyExpiredData recentlyExpiredData) {
        RecentlyExpiredListAdapter mAdapter =
                new RecentlyExpiredListAdapter(
//                        TODO CHECK FOR VALID DECIMAL
                        recentlyExpiredData,
                        getSupportFragmentManager(),
                        recyclerView,
                        DashboardListingActivity.this);
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(DashboardListingActivity.this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();
    }

    public void inflateSuspendedAdapter(SuspendedAccountData suspendedAccountData) {
        SuspendedListAdapter mAdapter =
                new SuspendedListAdapter(
//
                        suspendedAccountData,
                        getSupportFragmentManager(),
                        recyclerView,
                        DashboardListingActivity.this);
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(DashboardListingActivity.this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();
    }

    public void inflateExpiration7Adapter(Expire7DaysData expire7DaysData) {
        Expire7DaysListAdapter mAdapter =
                new Expire7DaysListAdapter(
//                        TODO CHECK FOR VALID DECIMAL
                        expire7DaysData,
                        getSupportFragmentManager(),
                        recyclerView,
                        DashboardListingActivity.this);
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(DashboardListingActivity.this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();
    }

    public void inflateOpenComplaintAdapter(OpenComplaintsData openComplaintsData) {
        OpenComplaintAdapter mAdapter =
                new OpenComplaintAdapter(
//                        TODO CHECK FOR VALID DECIMAL
                        openComplaintsData,
                        getSupportFragmentManager(),
                        recyclerView,
                        DashboardListingActivity.this);
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(DashboardListingActivity.this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();
    }

    public void inflateCloseComplaintAdapter(ClosedComplaintsData closedComplaintsData) {
        ClosedComplaintAdapter mAdapter =
                new ClosedComplaintAdapter(
                        closedComplaintsData,
                        getSupportFragmentManager(),
                        recyclerView,
                        DashboardListingActivity.this);
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        layoutManager = new LinearLayoutManager(DashboardListingActivity.this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();
    }




    public void loadmoreData() {

        //   Log.d(TAG, "onScrolled: " + isScrolled);
        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();
        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;
//            if (notificationResponse.getTotalCount() > per_page * page_no) {
                page_no += 1;
                loadNewData = false;
                getListingData();
                
                
//            } else {
//                Log.d(TAG, "onScrolled: no data to fetch");
//            }


        }

    }




//    merging objects
//    @SuppressWarnings("unchecked")
//    public static <T> T mergeObjects(T first, T second) throws IllegalAccessException, InstantiationException {
//        Class<?> clazz = first.getClass();
//        Field[] fields = clazz.getDeclaredFields();
//        Object returnValue = clazz.newInstance();
//        try {
//            for (Field field : fields) {
//                field.setAccessible(true);
//                Object value1 = field.get(first);
//                Object value2 = field.get(second);
//                Object value = (value1 != null) ? value1 : value2;
//                field.set(returnValue, value);
//            }
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        return (T) returnValue;
//    }

    public static void bulkOperation(String state, ArrayList<String> accountIdJsonList) {
        listingType = state;
        accountIdList = accountIdJsonList;
        if(state.equalsIgnoreCase(Params.LIST_ACTIVE)){
            if(bulk_button!=null) {
                bulk_button.setText("Suspend");
                bulk_button.setVisibility(View.VISIBLE);
            }
        }else if(state.equalsIgnoreCase(Params.LIST_SUSPENDED)){
            if(bulk_button!=null) {
                bulk_button.setText("Resume");
                bulk_button.setVisibility(View.VISIBLE);
            }
        }
    }




    public void showRemarkDialog(){
        try {
            final MaterialDialog.Builder builder = new MaterialDialog.Builder(DashboardListingActivity.this);
            builder.autoDismiss(true);
            builder.cancelable(true);

            builder.title("Remark")
                    .inputRange(0, 255)
                    .positiveColor(getResources().getColor(R.color.colorPrimary))
                    .negativeColor(getResources().getColor(R.color.colorPrimary))
                    .input("Type your comment here", "", false, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            performBulkOperation(input.toString());
                        }
                    })
                    .positiveText("Submit")
//                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//
//                        }
//                    })
                    .negativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            builder.cancelable(true);
                        }
                    })
                    .canceledOnTouchOutside(true)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener bulkButtonListener =new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            showRemarkDialog();
        }
    };

    private void performBulkOperation(String s) {
        try {
//            ApiRequest apiRequest = new ApiRequest();
//            Call<JsonObject> call = null;
//            JsonObject jsonObject = new JsonObject();
//// https://stackoverflow.com/questions/21090937/send-arraylist-as-value-of-json-object
//            JsonElement list = new GsonBuilder().create().toJsonTree(getIntegerArray(accountIdList));
//            jsonObject.add("account_ids",list);
//            jsonObject.addProperty("Remark", s);
//            Log.d(TAG, "performBulkOperation: "+jsonObject.toString());
//            if(listingType.equalsIgnoreCase(Params.LIST_ACTIVE)) {
//                call = apiInterface.bulkSuspend(UtilPref.getAuth(DashboardListingActivity.this),jsonObject);
//                apiRequest.request(call, bulkOperationListener);
//            }else if(listingType.equalsIgnoreCase(Params.LIST_SUSPENDED)){
//                call = apiInterface.bulkResume(UtilPref.getAuth(DashboardListingActivity.this),jsonObject);
//                apiRequest.request(call, bulkOperationListener);
//            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }




    FetchDataListener bulkOperationListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    RequestQueueService.cancelProgressDialog();
                    bulk_button.setVisibility(View.GONE);
                    getListingData();
                }
            }, 1000);

        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(DashboardListingActivity.this,msg,Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(DashboardListingActivity.this);
        }
    };


    private ArrayList<Integer> getIntegerArray(ArrayList<String> stringArray) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        for(String stringValue : stringArray) {
            try {
                //Convert String to Integer, and store it into integer array list.
                result.add(Integer.parseInt(stringValue));
            } catch(NumberFormatException nfe) {
                //System.out.println("Could not parse " + nfe);
                Log.w("NumberFormat", "Parsing failed! " + stringValue + " can not be an integer");
            }
        }
        return result;
    }

    public  void closeComplaint(String remark, String ticketno) {
        try{
            ApiRequest apiRequest = new ApiRequest(DashboardListingActivity.this);
            Call<JsonObject> call = null;
            JsonObject jsonObject =new JsonObject();
            jsonObject.addProperty("closing_remark",remark);
            jsonObject.addProperty("status",3);
            call = apiInterface.closeComplaint(UtilPref.getAuth(dashboardListingActivity),ticketno,jsonObject);
            apiRequest.request(call, closeComplaintListener);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

     FetchDataListener closeComplaintListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            listingType = Params.LIST_OPEN_COMPLAINTS;
            getListingData();
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(dashboardListingActivity,msg,Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(dashboardListingActivity);
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        dashboardListingActivity = this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                DashboardListingActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
