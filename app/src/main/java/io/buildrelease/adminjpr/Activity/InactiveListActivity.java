package io.buildrelease.adminjpr.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.InactiveListAdapter;
import io.buildrelease.adminjpr.Model.InactiveAccountData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class InactiveListActivity extends AppCompatActivity {

    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    int pageno =1 ,pageCount = 50;
    boolean newData = true;
    boolean isScrolled = false;
    InactiveListAdapter mAdapter;
    LinearLayoutManager layoutManager;
    List<InactiveAccountData.Datum> datumArrayListInactive =new ArrayList<>();
    String totalPageCount = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inactive_list);
        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView_inactiveList);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Inactive List");
        }
        connnectUI();
        getinactiveListData();
    }

    private void connnectUI() {
        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView_inactiveList);
        mAdapter = new InactiveListAdapter(datumArrayListInactive,getSupportFragmentManager(),InactiveListActivity.this);
        layoutManager = new LinearLayoutManager(InactiveListActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolled = true;
                    loadmoreData();
                }
            }
        });

    }

    private void loadmoreData() {

        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();
        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;
            if(totalPageCount!=null)
            {
                int total = Integer.parseInt(totalPageCount);
                Log.d(TAG, "loadmoreData: total"+total+" pageno "+pageno + " pagecount "+pageCount);
                if(total>pageno*pageCount) {
                    pageno += 1;
                    newData = false;
                    getinactiveListData();
                }
            }
        }
    }

    public void getinactiveListData() {
        ApiRequest apiRequest = new ApiRequest(InactiveListActivity.this);
        Call<JsonObject> call = apiInterface.getInactiveAccountList(UtilPref.getAuth(InactiveListActivity.this),pageno,pageCount);
        apiRequest.request(call, getinactiveListDataListener);
    }


    FetchDataPaginatedListener getinactiveListDataListener =new FetchDataPaginatedListener() {

        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG,"onFetchComplete: " + data);
            InactiveAccountData inactiveAccountData = new Gson().fromJson(data, InactiveAccountData.class);
            totalPageCount = pageCount;
            inflateInactiveAdapter(inactiveAccountData);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(InactiveListActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(InactiveListActivity.this);
        }
    };



    public void inflateInactiveAdapter(InactiveAccountData inactiveAccountData){
        if(newData) {
            datumArrayListInactive.clear();
            datumArrayListInactive.addAll(inactiveAccountData.getData());
            mAdapter.notifyDataSetChanged();
        }else
        {
            datumArrayListInactive.addAll(inactiveAccountData.getData());
            mAdapter.notifyDataSetChanged();
        }
        if(inactiveAccountData.getData()!=null && inactiveAccountData.getData().size()==0)
        {
            no_data_ll.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            no_data_ll.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                InactiveListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
