package io.buildrelease.adminjpr.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.buildrelease.adminjpr.Model.Login;
import io.buildrelease.adminjpr.Model.LoginResponse;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;


public class LoginActivity extends AppCompatActivity {

    APIInterface apiInterface;
    String TAG = getClass().getSimpleName();
    ApiRequest apiRequest = new ApiRequest(LoginActivity.this);
    ImageView company_logo;
    EditText username,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        company_logo = findViewById(R.id.company_logo);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        setLoginStatus();
        if(getSupportActionBar()!=null)
        {
            getSupportActionBar().hide();
        }
        CommonUtils.displayImageResource(R.drawable.ic_jpr,LoginActivity.this,company_logo);
//        login();

        //username.setText("testinglmo");
        ///>......password.setText("123456");
    }

    private void setLoginStatus() {
        if(UtilPref.getPassword(LoginActivity.this)!=null)
        {
            username.setText(UtilPref.getUserName(LoginActivity.this));
            password.setText(UtilPref.getPassword(LoginActivity.this));
        }
        else
        {
            username.setText("");
            password.setText("");
        }
    }


//    public void login()
//    {
//        Login login = new Login();
//        Login.LoginForm loginForm = new Login.LoginForm();
//        loginForm.setUsername(username.getText().toString());
//        loginForm.setPassword(password.getText().toString());
//        login.setLoginForm(loginForm);
//
//        Call<JsonObject> call = null;
//        call = apiInterface.login(new JsonParser().parse(new Gson().toJson(login)).getAsJsonObject());
//        apiRequest.request(call,fetchDataListener);
//
//
//    }

    public void requestLogin()
    {
        if(!username.getText().toString().trim().isEmpty()
                && !password.getText().toString().trim().isEmpty()) {
            Login login = new Login();
            Login.LoginForm loginForm = new Login.LoginForm();
            loginForm.setUsername(username.getText().toString().trim());
            loginForm.setPassword(password.getText().toString().trim());
            login.setLoginForm(loginForm);

            Call<JsonObject> call = null;
            call = apiInterface.login(new JsonParser().parse(new Gson().toJson(login)).getAsJsonObject());
            apiRequest.request(call, fetchDataListener);
        }else{
            CommonUtils.showToasty(LoginActivity.this,"All fields are compulsory",Params.TOASTY_WARN);
        }


    }

    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);
            LoginResponse response = new Gson().fromJson(data,LoginResponse.class);
            UtilPref.setUserName(LoginActivity.this,response.getData().getUsername());
            UtilPref.setPassword(LoginActivity.this,password.getText().toString().trim());
            UtilPref.setEmail(LoginActivity.this,response.getData().getEmail());
            String accessToken = "Bearer "+response.getData().getAccessToken();
            UtilPref.setOperatorID(LoginActivity.this,response.getData().getOperatorId());
            UtilPref.setAuth(LoginActivity.this,accessToken);
            UtilPref.setSubID(LoginActivity.this,response.getData().getSubscriberId());
            UtilPref.setBalance(LoginActivity.this,response.getData().getBalance().floatValue());

            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            LoginActivity.this.finish();

        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(LoginActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(LoginActivity.this);
        }
    };

    public void callLogin(View view) {
        requestLogin();
    }
}
