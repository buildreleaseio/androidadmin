package io.buildrelease.adminjpr.Activity;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import io.buildrelease.adminjpr.Fragments.InventoryFragment;
import io.buildrelease.adminjpr.Fragments.DashBoardFragment;
import io.buildrelease.adminjpr.Fragments.WalletFragment;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;

public class MainActivity extends AppCompatActivity {


    private static ActionBar toolbar;
    private int backCount;
    boolean doubleBackToExitPressedOnce = false;
    static BottomNavigationView navigation;
    static MainActivity mainActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity = MainActivity.this;

        toolbar = getSupportActionBar();
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.setSelectedItemId(R.id.navigation_home);
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;

            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (!(currentFragment instanceof DashBoardFragment)) {
                        toolbar.setTitle("Dashboard");
                        fragment = new DashBoardFragment();
                        loadFragment(fragment);
                    }
                    return true;
                case R.id.navigation_account:
                    if (!(currentFragment instanceof WalletFragment)) {
                        toolbar.setTitle("Wallet");
                        fragment = new WalletFragment();
                        loadFragment(fragment);
                    }
                    return true;
                case R.id.navigation_history:
                    if (!(currentFragment instanceof InventoryFragment)) {
                        toolbar.setTitle("Inventory");
                        fragment = new InventoryFragment();
                        loadFragment(fragment);
                    }
                    return true;
            }


            return false;
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_icon_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.recharge:
                moveToRecharge();
                return true;

            case R.id.logout:
                CommonUtils.logout(MainActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void moveToRecharge() {
        Intent intent = new Intent(MainActivity.this,RechargeActivity.class);
        startActivity(intent);
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        FragmentManager fragmentManager = getSupportFragmentManager();
        backCount = fragmentManager.getBackStackEntryCount();
        Log.e(Params.TAG, "onBackPressed: backCount " + backCount + " Fragment " + f);
        Log.e(Params.TAG, "onBackPressed: doubleBackToExitPressedOnce " + doubleBackToExitPressedOnce);
//        handleBottomNavBackHandle(f);
        Log.d(Params.TAG, "onBackPressed:>> " + f);
        if (backCount != 1) {
            fragmentManager.popBackStack();
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                MainActivity.this.finish();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            CommonUtils.showToasty(this, "Please click BACK again to exit", Params.TOASTY_INFO);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }
    }


    public static void showBottomNav(){
        navigation.setVisibility(View.VISIBLE);
    } public static void hideBottomNav(){
        navigation.setVisibility(View.VISIBLE);
    }public static void setToolbarTitle(String title){
        toolbar.setTitle(title);
    }

}
