package io.buildrelease.adminjpr.Activity;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.JsonObject;

import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

public class OpenComplainDetailActivity extends AppCompatActivity {

    String TAG=getClass().getSimpleName();
    APIInterface apiInterface;
    private static ActionBar toolbar;
    TextView tv_name_lbl,tv_smartcard_lbl,tv_ticketno,tv_priority_lbl,tv_location_lbl;
    EditText et_complaint_remark;
    String remark;
    ApiRequest apiRequest = new ApiRequest(OpenComplainDetailActivity.this);
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_complain_detail);
        connectUI();
        apiInterface = APIClient.getClient().create(APIInterface.class);

    }

    private void connectUI() {
        if(getSupportActionBar()!=null) {
            toolbar = getSupportActionBar();
            toolbar.setDisplayHomeAsUpEnabled(true);
            toolbar.setTitle("Close Complaint");
        }
        tv_location_lbl=findViewById(R.id.tv_location_lbl);
        tv_name_lbl=findViewById(R.id.tv_name_lbl);
        tv_smartcard_lbl=findViewById(R.id.tv_smartcard_lbl);
        tv_ticketno=findViewById(R.id.tv_ticketno_lbl);
        tv_priority_lbl=findViewById(R.id.tv_priority_lbl);
        et_complaint_remark=findViewById(R.id.complaint_remark);
        getDetails();
    }

    private void getDetails() {
            tv_location_lbl.setText(getIntent().getStringExtra("location_lbl"));
            tv_name_lbl.setText(getIntent().getStringExtra("name_lbl"));
            tv_smartcard_lbl.setText(getIntent().getStringExtra("smartcard_lbl"));
            tv_ticketno.setText(getIntent().getStringExtra("ticketno"));
            tv_priority_lbl.setText(getIntent().getStringExtra("priority"));
            id=getIntent().getStringExtra("id");
    }

    public void closeComplaint(View view) {
        if(!et_complaint_remark.getText().toString().trim().isEmpty())
        {
            remark=et_complaint_remark.getText().toString();
        }
        else
        {
            remark="";
        }
        closeComplaint(remark,id);
    }

    public  void closeComplaint(String remark, String complaintid) {
        try{
            ApiRequest apiRequest = new ApiRequest(OpenComplainDetailActivity.this);
            Call<JsonObject> call = null;
            JsonObject jsonObject =new JsonObject();
            jsonObject.addProperty("closing_remark",remark);
            jsonObject.addProperty("status",3);
            call = apiInterface.closeComplaint(UtilPref.getAuth(OpenComplainDetailActivity.this),complaintid,jsonObject);
            apiRequest.request(call, closeComplaintListener);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    FetchDataListener closeComplaintListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);

            CommonUtils.showSuccessMessage(OpenComplainDetailActivity.this,data,"Complaint Closed");
          //  CommonUtils.showToasty(OpenComplainDetailActivity.this,"Complaint Closed",Params.TOASTY_SUCCESS);
            OpenComplainDetailActivity.this.finish();
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(OpenComplainDetailActivity.this,msg,"Something went wrong, Please try  sometime  later");
           // CommonUtils.showToasty(OpenComplainDetailActivity.this,msg,Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(OpenComplainDetailActivity.this);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                OpenComplainDetailActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
