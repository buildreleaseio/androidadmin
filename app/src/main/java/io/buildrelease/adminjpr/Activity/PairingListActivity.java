package io.buildrelease.adminjpr.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.buildrelease.adminjpr.Adapters.PairingListAdapter;
import io.buildrelease.adminjpr.Model.ParingListData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class PairingListActivity extends AppCompatActivity {

    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pairing_list);
        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView_pairingList);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Pairing List");
        }
        getPairingListData();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                PairingListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    public void getPairingListData() {
        ApiRequest apiRequest = new ApiRequest(PairingListActivity.this);
        Call<JsonObject> call = apiInterface.getPairingListData(UtilPref.getAuth(PairingListActivity.this));
        apiRequest.request(call, getPairingDataListener);

    }


    FetchDataListener getPairingDataListener =new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            ParingListData paringListData = new Gson().fromJson(data, ParingListData.class);
            inflatePairingAdapter(paringListData);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(PairingListActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
                RequestQueueService.showProgressDialog(PairingListActivity.this);
        }
    };



    public void inflatePairingAdapter(ParingListData paringListData){

       if( paringListData.getData().size() >0) {
           PairingListAdapter mAdapter =
                   new PairingListAdapter(
//
                           paringListData,
                           getSupportFragmentManager(),
                           recyclerView,
                           PairingListActivity.this);
           recyclerView.hasFixedSize();
           recyclerView.setAdapter(mAdapter);
           LinearLayoutManager layoutManager = new LinearLayoutManager(PairingListActivity.this);
           recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
           mAdapter.notifyDataSetChanged();
           no_data_ll.setVisibility(View.GONE);
       }else
       {
           no_data_ll.setVisibility(View.VISIBLE);
       }
    }

}
