package io.buildrelease.adminjpr.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.RecentlyExpiredListAdapter;
import io.buildrelease.adminjpr.Model.RecentlyExpiredData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class RecentlyExpiredListActivity extends AppCompatActivity {

    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    int pageno =1 ,pageCount = 50;
    RecentlyExpiredListAdapter mAdapter ;
    boolean newData = true;
    Button btn_suspend;
    LinearLayoutManager layoutManager;
    List<String> ids =null;
    boolean isScrolled = false;
    List<RecentlyExpiredData.Datum> datumArrayListRecentlyExpired =new ArrayList<>();
    String totalPageCount = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recently_expired_list);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Recently Expired");
        }
        connnectUI();
        getRecentlyExpiredListData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                RecentlyExpiredListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getRecentlyExpiredListData() {
        ApiRequest apiRequest = new ApiRequest(RecentlyExpiredListActivity.this);
        Call<JsonObject> call = apiInterface.getExpiredtList(UtilPref.getAuth(RecentlyExpiredListActivity.this),pageno,pageCount);
        apiRequest.request(call, getRecentlyExpiredListDataListener);
    }


    FetchDataPaginatedListener getRecentlyExpiredListDataListener =new FetchDataPaginatedListener() {

        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            RecentlyExpiredData recentlyExpiredData = new Gson().fromJson(data, RecentlyExpiredData.class);
            inflateRecentlyExpiredAdapter(recentlyExpiredData);
            totalPageCount = pageCount;
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(RecentlyExpiredListActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(RecentlyExpiredListActivity.this);
        }
    };

    public void inflateRecentlyExpiredAdapter(RecentlyExpiredData recentlyExpiredData){

        if(newData) {
            datumArrayListRecentlyExpired.clear();
            datumArrayListRecentlyExpired.addAll(recentlyExpiredData.getData());
            mAdapter.notifyDataSetChanged();
        }else
        {
            datumArrayListRecentlyExpired.addAll(recentlyExpiredData.getData());
            mAdapter.notifyDataSetChanged();
        }
        if(recentlyExpiredData.getData()!=null && recentlyExpiredData.getData().size()==0)
        {
            no_data_ll.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            no_data_ll.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }

    }

    public void connnectUI()
    {
        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView_recentlyExpiredList);
        mAdapter = new RecentlyExpiredListAdapter(getSupportFragmentManager(),RecentlyExpiredListActivity.this,datumArrayListRecentlyExpired);
        layoutManager = new LinearLayoutManager(RecentlyExpiredListActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolled = true;
                    loadmoreData();
                }
            }
        });

    }
    public void loadmoreData() {
        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();

        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;
            if(totalPageCount!=null)
            {
                int total = Integer.parseInt(totalPageCount);
                Log.d(TAG, "loadmoreData: total"+total+" pageno "+pageno + " pagecount "+pageCount);
                if(total>pageno*pageCount) {
                    pageno += 1;
                    newData = false;
                    getRecentlyExpiredListData();
                }
            }
        }
    }
}
