package io.buildrelease.adminjpr.Activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import java.net.URLEncoder;

import io.buildrelease.adminjpr.Network.APIURL;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.UtilPref;

public class RechargeActivity extends AppCompatActivity {

    WebView webView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge);
        webView = findViewById(R.id.webView_recharge);
        progressBar = findViewById(R.id.progress_bar_webView);
        progressBar.setVisibility(View.GONE);
        String url = APIURL.getBASE_URL_PAYMENT() + "/portal/index.php?r=/portal/online-pay/fran-pay&id=" + UtilPref.getOperatorID(RechargeActivity.this);
        loadWebView(url);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Recharge Wallet");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                RechargeActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @SuppressLint("SetJavaScriptEnabled")
    public void loadWebView(String url) {
        try {

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setDomStorageEnabled(true);
//        tradeView.loadUrl("https://www.tradingview.com/chart/?symbol=BITFINEX%3ABTCUSD");


            webView.loadUrl(url);
//            webView.loadUrl(URLEncoder.encode(url, "UTF-8"));
            Log.d(Params.TAG, "loadWebView: " + URLEncoder.encode(url, "UTF-8"));
            Log.d(Params.TAG, "loadWebView: " + url);
            myWebViewClient myWebViewClient = new myWebViewClient();
            myWebViewClient.shouldOverrideUrlLoading(webView, url);
            webView.setWebViewClient(new myWebViewClient());
            webView.setWebChromeClient(new WebChromeClient());
            webView.setWebViewClient(new WebViewClient(){
                @Override
                public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                    // DOT CALL SUPER METHOD
                    // super.onReceivedSslError(view, handler, error);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(RechargeActivity.this);
                    String message = "SSL Certificate error.";
                    switch (error.getPrimaryError()) {
                        case SslError.SSL_UNTRUSTED:
                            message = "The certificate authority is not trusted.";
                            break;
                        case SslError.SSL_EXPIRED:
                            message = "The certificate has expired.";
                            break;
                        case SslError.SSL_IDMISMATCH:
                            message = "The certificate Hostname mismatch.";
                            break;
                        case SslError.SSL_NOTYETVALID:
                            message = "The certificate is not yet valid.";
                            break;
                    }
                    message += " Do you want to continue anyway?";
                    builder.setTitle("SSL Certificate Error");
                    builder.setMessage(message);
                    builder.setPositiveButton("continue", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            handler.proceed();
                        }
                    });
                    builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            handler.cancel();
                        }
                    });
                    final AlertDialog dialog = builder.create();
                    dialog.show();
//                    handler.proceed();
                }
            });



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class myWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
//            RequestQueueService.showProgressDialog(AccountWebviewActivity.this);
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
//            RequestQueueService.cancelProgressDialog();
            progressBar.setVisibility(View.GONE);
        }

    }


    @Override
    public void onBackPressed() {
//        if (webView.canGoBack()) {

        Intent intent = new Intent(RechargeActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
//        if( webView != null ) {
//            if( (keyCode == KeyEvent.KEYCODE_BACK) && (webView.canGoBack()) ) {
//                webView.goBack();
//                webView.stopLoading();
//                return true;
//            }
//        }
//        // If it wasn't the Back key or there's no web page history, bubble up to the default
//        // system behavior (probably exit the activity)
//        return super.onKeyDown(keyCode, event);
        if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
            webView.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
