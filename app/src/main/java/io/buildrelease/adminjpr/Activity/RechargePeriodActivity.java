package io.buildrelease.adminjpr.Activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.RechargeListAdapter;
import io.buildrelease.adminjpr.Model.BulkAccountRenewalPayload;
import io.buildrelease.adminjpr.Model.BulkBouqoutesRenewalPayload;
import io.buildrelease.adminjpr.Model.RechargePayload;
import io.buildrelease.adminjpr.Model.RechargeResponse;
import io.buildrelease.adminjpr.Model.SavePayload;
import io.buildrelease.adminjpr.Model.SavePayloadForPackChange;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.APIURL;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import io.buildrelease.adminjpr.interfaces.SelectedPeriodInterfaceAddon;
import retrofit2.Call;

public class RechargePeriodActivity extends AppCompatActivity implements SelectedPeriodInterfaceAddon {


    final String TAG = getClass().getSimpleName();
    APIInterface apiInterface;
    ApiRequest apiRequest = new ApiRequest(this);
    private RecyclerView recyclerView;
    private RechargeListAdapter mAdapter;
    List<RechargeResponse.Data.Rp0> dataList = new ArrayList<>();
    String ids = "",accno= "";
    Boolean addon = false;
    String url = "";
    String type ;
   // String posturl = "http://jprnetworksms.net/portal/index.php?r=portal/online-pay/checkout";
    BulkAccountRenewalPayload bulkAccountRenewalPayload ;
    BulkBouqoutesRenewalPayload bulkBouqoutesRenewalPayload;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_period);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Recharge Period");
        }

        if(getIntent().getExtras()!=null)
        {
            ids = getIntent().getExtras().getString("id");
            accno = getIntent().getExtras().getString("accno");
           // addon = getIntent().getExtras().getBoolean("addon",false);
            type = getIntent().getExtras().getString("type");
            if(type.equalsIgnoreCase(Params.BULKACCOUNTRENEWAL))
            {
                String data = getIntent().getExtras().getString("bulkaccount");
                bulkAccountRenewalPayload = new Gson().fromJson(data,BulkAccountRenewalPayload.class);
            }else if(type.equalsIgnoreCase(Params.BULKBOUQUETSRENEWAL))
            {
                String data = getIntent().getExtras().getString("bulkbouquets");
                bulkBouqoutesRenewalPayload = new Gson().fromJson(data,BulkBouqoutesRenewalPayload.class);
            }

        }
        connectUI();

        callRechargePeriod();

    }


    public void callRechargePeriod()
    {
        if(type.equalsIgnoreCase(Params.BULKACCOUNTRENEWAL))
        {
            RechargePeriodBulkAccount();
        }else if(type.equalsIgnoreCase(Params.BULKBOUQUETSRENEWAL))
        {
            RechargePeriodBulkBouqoutes();
        }else
        {
            RechargePeriod();
        }
    }


    public String getPostData(String account_ids,String type,String rperiod_id,String remark,List<String> ids)
    {
        String postData = "";
        try {
           postData = "account_ids=" + URLEncoder.encode(account_ids, "UTF-8") + "&rperiod_id=" + URLEncoder.encode(rperiod_id, "UTF-8")+"&remark=" + URLEncoder.encode(remark, "UTF-8")+"&type=" + URLEncoder.encode(type, "UTF-8");
            for(String id : ids)
            {
                postData = postData+"&bouque_ids[]=" + URLEncoder.encode(id, "UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.d(TAG, "getpaymentUrl: "+e);
        }

        Log.d(TAG, "getPostData: "+postData);
        return postData;
        // webview.postUrl(url,postData.getBytes());

    }


    public void buildURL(String period,List<String> ids,String accid)
    {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("http")
                .authority(APIURL.getPAYMENT_GATEWAY())
                .appendPath("portal")
                .appendPath("index.php")
                .appendQueryParameter("rperiod_id",period)
                .appendQueryParameter("account_id",accid)
                .appendQueryParameter("type",type)
                .appendQueryParameter("r","portal/online-pay/checkout");
        for(String id : ids)
        {
            builder.appendQueryParameter("bouque_id[]",id);
        }
         url = builder.build().toString();

        Log.d(TAG, "buildURL: "+url);
    }


    public void connectUI() {

        recyclerView = findViewById(R.id.recyclerView);
        mAdapter = new RechargeListAdapter(RechargePeriodActivity.this, dataList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                RechargePeriodActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

//    public void AdddonPeriod()
//    {
//        Call<JsonObject> call = apiInterface.getAddonRenewal("Bearer "+UtilPref.getAuth(this),ids);
//        apiRequest.request(call,fetchDataListener);
//    }


    public void RechargePeriodBulkAccount()
    {
        Call<JsonObject> call = apiInterface.getRechargePeriodBulkAccount(UtilPref.getAuth(this));
        apiRequest.request(call,fetchDataListener);
    }

    public void RechargePeriodBulkBouqoutes()
    {
        Call<JsonObject> call = apiInterface.getRechargePeriodBulkBouquets(UtilPref.getAuth(this),bulkBouqoutesRenewalPayload.getBouqueIds().get(0));
        apiRequest.request(call,fetchDataListener);
    }

    public void RechargePeriod()
    {
        RechargePayload rechargePayload = new RechargePayload();
        rechargePayload.setAccountId(accno);
        Call<JsonObject> call = apiInterface.getRechargeRenewal(UtilPref.getAuth(this),ids,rechargePayload);
        apiRequest.request(call,fetchDataListener);
    }

    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);
            RechargeResponse rechargeResponse = new Gson().fromJson(data,RechargeResponse.class);
            dataList.clear();
            dataList.addAll(rechargeResponse.getData().getRp0());
            mAdapter.notifyDataSetChanged();

        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(RechargePeriodActivity.this,msg,"Something went wrong, Please try  sometime  later");

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(RechargePeriodActivity.this);
        }
    };



    @Override
    public void selectedID(String id) {



        Call<JsonObject> call=null;
        if (type.equals(Params.RENEWAL)) {
            List<String> idList = Arrays.asList(ids.split("\\s*-\\s*"));
            List<Integer> idsList = new ArrayList<>();
            SavePayload savePayload = new SavePayload();
            savePayload.setAccountId(accno);
            savePayload.setRemark("Renewal");
            savePayload.setRperiodId(id);
            for (String data : idList) {
                idsList.add(Integer.parseInt(data));
            }
            savePayload.setBouqueIds(idsList);
            call = apiInterface.saveBouquets(UtilPref.getAuth(this), savePayload);
        } else if(type.equalsIgnoreCase(Params.BULKACCOUNTRENEWAL))
        {
            bulkAccountRenewalPayload.setRperiodId(id);
            call = apiInterface.renewBulkAccounts(UtilPref.getAuth(this), bulkAccountRenewalPayload);

        }else if(type.equalsIgnoreCase(Params.BULKBOUQUETSRENEWAL))
        {
            bulkBouqoutesRenewalPayload.setRperiodId(id);
            call = apiInterface.bulkBouqueRenewal(UtilPref.getAuth(this), bulkBouqoutesRenewalPayload);

        }else
        {
            SavePayloadForPackChange savePayload = new SavePayloadForPackChange();
            savePayload.setAccountIds(accno);
            savePayload.setRemark(Params.CHANGE_BASE_PLAN);
            savePayload.setRperiodId(id);
            savePayload.setNewbouqueIds(Integer.parseInt(ids));
            call = apiInterface.changeBasePlan(UtilPref.getAuth(this), savePayload);
        }
        apiRequest.request(call, fetchDataListenerSave);


        //buildURL(id,idList,UtilPref.getACCID(RechargePeriodActivity.this));

//        Intent intent = new Intent(RechargePeriodActivity.this,PaymentActivity.class);
//        intent.putExtra("url",posturl);
//        intent.putExtra("postdata",getPostData(UtilPref.getACCID(RechargePeriodActivity.this),type,id,"remark" ,idList));
//        startActivity(intent);

}

    FetchDataListener fetchDataListenerSave = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);
            if (type.equals(Params.RENEWAL)) {

                CommonUtils.showSuccessMessage(RechargePeriodActivity.this,data,"Your request is saved");
               // Toasty.success(RechargePeriodActivity.this,"Your request is saved").show();
                finishAffinity();
            }else if(type.equalsIgnoreCase(Params.BULKACCOUNTRENEWAL))
            {
                try
                {
                    CommonUtils.showSuccessMessage(RechargePeriodActivity.this,data,"Your request is saved");
//                    JsonObject jsonObject=new Gson().fromJson(data,JsonObject.class);
//                    JsonObject jsonData= (JsonObject) jsonObject.get("data");
//                    Toasty.success(RechargePeriodActivity.this,jsonData.get("msg").toString()).show();
                    finishAffinity();
                }
                catch (NullPointerException j)
                {
                    CommonUtils.showSuccessMessage(RechargePeriodActivity.this,data,"Renewed Successfully");
                    //Toasty.success(RechargePeriodActivity.this,"Renewed Successfully").show();
                    finishAffinity();
                }
            }
            else
            {
               try
               {
                   CommonUtils.showSuccessMessage(RechargePeriodActivity.this,data,"Your request is saved");
//                   JsonObject jsonObject=new Gson().fromJson(data,JsonObject.class);
//                   JsonObject jsonData= (JsonObject) jsonObject.get("data");
//                   Toasty.success(RechargePeriodActivity.this,jsonData.get("msg").toString()).show();
                   finishAffinity();
               }
               catch (NullPointerException j)
               {
                   CommonUtils.showSuccessMessage(RechargePeriodActivity.this,data,"Changed Base Plan Successfully");
//
                   //Toasty.success(RechargePeriodActivity.this,"Changed Base Plan Successfully").show();
                   finishAffinity();
               }
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(RechargePeriodActivity.this,msg,"Something went wrong, Please try  sometime  later");

//            Toasty.error(RechargePeriodActivity.this,msg).show();

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(RechargePeriodActivity.this);
        }
    };

}
