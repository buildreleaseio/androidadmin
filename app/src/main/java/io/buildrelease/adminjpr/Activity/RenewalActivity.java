package io.buildrelease.adminjpr.Activity;

import android.os.Bundle;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.AddonAdapter;
import io.buildrelease.adminjpr.Model.RenewPlanReponse;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import io.buildrelease.adminjpr.interfaces.SelectedRenewalInterface;
import retrofit2.Call;

public class RenewalActivity extends AppCompatActivity implements SelectedRenewalInterface {

    final String TAG = getClass().getSimpleName();
    APIInterface apiInterface;
    ApiRequest apiRequest = new ApiRequest(this);

    private RecyclerView recyclerView;
    private AddonAdapter mAdapter;
    List<RenewPlanReponse.Datum> dataList = new ArrayList<>();
    List<RenewPlanReponse.Datum> filerList = new ArrayList<>();
    TextView tv_stb,tv_vcnumber,tv_basename;
//    CheckBox basechckbox;
//    ImageView iv_view_base;
    Button btn_proceed;
    String basePlanId =null;
    android.support.v7.widget.SearchView searchView;
    ChipGroup chipGroup;
    CheckBox selectallcheckbox;

    String vc_no,stb_no,acc_no;

    List<String> typeList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_renewal);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Renewal");
        }



        if(getIntent().getExtras()!=null)
        {
            vc_no = getIntent().getExtras().getString("vc","");
            stb_no = getIntent().getExtras().getString("stbno","");
            acc_no = getIntent().getExtras().getString("accno","");
            connectUI();
            getRenewals();
        }




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                RenewalActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    public void filterList(String type)
    {

        filerList.clear();
        if(type.equalsIgnoreCase("all"))
        {

            for(RenewPlanReponse.Datum datum : dataList)
            {
               filerList.add(datum);
            }
        }else
        {
            for(RenewPlanReponse.Datum datum : dataList)
            {
                if(datum.getTypeLbl().equalsIgnoreCase(type)) {

                        filerList.add(datum);

                }
            }
        }
        mAdapter.notifyDataSetChanged();
    }




    public void createTypeList()
    {
        for(RenewPlanReponse.Datum datum : dataList)
        {
            if(!typeList.contains(datum.getTypeLbl()))
            {
                typeList.add(datum.getTypeLbl());
            }
        }

        setUpChips();
    }

    public void setUpChips()
    {

        chipGroup.setChipSpacing(20);
        chipGroup.setSingleSelection(true);
        Chip chip1 = new Chip(RenewalActivity.this);
        chip1.setTag("all");

        chip1.setCheckable(true);
        // chip1.setChecked(true);
        chip1.setText("  All   ");
        chip1.setClickable(true);
        chip1.setChipBackgroundColorResource(R.color.colorPrimary);
        chip1.setTextColor(getResources().getColor(R.color.colorWhite));
        chip1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterList(view.getTag().toString());
            }
        });
        chipGroup.addView(chip1);
        for(String type: typeList)
        {
            Chip chip = new Chip(RenewalActivity.this);
            chip.setTag(type);
            chip.setCheckable(true);
            chip.setText(type);
            chip.setClickable(true);
            chip.setChipBackgroundColorResource(R.color.colorPrimary);
            chip.setTextColor(getResources().getColor(R.color.colorWhite));
            chip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    filterList(view.getTag().toString());
                }
            });
            chipGroup.addView(chip);
        }

    }





    public void markAllSelected()
    {
        if(mAdapter!=null) {
            if(filerList.size()>0) {
                mAdapter.markSelected();
            }
        }
    }



    public void markAlUnlSelected()
    {
        if(mAdapter!=null) {
            if(filerList.size()>0) {
                mAdapter.markUnSelected();
            }
        }
    }


    public void unCheckSelectedAll()
    {
        if(selectallcheckbox!=null)
        {
            if(selectallcheckbox.isChecked()) {
                unsetCheckBoxLisener();
                selectallcheckbox.setChecked(false);
                setCheckBoxLisener();
            }
        }


    }



    public void setCheckBoxLisener()
    {
        selectallcheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    markAllSelected();
                }else
                {
                    markAlUnlSelected();
                }
            }
        });
    }

    public void unsetCheckBoxLisener()
    {
        selectallcheckbox.setOnCheckedChangeListener(null);
    }


    public void connectUI()
    {

        selectallcheckbox = findViewById(R.id.selectallcheckbox);

        setCheckBoxLisener();

        chipGroup = findViewById(R.id.filter_chipgroup);
        searchView = findViewById(R.id.searchView);

        searchView.setOnQueryTextListener(new android.support.v7.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {

                mAdapter.getFilter().filter(s);
                return false;
            }
        });


        searchView.setIconified(false);
        searchView.clearFocus();
        btn_proceed = findViewById(R.id.btn_proceed);
        btn_proceed.setVisibility(View.GONE);
        recyclerView =  findViewById(R.id.recyclerView);
        mAdapter = new AddonAdapter(RenewalActivity.this,filerList,this,acc_no);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        tv_stb = findViewById(R.id.tv_stb);
        tv_stb.setText(stb_no);
        tv_vcnumber = findViewById(R.id.tv_vcnumber);
        tv_vcnumber.setText(vc_no);
        tv_basename = findViewById(R.id.tv_basename);
//        basechckbox = findViewById(R.id.basechckbox);
//        iv_view_base = findViewById(R.id.iv_view_base);

//        iv_view_base.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(RenewalActivity.this,ChannelListActivity.class);
//                intent.putExtra("id",basePlanId+"");
//                startActivity(intent);
//            }
//        });
//
//        basechckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                if(b) {
//                    btn_proceed.setVisibility(View.VISIBLE);
//                }else
//                {
//                    btn_proceed.setVisibility(View.GONE);
//                }
//            }
//        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        searchView.clearFocus();

    }

    public void getRenewals()
    {
        Log.d(TAG, "getRenewals: "+UtilPref.getACCID(RenewalActivity.this));
        Call<JsonObject> call = apiInterface.getRenewals(UtilPref.getAuth(this),acc_no);
        apiRequest.request(call,fetchDataListener);
    }


    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: "+data);
            RenewPlanReponse renewPlanReponse = new Gson().fromJson(data,RenewPlanReponse.class);
            List<RenewPlanReponse.Datum> list = new ArrayList<>();
            list.clear();
            list.addAll(renewPlanReponse.getData());
            dataList.clear();


            for(RenewPlanReponse.Datum datum : list)
            {
//                if(datum.getTypeLbl().equalsIgnoreCase("base"))
//                {
//                    tv_basename.setText(datum.getName());
//                    basePlanId = datum.getId()+"";
//                }else
                {
                    dataList.add(datum);
                }

            }
            filerList.clear();
            filerList.addAll(dataList);
            if(filerList.size()>0)
            {
                selectallcheckbox.setVisibility(View.VISIBLE);
            }else
            {
                selectallcheckbox.setVisibility(View.GONE);
            }
            createTypeList();
            mAdapter.notifyDataSetChanged();


        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();

        }

        @Override
        public void onFetchStart() {
                RequestQueueService.showProgressDialog(RenewalActivity.this);
        }
    };

    public void proceed(View view) {
//        if(basechckbox.isChecked()) {
//            mAdapter.proceed(basePlanId);
//        }else
        {
            mAdapter.proceed(dataList);
        }
    }




    @Override
    public void selected(List<String> selectedList) {


        if(selectedList.size()>0) {
            btn_proceed.setVisibility(View.VISIBLE);
        }else
        {
            btn_proceed.setVisibility(View.GONE);
        }
    }
}
