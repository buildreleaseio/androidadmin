package io.buildrelease.adminjpr.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.RemovebouquetsListAdapter;
import io.buildrelease.adminjpr.Model.RemoveBouquetsResponse;
import io.buildrelease.adminjpr.Model.RemovebouqePayload;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import io.buildrelease.adminjpr.interfaces.RemoveBouquetsInterface;
import retrofit2.Call;

public class SingleRemoveBouquetsActivity extends AppCompatActivity implements RemoveBouquetsInterface {

    final String TAG = getClass().getSimpleName();
    APIInterface apiInterface;
    ApiRequest apiRequest = new ApiRequest(this);
    private RecyclerView recyclerView;
    RemovebouquetsListAdapter removebouquetsListAdapter;

    List<RemoveBouquetsResponse.Datum> datumList = new ArrayList<>();
    String accno;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_remove_bouquets);
        apiInterface = APIClient.getClient().create(APIInterface.class);

        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Remove Bouquets");
        }

        if(getIntent().getExtras()!=null)
        {
            accno= getIntent().getExtras().getString("accno");
            connectUI();
            getAddonList();
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                SingleRemoveBouquetsActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    public void connectUI()
    {



        recyclerView =  findViewById(R.id.recyclerView);
        removebouquetsListAdapter = new RemovebouquetsListAdapter(SingleRemoveBouquetsActivity.this,datumList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(removebouquetsListAdapter);
    }

    public void getAddonList()
    {
        Call<JsonObject> call = apiInterface.getAllotedBoques(UtilPref.getAuth(this),accno);
        apiRequest.request(call,fetchDataListener);
    }


    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            RemoveBouquetsResponse removeBouquetsResponse = new Gson().fromJson(data,RemoveBouquetsResponse.class);
            datumList.clear();
            datumList.addAll(removeBouquetsResponse.getData());
            removebouquetsListAdapter.notifyDataSetChanged();

        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(SingleRemoveBouquetsActivity.this);
        }
    };


    @Override
    public void remove(String subs_tran_ids) {

        RemovebouqePayload removebouqePayload = new RemovebouqePayload();
        removebouqePayload.setAccountId(accno);
        removebouqePayload.setRemark("Remove Bouque");
        removebouqePayload.setSubsTranIds(subs_tran_ids);
        remove(removebouqePayload);
    }


    public void remove(RemovebouqePayload removebouqePayload)
    {
        Call<JsonObject> call = apiInterface.removeSingleBouquets(UtilPref.getAuth(this),removebouqePayload);
        apiRequest.request(call,fetchDataListenerRemove);

    }


    FetchDataListener fetchDataListenerRemove = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();

            CommonUtils.showSuccessMessage(SingleRemoveBouquetsActivity.this,data,"Removed");
           // Toasty.success(SingleRemoveBouquetsActivity.this,"Removed").show();
            SingleRemoveBouquetsActivity.this.finishAffinity();
            getAddonList();

        }

        @Override
        public void onFetchFailure(String msg) {
            Log.d(TAG, "onFetchFailure: "+msg);
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(SingleRemoveBouquetsActivity.this,msg,"Something went wrong, Please try  sometime  later");
           // Toasty.error(SingleRemoveBouquetsActivity.this,"msg").show();

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(SingleRemoveBouquetsActivity.this);
        }
    };

}
