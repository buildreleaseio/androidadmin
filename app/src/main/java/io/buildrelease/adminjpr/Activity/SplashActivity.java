package io.buildrelease.adminjpr.Activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.UtilPref;

public class SplashActivity extends AppCompatActivity {

    ImageView logo_image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        logo_image = findViewById(R.id.logo_image);
        CommonUtils.displayImageResource(R.drawable.ic_jpr,SplashActivity.this,logo_image);
        startTimer();
    }

    private void checkLogin() {
        String Auth = UtilPref.getAuth(SplashActivity.this);
        if(Auth == null || Auth.isEmpty()){
            moveToLogin();
        }else{
            moveToMain();
        }
    }

    private void moveToLogin() {
        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
    private void moveToMain() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void startTimer() {
        new CountDownTimer(2000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d("seconds remaining: ", ">>>> " + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                //   getPrefetchData();
                checkLogin();
//                startActivity(new Intent(SplashActivity.this,MainActivity.class));
                //  getPrefetchData();

            }
        }.start();
    }

}
