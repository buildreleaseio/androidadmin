package io.buildrelease.adminjpr.Activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.ActiveListAdapter;
import io.buildrelease.adminjpr.Model.ActionPayload;
import io.buildrelease.adminjpr.Model.ActiveAccountData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import io.buildrelease.adminjpr.interfaces.SelectedIdListInterface;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class SuspendedListActivity extends AppCompatActivity implements SelectedIdListInterface {

    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    int pageno =1 ,pageCount = 20;
    ActiveListAdapter mAdapter ;
    boolean newData = true;
    Button btn_resume;
    LinearLayoutManager layoutManager;
    List<String> ids =null;
    boolean isScrolled = false;
    String totalPageCount = "0";

    List<ActiveAccountData.Datum> datumArrayListSuspended =new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suspended_list);

        apiInterface = APIClient.getClient().create(APIInterface.class);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("In Active List");
        }
        getSuspendedListData();
        connnectUI();
    }

    public void connnectUI()
    {
        no_data_ll = findViewById(R.id.no_data_ll);
        recyclerView = findViewById(R.id.recyclerView_inactiveList);
        btn_resume=findViewById(R.id.btn_resume);
        btn_resume.setVisibility(View.GONE);
        mAdapter = new ActiveListAdapter(SuspendedListActivity.this,datumArrayListSuspended,this);
        layoutManager = new LinearLayoutManager(SuspendedListActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolled = true;
                    loadmoreData();
                }
            }
        });

    }


    public void getSuspendedListData() {
        ApiRequest apiRequest = new ApiRequest(SuspendedListActivity.this);
        Call<JsonObject> call = apiInterface.getInactiveAccountList(UtilPref.getAuth(SuspendedListActivity.this),pageno,pageCount);
        apiRequest.request(call, getSuspendedListDataListener);
    }


    FetchDataPaginatedListener getSuspendedListDataListener =new FetchDataPaginatedListener() {
        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            ActiveAccountData suspendedAccountData = new Gson().fromJson(data, ActiveAccountData.class);
            inflateSuspendedAdapter(suspendedAccountData);
            totalPageCount = pageCount;
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(SuspendedListActivity.this,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(SuspendedListActivity.this);
        }
    };



    public void inflateSuspendedAdapter(ActiveAccountData suspendedAccountData){
            if(newData) {
                datumArrayListSuspended.clear();
                datumArrayListSuspended.addAll(suspendedAccountData.getData());
                mAdapter.notifyDataSetChanged();
            }else
            {
                datumArrayListSuspended.addAll(suspendedAccountData.getData());
                mAdapter.notifyDataSetChanged();
            }
        if(suspendedAccountData.getData()!=null && suspendedAccountData.getData().size()==0)
        {
            no_data_ll.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            no_data_ll.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                SuspendedListActivity.this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void resume(View view) {
        showRemarkDialog();
    }

    private void showRemarkDialog() {
        try {
            final MaterialDialog.Builder builder = new MaterialDialog.Builder(SuspendedListActivity.this);
            builder.autoDismiss(true);
            builder.cancelable(true);

            builder.title("Remark")
                    .inputRange(0, 255)
                    .positiveColor(getResources().getColor(R.color.colorPrimary))
                    .negativeColor(getResources().getColor(R.color.colorPrimary))
                    .input("Type your comment here", "", false, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            performBulkOperation(input.toString());
                        }
                    })
                    .positiveText("Submit")
//                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//
//                        }
//                    })
                    .negativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            builder.cancelable(true);
                        }
                    })
                    .canceledOnTouchOutside(true)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void performBulkOperation(String s) {
        try {
            ApiRequest apiRequest = new ApiRequest(SuspendedListActivity.this);
            ActionPayload actionPayload = new ActionPayload();
            actionPayload.setRemark(s);
            actionPayload.setAccountIds(ids);
            Call<JsonObject>  call = apiInterface.bulkResume(UtilPref.getAuth(SuspendedListActivity.this),actionPayload);
            apiRequest.request(call, bulkOperationListener);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    FetchDataListener bulkOperationListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
//                    bulk_button.setVisibility(View.GONE);
            CommonUtils.showSuccessMessage(SuspendedListActivity.this,data,"Successfully resumed");
            //Toasty.success(SuspendedListActivity.this,"Successfully resumed").show();
            reload();


        }

        public void reload()
        {
            newData = true;
            pageno = 1;
            mAdapter.clearSelectedList();
            btn_resume.setVisibility(View.GONE);
            datumArrayListSuspended.clear();
            mAdapter.notifyDataSetChanged();
            getSuspendedListData();
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showFailureMessage(SuspendedListActivity.this,msg,"Something went wrong, Please try  sometime  later");
            //CommonUtils.showToasty(SuspendedListActivity.this,msg,Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(SuspendedListActivity.this);
        }
    };

    @Override
    public void selectedIds(List<String> ids) {
        this.ids = ids;
        if(ids.size()>0)
        {
            btn_resume.setVisibility(View.VISIBLE);
        }else
        {
            btn_resume.setVisibility(View.GONE);
        }
    }

    public void loadmoreData() {
        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();

        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;
            if(totalPageCount!=null)
            {
                int total = Integer.parseInt(totalPageCount);
                Log.d(TAG, "loadmoreData: total"+total+" pageno "+pageno + " pagecount "+pageCount);
                if(total>pageno*pageCount) {
                    pageno += 1;
                    newData = false;
                    getSuspendedListData();
                }
            }
        }
    }
}
