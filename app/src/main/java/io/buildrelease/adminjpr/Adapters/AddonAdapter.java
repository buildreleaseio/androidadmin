package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Activity.ChannelListActivity;
import io.buildrelease.adminjpr.Activity.RechargePeriodActivity;
import io.buildrelease.adminjpr.Activity.RenewalActivity;
import io.buildrelease.adminjpr.Model.RenewPlanReponse;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.interfaces.SelectedRenewalInterface;

public class AddonAdapter extends RecyclerView.Adapter<AddonAdapter.MyViewHolder> implements Filterable {

    Context context;
    View view;
    List<RenewPlanReponse.Datum> dataList;
    List<RenewPlanReponse.Datum> filtereddataList;
    SelectedRenewalInterface selectedRenewalInterface;
    List<String> selectedId = new ArrayList<>();
    String TAG = getClass().getSimpleName();
    String accno;


    public AddonAdapter(Context context, List<RenewPlanReponse.Datum> dataList, SelectedRenewalInterface selectedRenewalInterface, String accid) {
        this.context = context;
        this.dataList = dataList;
        this.selectedRenewalInterface = selectedRenewalInterface;
        filtereddataList = dataList;
        accno = accid;
    }

    public AddonAdapter() {

    }

    @NonNull
    @Override
    public AddonAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_add_on, viewGroup, false);
        AddonAdapter.MyViewHolder myViewHolder = new AddonAdapter.MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AddonAdapter.MyViewHolder myViewHolder, final int i) {
        RenewPlanReponse.Datum data = filtereddataList.get(i);


//        if(!data.getBoxtypeLbl().equalsIgnoreCase("base")) {
//            myViewHolder.tv_addon_name.setText(data.getName()+"");
//        }
//
//        myViewHolder.chkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//
//            }
//        });
//
//
//        myViewHolder.iv_view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//

        myViewHolder.chkbox.setOnCheckedChangeListener(null);

        if (selectedId.contains(data.getId() + "")) {
            myViewHolder.chkbox.setChecked(true);
        } else {
            myViewHolder.chkbox.setChecked(false);
        }

        if (!data.getBoxtypeLbl().equalsIgnoreCase("base")) {
            myViewHolder.tv_addon_name.setText(data.getName() + "");
        }

        myViewHolder.chkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Toast.makeText(context,"selected : "+i+ dataList.get(i).getId(),Toast.LENGTH_SHORT).show();
                if (b) {
                    selectedId.add(filtereddataList.get(i).getId() + "");
                } else {
                    ((RenewalActivity) context).unCheckSelectedAll();
                    selectedId.remove(filtereddataList.get(i).getId() + "");
                }
                selectedRenewalInterface.selected(selectedId);
            }
        });


        myViewHolder.iv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ChannelListActivity.class);
                intent.putExtra("id", dataList.get(i).getId() + "");
                context.startActivity(intent);
                // Toast.makeText(context,"selected : "+i+ dataList.get(i).getId(),Toast.LENGTH_SHORT).show();
            }
        });


    }


    public void markSelected() {
        selectedId.clear();
        for (RenewPlanReponse.Datum data : dataList) {
            selectedId.add(data.getId() + "");
        }
        selectedRenewalInterface.selected(selectedId);

        notifyDataSetChanged();
    }


    public void markUnSelected() {
        selectedId.clear();
        notifyDataSetChanged();
        selectedRenewalInterface.selected(selectedId);

    }

    public void proceed(List<RenewPlanReponse.Datum> dataLisOriginal) {


        String id = null;

        for (String iddata : selectedId) {
            if (id != null) {
                id = id + "-" + iddata;
            } else {
                id = iddata;
            }
        }
        Log.d("iddata", "proceed: " + id);

        String selection = getSelectedListString(dataLisOriginal);

        if (id != null && !id.isEmpty()) {

            Intent intent = new Intent(context, RechargePeriodActivity.class);
            intent.putExtra("id", id);
            intent.putExtra("accno", accno);
            // intent.putExtra("addon", true);
            // intent.putExtra("selection", selection);
            intent.putExtra("type", Params.RENEWAL);
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "Please select a option to proceed", Toast.LENGTH_LONG).show();
        }


    }


    public String getSelectedListString(List<RenewPlanReponse.Datum> dataLisOriginal) {
        List<RenewPlanReponse.Datum> selectedList = new ArrayList<>();
        for (RenewPlanReponse.Datum datum : dataLisOriginal) {
            if (selectedId.contains(datum.getId() + "")) {
                selectedList.add(datum);
            }
        }

        String data = new Gson().toJson(selectedList).trim();
        Log.d(TAG, "getSelectedListString: " + data);
        return data;

    }


    @Override
    public int getItemCount() {
        return filtereddataList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtereddataList = dataList;
                } else {
                    List<RenewPlanReponse.Datum> filteredList = new ArrayList<>();
                    for (RenewPlanReponse.Datum row : dataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filtereddataList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filtereddataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtereddataList = (ArrayList<RenewPlanReponse.Datum>) filterResults.values;
                notifyDataSetChanged();
            }
        };

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tv_addon_name;
        CheckBox chkbox;
        ImageView iv_view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_addon_name = itemView.findViewById(R.id.tv_addon_name);
            chkbox = itemView.findViewById(R.id.chkbox);
            iv_view = itemView.findViewById(R.id.iv_view);

        }
    }
}
