package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Activity.ChannelListActivity;
import io.buildrelease.adminjpr.Activity.RechargePeriodActivity;
import io.buildrelease.adminjpr.Model.AddonResponse;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.interfaces.SelectedAddonInterface;


public class AddonListAdapter extends RecyclerView.Adapter<AddonListAdapter.MyViewHolder> implements Filterable {

    Context context;
    View view;
    List<AddonResponse.Datum> dataList;
    List<AddonResponse.Datum> filtereddataList;
    SelectedAddonInterface selectedAddonInterface;
    List<String> selectedId = new ArrayList<>();
    String TAG = getClass().getSimpleName();




    public AddonListAdapter(Context context, List<AddonResponse.Datum> dataList, SelectedAddonInterface selectedAddonInterface) {
        this.context = context;
        this.dataList = dataList;
        this.selectedAddonInterface = selectedAddonInterface;
        filtereddataList = dataList;
    }

    public AddonListAdapter() {

    }

    @NonNull
    @Override
    public AddonListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_add_on, viewGroup, false);
        AddonListAdapter.MyViewHolder myViewHolder = new  AddonListAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AddonListAdapter.MyViewHolder myViewHolder, final int i) {
        final AddonResponse.Datum data = filtereddataList.get(i);


        myViewHolder.chkbox.setOnCheckedChangeListener(null);

        if(selectedId.contains(data.getId()+"")) {
            myViewHolder.chkbox.setChecked(true);
        }else
        {
            myViewHolder.chkbox.setChecked(false);
        }

        if(!data.getBoxtypeLbl().equalsIgnoreCase("base")) {
            myViewHolder.tv_addon_name.setText(data.getName()+"");
        }

        myViewHolder.chkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
              //  Toast.makeText(context,"selected : "+i+ dataList.get(i).getId(),Toast.LENGTH_SHORT).show();
                if(b)
                {
                    selectedId.add( filtereddataList.get(i).getId()+"");
                }else
                {
                    selectedId.remove( filtereddataList.get(i).getId()+"");
                };
                selectedAddonInterface.selected(selectedId);

            }
        });


        myViewHolder.iv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(context,"selected : "+i+ dataList.get(i).getId(),Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context,ChannelListActivity.class);
                intent.putExtra("id",filtereddataList.get(i).getId()+"");
                context.startActivity(intent);
            }
        });


    }


    public void proceed( List<AddonResponse.Datum> dataListOrginal,String accno)
    {

        String id = null;
        for(String iddata : selectedId)
        {
            if(id!=null)
            {
                id = id + "-"+iddata;
            }else
            {
                id = iddata;
            }
        }
        Log.d("iddata", "proceed: "+id);

        String selection = getSelectedListString(dataListOrginal);
        Intent intent = new Intent(context,RechargePeriodActivity.class);
        intent.putExtra("id",id);
       // intent.putExtra("addon",true);
        //intent.putExtra("type","addon");
        intent.putExtra("type", Params.RENEWAL);
        intent.putExtra("accno",accno);
        context.startActivity(intent);


    }

    public String getSelectedListString(List<AddonResponse.Datum> dataListOrginal)
    {
        List<AddonResponse.Datum> selectedList = new ArrayList<>();
        for(AddonResponse.Datum datum : dataListOrginal)
        {
            if(selectedId.contains(datum.getId()+""))
            {
                selectedList.add(datum);
            }
        }

        String data = new Gson().toJson(selectedList).trim();
        Log.d(TAG, "getSelectedListString: "+data);
        return  data;

    }


    @Override
    public int getItemCount() {
        return filtereddataList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtereddataList = dataList;
                } else {
                    List<AddonResponse.Datum>filteredList = new ArrayList<>();
                    for (AddonResponse.Datum row : dataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filtereddataList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filtereddataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtereddataList = (ArrayList<AddonResponse.Datum>) filterResults.values;
                notifyDataSetChanged();
            }
        };

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView  tv_addon_name;
        CheckBox chkbox;
        ImageView iv_view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_addon_name = itemView.findViewById(R.id.tv_addon_name);
            chkbox = itemView.findViewById(R.id.chkbox);
            iv_view = itemView.findViewById(R.id.iv_view);

        }
    }
}
