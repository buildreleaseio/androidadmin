package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Activity.BulkOperationsActivity;
import io.buildrelease.adminjpr.Activity.CustomerDetailActivity;
import io.buildrelease.adminjpr.Model.BulkSubcriberAccountResponse;
import io.buildrelease.adminjpr.Model.BulkSubcriberAssignmentResponse;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.interfaces.SelectedIdListInterface;

public class BulkOperationsAdapter extends RecyclerView.Adapter<BulkOperationsAdapter.DataObjectHolder> {


    View view;
    List<BulkSubcriberAccountResponse.DataBean> datumArrayListActive;
    List<BulkSubcriberAssignmentResponse.DataBean> dataBeans =new ArrayList<>();
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    SelectedIdListInterface lisener;
    ArrayList<String> selectedIdList =new ArrayList<>();
    Context context;
    String type;


    public BulkOperationsAdapter(Context context, List<BulkSubcriberAccountResponse.DataBean> datumArrayListActive, SelectedIdListInterface lisener,String type) {
        this.datumArrayListActive = datumArrayListActive;
        this.context = context;
        this.lisener = lisener;
        this.type = type;
    }

    public void markSelected()
    {
        selectedIdList.clear();
        for(BulkSubcriberAccountResponse.DataBean data : datumArrayListActive)
        {
            selectedIdList.add(data.getId()+"");
        }
        lisener.selectedIds(selectedIdList);
        notifyDataSetChanged();
    }


    public void markUnSelected()
    {
        selectedIdList.clear();
        notifyDataSetChanged();
        lisener.selectedIds(selectedIdList);

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.active_row_item,viewGroup,false);
        return new BulkOperationsAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, final int i) {


        final BulkSubcriberAccountResponse.DataBean  activeAccountData = datumArrayListActive.get(i);

                holder.name.setText(activeAccountData.getCustomerName());
                holder.customerID.setText(activeAccountData.getCustomerId()+"");
                holder.address.setText(activeAccountData.getAddress());
                holder.mobile.setText(activeAccountData.getMobileNo());
                holder.vcNumber.setText(activeAccountData.getSmartcardno());
                holder.status.setText(activeAccountData.getStatusLbl());
                holder.brand.setText(activeAccountData.getBoxtypeLbl());
                holder.expDate.setText(CommonUtils.dateFormat(activeAccountData.getDeactivationDate()));

                if(datumArrayListActive.get(i).getStatusLbl().trim().equalsIgnoreCase("active")){
                    holder.status.setTextColor(context.getResources().getColor(R.color.colorGreen));
                }else{
                    holder.status.setTextColor(context.getResources().getColor(R.color.colorRed));
                }


                checkCheckedState(holder,activeAccountData);
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        updateList();
                        if(b)
                        {
                            if(!selectedIdList.contains(datumArrayListActive.get(i).getId())) {
                                selectedIdList.add(datumArrayListActive.get(i).getId());
                            }
                        }else
                        {
                            ((BulkOperationsActivity) context).unCheckSelectedAll();
                            if(selectedIdList.contains(datumArrayListActive.get(i).getId())) {
                                selectedIdList.remove(datumArrayListActive.get(i).getId());
                            }
                        }

                        updateList();

                    }
                });
    }

    public void updateList()
    {
        lisener.selectedIds(selectedIdList);
    }

    public void checkCheckedState(DataObjectHolder holder, BulkSubcriberAccountResponse.DataBean  subscriberList)
    {
            holder.checkBox.setOnCheckedChangeListener(null);
            if(selectedIdList.contains(subscriberList.getId()))
            {
                holder.checkBox.setChecked(true);
            }else
            {
                holder.checkBox.setChecked(false);
            }
    }

    @Override
    public int getItemCount() {
        return datumArrayListActive.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView vcNumber,status,brand,expDate,name,address,mobile,customerID;
        CheckBox checkBox;
        LinearLayout container;


        public DataObjectHolder(View itemView) {
            super(itemView);
            vcNumber     = itemView.findViewById(R.id.vcnumber_value);
            customerID     = itemView.findViewById(R.id.customer_id_value);
            status   = itemView.findViewById(R.id.status_value);
            brand   = itemView.findViewById(R.id.brand_value);
            expDate   = itemView.findViewById(R.id.exp_date_value);
            name   = itemView.findViewById(R.id.name_tv);
            address   = itemView.findViewById(R.id.address_tv);
            mobile   = itemView.findViewById(R.id.mobile_value);
            checkBox   = itemView.findViewById(R.id.checkbox_active_list);
            container   = itemView.findViewById(R.id.container);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CustomerDetailActivity.class);
                    intent.putExtra("customer_id",datumArrayListActive.get(getAdapterPosition()).getCustomerId());
                    context.startActivity(intent);
                }
            });
        }
    }
}
