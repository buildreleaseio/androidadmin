package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.buildrelease.adminjpr.Model.RenewPlanReponse;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.interfaces.SelectedRenewalInterface;


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder>  {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    Context context;
    View view;
    List<RenewPlanReponse.Datum> dataList;



    public CartAdapter(Context context, List<RenewPlanReponse.Datum> dataList, SelectedRenewalInterface selectedRenewalInterface) {


        this.context = context;
        this.dataList = dataList;

    }

    public CartAdapter() {

    }

    @NonNull
    @Override
    public CartAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_cart_on, viewGroup, false);
        CartAdapter.MyViewHolder myViewHolder = new  CartAdapter.MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CartAdapter.MyViewHolder myViewHolder, final  int i) {
        RenewPlanReponse.Datum data = dataList.get(i);
        myViewHolder.tv_addon_name.setText(data.getName());
        myViewHolder.type.setText(data.getTypeLbl());

        }



    @Override
    public int getItemCount() {
        return dataList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView  tv_addon_name,type;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_addon_name = itemView.findViewById(R.id.tv_addon_name);
            type = itemView.findViewById(R.id.type);
        }
    }
}
