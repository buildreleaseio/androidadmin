package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Activity.BulkRechargePeriodActivity;
import io.buildrelease.adminjpr.Activity.ChannelListActivity;
import io.buildrelease.adminjpr.Activity.RechargePeriodActivity;
import io.buildrelease.adminjpr.Model.NewBasePlan;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.Params;


public class ChangeBasePlanListAdapter extends RecyclerView.Adapter<ChangeBasePlanListAdapter.MyViewHolder>  implements Filterable {

    Context context;
    View view;
    List<NewBasePlan.Datum> dataList;
    List<NewBasePlan.Datum> filtereddataList;
    List<String> selectedId = new ArrayList<>();
    String accountno;
    String type=null;


    public ChangeBasePlanListAdapter(Context context, List<NewBasePlan.Datum> dataList,String accno) {
        this.context = context;
        this.dataList = dataList;
        filtereddataList = dataList;
        this.accountno=accno;
    }

    public ChangeBasePlanListAdapter(Context context, List<NewBasePlan.Datum> dataList, List<String> accountids,String type) {
        this.context = context;
        this.dataList = dataList;
        this.selectedId=accountids;
        this.type=type;
    }


    @NonNull
    @Override
    public ChangeBasePlanListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_change_baseplan, viewGroup, false);
        ChangeBasePlanListAdapter.MyViewHolder myViewHolder = new  ChangeBasePlanListAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ChangeBasePlanListAdapter.MyViewHolder myViewHolder, final int i) {
        if(type!=null)
        {
            final NewBasePlan.Datum data = dataList.get(i);
            myViewHolder.tv_addon_name.setText(data.getName()+"");
            myViewHolder.iv_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChannelListActivity.class);
                    intent.putExtra("id",dataList.get(i).getId()+"");
                    context.startActivity(intent);
                }
            });
        }
        else
        {
            final NewBasePlan.Datum data = filtereddataList.get(i);
            myViewHolder.tv_addon_name.setText(data.getName()+"");
            myViewHolder.iv_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ChannelListActivity.class);
                    intent.putExtra("id",filtereddataList.get(i).getId()+"");
                    context.startActivity(intent);
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        if(type!=null)
        {
            return dataList.size();
        }
        else
        {
            return filtereddataList.size();
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtereddataList = dataList;
                } else {
                    List<NewBasePlan.Datum>filteredList = new ArrayList<>();
                    for (NewBasePlan.Datum row : dataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filtereddataList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filtereddataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtereddataList = (ArrayList<NewBasePlan.Datum>) filterResults.values;
                notifyDataSetChanged();
            }
        };

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView  tv_addon_name;
        ImageView iv_view;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_addon_name = itemView.findViewById(R.id.tv_addon_name);
            iv_view = itemView.findViewById(R.id.iv_view);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent;

                    if(type!=null)
                    {
                        intent  = new Intent(context, BulkRechargePeriodActivity.class);
                        intent.putExtra("id",String.valueOf(dataList.get(getAdapterPosition()).getId()));
                        intent.putStringArrayListExtra("accountnolist",new ArrayList<>(selectedId));
                        intent.putExtra("type", Params.BULKBOUQUETSREPLACEMENT);
                    }
                    else
                    {
                        intent  = new Intent(context, RechargePeriodActivity.class);
                        intent.putExtra("id",String.valueOf(dataList.get(getAdapterPosition()).getId()));
                        intent.putExtra("accno",accountno);
                        intent.putExtra("type", Params.CHANGE_BASE_PLAN);
                    }
                    context.startActivity(intent);
                }
            });

        }
    }
}
