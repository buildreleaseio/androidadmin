package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;
import io.buildrelease.adminjpr.Model.ChannelListData;
import io.buildrelease.adminjpr.R;

public class ChannelListAdapter extends RecyclerView.Adapter<ChannelListAdapter.MyViewHolder>   implements Filterable {

    Context context;
    View view;
    List<ChannelListData.ChannelList> dataList;
    List<ChannelListData.ChannelList> filtereddataList;


    public ChannelListAdapter(Context context, List<ChannelListData.ChannelList> dataList) {
        this.context = context;
        this.dataList = dataList;
        filtereddataList = dataList;
    }

    public ChannelListAdapter() {

    }

    @NonNull
    @Override
    public ChannelListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_channel_item, viewGroup, false);
        ChannelListAdapter.MyViewHolder myViewHolder = new  ChannelListAdapter.MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChannelListAdapter.MyViewHolder myViewHolder, int i) {
        ChannelListData.ChannelList  data = filtereddataList.get(i);
//
        myViewHolder.tv_channelname.setText(data.getName());

        Glide.with(context).load(data.getLogoUrl())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(myViewHolder.iv_logo);
    }

    @Override
    public int getItemCount() {
        return filtereddataList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filtereddataList = dataList;
                } else {
                    List<ChannelListData.ChannelList> filteredList = new ArrayList<>();
                    for (ChannelListData.ChannelList row : dataList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    filtereddataList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filtereddataList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtereddataList = (ArrayList<ChannelListData.ChannelList>) filterResults.values;
                notifyDataSetChanged();
            }
        };

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView  tv_channelname;
        ImageView iv_logo;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_channelname = itemView.findViewById(R.id.tv_channelname);
            iv_logo = itemView.findViewById(R.id.iv_logo);

        }
    }
}
