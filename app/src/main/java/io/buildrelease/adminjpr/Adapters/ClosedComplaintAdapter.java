package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import io.buildrelease.adminjpr.Model.ClosedComplaintsData;
import io.buildrelease.adminjpr.R;

public class ClosedComplaintAdapter extends RecyclerView.Adapter<ClosedComplaintAdapter.DataObjectHolder> {


    View view;
    ClosedComplaintsData closedComplaintsData;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;
    List<ClosedComplaintsData.Datum> complainLIst;


    public ClosedComplaintAdapter(ClosedComplaintsData closedComplaintsData, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.closedComplaintsData = closedComplaintsData;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;

    }


    public ClosedComplaintAdapter(List<ClosedComplaintsData.Datum> complainLIst, Context context) {
        this.complainLIst = complainLIst;
        this.context = context;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.closed_complaints_row_item, viewGroup, false);
        return new ClosedComplaintAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {
        ClosedComplaintsData.Datum datum = complainLIst.get(i);
//                holder.complaintNo.setText(closedComplaintsData.getData().get(i).getTicketno() != null ? closedComplaintsData.getData().get(i).getTicketno() : "---");
//                holder.status.setText(closedComplaintsData.getData().get(i).getStatusLbl() != null ? closedComplaintsData.getData().get(i).getStatusLbl() : "---");
//                holder.userName.setText(closedComplaintsData.getData().get(i).getCreatedByLbl() != null ? closedComplaintsData.getData().get(i).getCreatedByLbl() : "---");
//                holder.date.setText(closedComplaintsData.getData().get(i).getCreatedAt() != null ? closedComplaintsData.getData().get(i).getCreatedAt() : "---");
//                holder.status.setTextColor(context.getResources().getColor(R.color.colorGreen));

                holder.complaintNo.setText(datum.getTicketno() != null ? datum.getTicketno() : "---");
                holder.status.setText(datum.getStatusLbl() != null ? datum.getStatusLbl() : "---");
                holder.userName.setText(datum.getNameLbl() != null ? datum.getNameLbl() : "---");
                holder.date.setText(datum.getCreatedAt() != null ? datum.getCreatedAt() : "---");
                holder.status.setTextColor(context.getResources().getColor(R.color.colorGreen));
    }

    @Override
    public int getItemCount() {
        return complainLIst.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView complaintNo, status, userName, date;
        LinearLayout container;

        public DataObjectHolder(View itemView) {
            super(itemView);
            complaintNo = itemView.findViewById(R.id.complaint_num_value);
            status = itemView.findViewById(R.id.status_value);
            date = itemView.findViewById(R.id.complaint_data_value);
            userName = itemView.findViewById(R.id.username_value);
//            inventoryType = itemView.findViewById(R.id.inventory_type_tv);
            container = itemView.findViewById(R.id.container);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

        }
    }
}
