package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import io.buildrelease.adminjpr.Activity.CustomerVCDetailActivity;
import io.buildrelease.adminjpr.Model.CustomerDetail;
import io.buildrelease.adminjpr.R;

public class CustomerVCListAdapter extends RecyclerView.Adapter<CustomerVCListAdapter.DataObjectHolder> {



    Context context;
    List<CustomerDetail.AccountList> accountLists;
    String customerName,customerId;



    public CustomerVCListAdapter(Context context, List<CustomerDetail.AccountList> accountLists,String customerName,String customerId) {
        this.context = context;
        this.accountLists = accountLists;
        this.customerName = customerName;
        this.customerId = customerId;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_vc_list_customer,viewGroup,false);
        return new CustomerVCListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {
        CustomerDetail.AccountList accountList = accountLists.get(i);
        holder.tv_status.setText(accountList.getStatusLbl());
        holder.tv_vc_number.setText(accountList.getSmartcardno());
    }

    @Override
    public int getItemCount() {
      return accountLists.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView tv_vc_number,tv_status;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_vc_number = itemView.findViewById(R.id.tv_vc_number);
            tv_status = itemView.findViewById(R.id.tv_status);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context,CustomerVCDetailActivity.class);
                    intent.putExtra("name",customerName);
                    intent.putExtra("id",customerId);
                    String data = new Gson().toJson(accountLists.get(getAdapterPosition()));
                    intent.putExtra("data",data);
                    context.startActivity(intent);
                }
            });


        }
    }
}
