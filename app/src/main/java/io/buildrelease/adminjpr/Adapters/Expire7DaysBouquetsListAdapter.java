package io.buildrelease.adminjpr.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Activity.BulkBouquetsRenewalAccountActivity;
import io.buildrelease.adminjpr.Model.ExpiredSevenDaysBouques;
import io.buildrelease.adminjpr.R;

public class Expire7DaysBouquetsListAdapter extends RecyclerView.Adapter<Expire7DaysBouquetsListAdapter.DataObjectHolder> {


    Context context;
    List<ExpiredSevenDaysBouques.Datum> datumArrayListExpire7DaysData=new ArrayList<>();




    public Expire7DaysBouquetsListAdapter(Context context,List<ExpiredSevenDaysBouques.Datum>  datumArrayListExpire7DaysData) {
        //this.fragmentManager = fragmentManager;
        this.context = context;
        this.datumArrayListExpire7DaysData = datumArrayListExpire7DaysData;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_7daysexpired_bouquets,viewGroup,false);
        return new Expire7DaysBouquetsListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {
        ExpiredSevenDaysBouques.Datum datum = datumArrayListExpire7DaysData.get(i);
        holder.tv_count.setText(datum.getCounts()+"");
        holder.tv_name.setText(datum.getName());

    }

    @Override
    public int getItemCount() {
        return datumArrayListExpire7DaysData.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

        TextView tv_name,tv_count;

        public DataObjectHolder(View itemView) {
            super(itemView);
            tv_name = itemView.findViewById(R.id.tv_name);
             tv_count = itemView.findViewById(R.id.tv_count);
             itemView.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View view) {
                        Intent intent = new Intent(context, BulkBouquetsRenewalAccountActivity.class);

                        intent.putExtra("bid",datumArrayListExpire7DaysData.get(getAdapterPosition()).getBouqueId());
                        ((Activity)context).startActivity(intent);
                 }
             });

        }
    }
}
