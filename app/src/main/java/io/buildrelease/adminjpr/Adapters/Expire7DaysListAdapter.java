package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Activity.CustomerDetailActivity;
import io.buildrelease.adminjpr.Model.Expire7DaysData;
import io.buildrelease.adminjpr.Model.expird7daysbouquetswiseresponse;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.interfaces.SelectedIdListInterface;

public class Expire7DaysListAdapter extends RecyclerView.Adapter<Expire7DaysListAdapter.DataObjectHolder> {


    View view;
    Expire7DaysData expire7DaysData;
    //FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;
    SelectedIdListInterface lisener;
    ArrayList<String> selectedIdList =new ArrayList<>();
    List<expird7daysbouquetswiseresponse.Datum> datumArrayListExpire7DaysData=new ArrayList<>();




    public void clearSelectedList()
    {
        selectedIdList.clear();
    }

    public Expire7DaysListAdapter(Expire7DaysData expire7DaysData, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.expire7DaysData = expire7DaysData;
        //this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;
    }

    public Expire7DaysListAdapter( Context context, List<expird7daysbouquetswiseresponse.Datum> datumArrayListExpire7DaysData, SelectedIdListInterface lisener) {
        //this.fragmentManager = fragmentManager;
        this.context = context;
        this.datumArrayListExpire7DaysData = datumArrayListExpire7DaysData;
        this.lisener = lisener;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sevendays_expired_account_row_item,viewGroup,false);
        return new Expire7DaysListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder,final  int i) {

            final expird7daysbouquetswiseresponse.Datum  expire7DaysData = datumArrayListExpire7DaysData.get(i);

                holder.name.setText(expire7DaysData.getCustomerName()!=null?expire7DaysData.getCustomerName().trim():"---");
                holder.customerID.setText(expire7DaysData.getCustomerId()!=null?expire7DaysData.getCustomerId().trim():"---");
                holder.vcNumber.setText(expire7DaysData.getSmartcardno()!=null?expire7DaysData.getSmartcardno().trim():"---");
//                holder.status.setText(expire7DaysData.getData().get(i).getStatus()!=null?""+expire7DaysData.getData().get(i).getStatus():"---");
                holder.status.setText("EXPIRING SOON");
//                holder.service.setText(expire7DaysData.getOperatorNameLbl()!=null?expire7DaysData.getOperatorNameLbl().trim():"---");
                holder.expDate.setText(CommonUtils.dateFormat(expire7DaysData.getDeactivationDate())!=null?expire7DaysData.getDeactivationDate().trim():"---");
                holder.bouqueType.setText(expire7DaysData.getBouque_lbl());
//
//                if(expire7DaysData.getBrandLbl()!=null) {
//
//
//                    switch (expire7DaysData.getBouqueType()) {
//                        case 1: {
//                            holder.bouqueType.setText("Base");
//                            break;
//                        }
//                        case 2: {
//                            holder.bouqueType.setText("Addon");
//                            break;
//                        }
//                        case 3: {
//                            holder.bouqueType.setText("A la carte");
//                            break;
//                        }
//                        default: {
//                            holder.bouqueType.setText("---");
//                        }
//                    }
//                }else
//                {
//                    holder.bouqueType.setText("---");
//                }

//                if(recentlyExpiredData.getData().get(i).getStatus().trim().equalsIgnoreCase("active")){
                    holder.status.setTextColor(context.getResources().getColor(R.color.colorRed));
//                }else{
//                    holder.status.setTextColor(context.getResources().getColor(R.color.colorRed));
//                }

                 checkCheckedState(holder,expire7DaysData);
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        updateList();
                        if(isChecked)
                        {
                            if(!selectedIdList.contains(datumArrayListExpire7DaysData.get(i).getId())) {
                                selectedIdList.add(datumArrayListExpire7DaysData.get(i).getId());
                            }
                        }else
                        {
                            if(selectedIdList.contains(datumArrayListExpire7DaysData.get(i).getId())) {
                                selectedIdList.remove(datumArrayListExpire7DaysData.get(i).getId());
                            }
                        }

                        updateList();
                    }
                });

    }


    public void updateList()
    {
        lisener.selectedIds(selectedIdList);
    }

    public void checkCheckedState(Expire7DaysListAdapter.DataObjectHolder holder, expird7daysbouquetswiseresponse.Datum  expire7DaysData)
    {
        holder.checkBox.setOnCheckedChangeListener(null);
        if(selectedIdList.contains(expire7DaysData.getId()))
        {
            holder.checkBox.setChecked(true);
        }else
        {
            holder.checkBox.setChecked(false);
        }
    }



    @Override
    public int getItemCount() {
        return datumArrayListExpire7DaysData.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView vcNumber,status,service,expDate,name,customerID,bouqueType;
        CheckBox checkBox;
        LinearLayout container;


        public DataObjectHolder(View itemView) {
            super(itemView);
            vcNumber     = itemView.findViewById(R.id.vcnumber_value);
            customerID     = itemView.findViewById(R.id.customer_id_value);
            status   = itemView.findViewById(R.id.status_value);
//            service   = itemView.findViewById(R.id.service_value);
            expDate   = itemView.findViewById(R.id.exp_date_value);
            bouqueType   = itemView.findViewById(R.id.bouque_type_value);
            name   = itemView.findViewById(R.id.name_tv);
            checkBox   = itemView.findViewById(R.id.checkbox_active_list);
            container   = itemView.findViewById(R.id.container);

            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CustomerDetailActivity.class);
                    intent.putExtra("customer_id",datumArrayListExpire7DaysData.get(getAdapterPosition()).getCustomerId());
                    context.startActivity(intent);
                }
            });
        }
    }
}
