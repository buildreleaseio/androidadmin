package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Activity.CustomerDetailActivity;
import io.buildrelease.adminjpr.Model.InactiveAccountData;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;

public class InactiveListAdapter extends RecyclerView.Adapter<InactiveListAdapter.DataObjectHolder> {


    View view;
    InactiveAccountData inactiveAccountData;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;
    List<InactiveAccountData.Datum> datumArrayListInactive =new ArrayList<>();


    public InactiveListAdapter(InactiveAccountData inactiveAccountData, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.inactiveAccountData = inactiveAccountData;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;
    }

    public InactiveListAdapter(List<InactiveAccountData.Datum> inactiveAccountData, FragmentManager fragmentManager, Context context) {
        this.datumArrayListInactive = inactiveAccountData;
        this.fragmentManager = fragmentManager;
        this.context = context;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inactive_row_item,viewGroup,false);
        return new InactiveListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {
        final InactiveAccountData.Datum  inactiveAccountData = datumArrayListInactive.get(i);
                holder.name.setText(inactiveAccountData.getCustomerName()!=null? inactiveAccountData.getCustomerName().trim():"---");
                holder.customerID.setText(inactiveAccountData.getCustomerId()!=null? inactiveAccountData.getCustomerId().trim():"---");
                holder.address.setText(inactiveAccountData.getAddress()!=null? inactiveAccountData.getAddress().trim():"---");
                holder.mobile.setText(inactiveAccountData.getMobileNo()!=null? inactiveAccountData.getMobileNo().trim():"---");
                holder.vcNumber.setText(inactiveAccountData.getSmartcardno()!=null? inactiveAccountData.getSmartcardno().trim():"---");
                holder.status.setText(inactiveAccountData.getStatusLbl()!=null? inactiveAccountData.getStatusLbl().trim():"---");
                holder.brand.setText(inactiveAccountData.getBoxtypeLbl()!=null? inactiveAccountData.getBoxtypeLbl().trim():"---");
                holder.expDate.setText(CommonUtils.dateFormat(inactiveAccountData.getDeactivationDate())!=null? inactiveAccountData.getDeactivationDate().trim():"---");
                if(inactiveAccountData.getStatusLbl().trim().equalsIgnoreCase("active")){
                    holder.status.setTextColor(context.getResources().getColor(R.color.colorGreen));
                }else{
                    holder.status.setTextColor(context.getResources().getColor(R.color.colorRed));
                }
    }

    @Override
    public int getItemCount() {
        return datumArrayListInactive.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView vcNumber,status,brand,expDate,name,address,mobile,customerID;
        LinearLayout container;


        public DataObjectHolder(View itemView) {
            super(itemView);
            vcNumber     = itemView.findViewById(R.id.vcnumber_value);
            customerID     = itemView.findViewById(R.id.customer_id_value);
            status   = itemView.findViewById(R.id.status_value);
            brand   = itemView.findViewById(R.id.brand_value);
            expDate   = itemView.findViewById(R.id.exp_date_value);
            name   = itemView.findViewById(R.id.name_tv);
            address   = itemView.findViewById(R.id.address_tv);
            mobile   = itemView.findViewById(R.id.mobile_value);
            container   = itemView.findViewById(R.id.container);

            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CustomerDetailActivity.class);
                    intent.putExtra("customer_id",datumArrayListInactive.get(getAdapterPosition()).getCustomerId());
                    context.startActivity(intent);
                }
            });
        }
    }
}
