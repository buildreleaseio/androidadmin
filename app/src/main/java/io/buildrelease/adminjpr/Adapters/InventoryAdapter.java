package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.buildrelease.adminjpr.Activity.PairingListActivity;
import io.buildrelease.adminjpr.Model.InventoryData;
import io.buildrelease.adminjpr.R;

public class InventoryAdapter extends RecyclerView.Adapter<InventoryAdapter.DataObjectHolder> {


    View view;
    InventoryData inventoryData;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;



    public InventoryAdapter(InventoryData inventoryData,FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.inventoryData = inventoryData;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.inventory_row_item,viewGroup,false);
        return new InventoryAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {
        switch (i){
            case 0:
                holder.inventoryType.setText("Embedded");
                holder.arrow.setVisibility(View.GONE);
                holder.total.setText(inventoryData.getData().getEmbeded().getTotal()!=null?inventoryData.getData().getEmbeded().getTotal():"---");
                holder.stock.setText(inventoryData.getData().getEmbeded().getStock()!=null?inventoryData.getData().getEmbeded().getStock():"---");
                holder.alloted.setText(inventoryData.getData().getEmbeded().getAlloted()!=null?inventoryData.getData().getEmbeded().getAlloted():"---");
                holder.assigned.setText(inventoryData.getData().getEmbeded().getAssigned()!=null?inventoryData.getData().getEmbeded().getAssigned():"---");
                break;
            case 1:
                holder.inventoryType.setText("SmartCard");
                holder.arrow.setVisibility(View.GONE);
                holder.total.setText(inventoryData.getData().getSmartcard().getTotal()!=null?inventoryData.getData().getSmartcard().getTotal():"---");
                holder.stock.setText(inventoryData.getData().getSmartcard().getStock()!=null?inventoryData.getData().getSmartcard().getStock():"---");
                holder.alloted.setText(inventoryData.getData().getSmartcard().getAlloted()!=null?inventoryData.getData().getSmartcard().getAlloted():"---");
                holder.assigned.setText(inventoryData.getData().getSmartcard().getAssigned()!=null?inventoryData.getData().getSmartcard().getAssigned():"---");
                break;
            case 2:
                holder.inventoryType.setText("STB");
                holder.arrow.setVisibility(View.GONE);
                holder.total.setText(inventoryData.getData().getStb().getTotal()!=null?inventoryData.getData().getStb().getTotal():"---");
                holder.stock.setText(inventoryData.getData().getStb().getStock()!=null?inventoryData.getData().getStb().getStock():"---");
                holder.alloted.setText(inventoryData.getData().getStb().getAlloted()!=null?inventoryData.getData().getStb().getAlloted():"---");
                holder.assigned.setText(inventoryData.getData().getStb().getAssigned()!=null?inventoryData.getData().getStb().getAssigned():"---");
                break;
            case 3:
                holder.inventoryType.setText("Pairing");
                holder.arrow.setVisibility(View.VISIBLE);
                holder.total.setText(inventoryData.getData().getPairing().getTotal()!=null?inventoryData.getData().getPairing().getTotal():"---");
                holder.stock.setText(inventoryData.getData().getPairing().getStock()!=null?inventoryData.getData().getPairing().getStock():"---");
                holder.alloted.setText(inventoryData.getData().getPairing().getAlloted()!=null?inventoryData.getData().getPairing().getAlloted():"---");
                holder.assigned.setText(inventoryData.getData().getPairing().getAssigned()!=null?inventoryData.getData().getPairing().getAssigned():"---");
                holder.container.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent =new Intent(context, PairingListActivity.class);
                        context.startActivity(intent);
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView total,stock,alloted,assigned,inventoryType;
        LinearLayout container;
        ImageView arrow;

        public DataObjectHolder(View itemView) {
            super(itemView);
            total     = itemView.findViewById(R.id.total_value);
            stock   = itemView.findViewById(R.id.stock_value);
            alloted   = itemView.findViewById(R.id.alloted_value);
            assigned   = itemView.findViewById(R.id.assigned_value);
            inventoryType   = itemView.findViewById(R.id.inventory_type_tv);
            container   = itemView.findViewById(R.id.container);
            arrow   = itemView.findViewById(R.id.arrow);
            arrow.setVisibility(View.GONE);

        }
    }
}
