package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.buildrelease.adminjpr.Model.CustomerMyPlansResponse;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;


public class MyPlansAdapter extends RecyclerView.Adapter<MyPlansAdapter.MyPlansViewHolder> {
    List<CustomerMyPlansResponse.DataBean> dataList;
    Context context;

    public MyPlansAdapter(Context context, List<CustomerMyPlansResponse.DataBean> dataList) {
        this.context=context;
        this.dataList=dataList;
    }

    @NonNull
    @Override
    public MyPlansViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_subscriptions, viewGroup, false);
        return new  MyPlansAdapter.MyPlansViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyPlansViewHolder myPlansViewHolder, int i) {
        CustomerMyPlansResponse.DataBean datum=dataList.get(i);


        myPlansViewHolder.tv_basename.setText(datum.getName() + " (" + datum.getBoxtype_lbl() + ")");
        myPlansViewHolder.tv_plan_type.setText( datum.getType_lbl().toUpperCase());
        myPlansViewHolder.tv_baseplan_status.setTextColor(context.getResources().getColor(CommonUtils.statusColor(datum.getStatus_lbl())));
        myPlansViewHolder.tv_baseplan_status.setText(datum.getStatus_lbl());
        myPlansViewHolder.tv_deactivationDate.setText(CommonUtils.dateFormat(datum.getDeactivation_date()));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyPlansViewHolder  extends  RecyclerView.ViewHolder{
        TextView tv_basename,tv_baseplan_status,tv_deactivationDate,tv_plan_type;
        MyPlansViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_basename=itemView.findViewById(R.id.tv_basename);
            tv_baseplan_status=itemView.findViewById(R.id.tv_baseplan_status);
            tv_deactivationDate=itemView.findViewById(R.id.tv_deactivationDate);
            tv_plan_type=itemView.findViewById(R.id.baseplan);
        }
    }
}