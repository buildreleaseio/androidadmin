package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import io.buildrelease.adminjpr.Activity.OpenComplainActivity;
import io.buildrelease.adminjpr.Activity.OpenComplainDetailActivity;
import io.buildrelease.adminjpr.Model.OpenComplaintsData;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;

import static io.buildrelease.adminjpr.Utility.CommonUtils.fullDateFormat;

public class OpenComplaintAdapter extends RecyclerView.Adapter<OpenComplaintAdapter.DataObjectHolder> {


    View view;
    OpenComplaintsData openComplaintsData;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    List<OpenComplaintsData.Datum> opendlist;
    Context context;


    public OpenComplaintAdapter(OpenComplaintsData openComplaintsData, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.openComplaintsData = openComplaintsData;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;
    }


    public OpenComplaintAdapter(Context context,List<OpenComplaintsData.Datum> opendlist){
        this.context = context;
        this.opendlist = opendlist;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.complaints_row_item, viewGroup, false);
        return new OpenComplaintAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, final int i) {
        OpenComplaintsData.Datum datum = opendlist.get(i);

        if(datum.getStatusLbl().equalsIgnoreCase("Closed"))
        {
            holder.arrow.setVisibility(View.GONE);
        }
        else
        {
            holder.arrow.setVisibility(View.VISIBLE);
        }
        holder.complaintNo.setText(datum.getTicketno() != null ? datum.getTicketno() : "---");
        holder.status.setText(datum != null ? datum.getStatusLbl() : "---");
        holder.status.setTextColor(CommonUtils.statusColor(datum.getStatusLbl().toLowerCase().trim()));
        holder.userName.setText(datum.getNameLbl() != null ? datum.getNameLbl() : "---");
        if(datum.getCreatedAt().equals(null))
            holder.date.setText("---");
        else
            holder.date.setText(fullDateFormat(datum.getCreatedAt()));
        holder.date.setText(fullDateFormat(datum.getCreatedAt()) != null ? datum.getCreatedAt() : "---");
        holder.arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final MaterialDialog.Builder builder = new MaterialDialog.Builder(Objects.requireNonNull(context));
                    builder.autoDismiss(true);
                    builder.cancelable(true);
                    builder.title("Remark")
                            .inputRange(0, 255)
                            .positiveColor(context.getResources().getColor(R.color.colorPrimary))
                            .negativeColor(context.getResources().getColor(R.color.colorPrimary))
                            .input("Type your comment here", "", false, new MaterialDialog.InputCallback() {
                                @Override
                                public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                                    ((OpenComplainActivity)context).closeComplaint(input.toString(),opendlist.get(i).getId()+"");
                                }
                            })
                            .positiveText("Submit")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//                            context.startActivity(new Intent(context, VerificationActivity.class));
//                            gotoAppPlaystore(context);
                                }
                            })
                            .negativeText("Cancel")
                            .onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    builder.cancelable(true);
                                }
                            })
                            .canceledOnTouchOutside(true)
                            .show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return opendlist.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView complaintNo, status, userName, date;
        LinearLayout container;
        ImageView arrow;

        public DataObjectHolder(View itemView) {
            super(itemView);
            complaintNo = itemView.findViewById(R.id.complaint_num_value);
            status = itemView.findViewById(R.id.status_value);
            date = itemView.findViewById(R.id.complaint_data_value);
            userName = itemView.findViewById(R.id.username_value);
//            inventoryType = itemView.findViewById(R.id.inventory_type_tv);
            container = itemView.findViewById(R.id.container);
            arrow = itemView.findViewById(R.id.chat_iv);
            arrow.setVisibility(View.GONE);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!opendlist.get(getAdapterPosition()).getStatusLbl().trim().equalsIgnoreCase("closed")) {
                        Intent intent = new Intent(context, OpenComplainDetailActivity.class);
                        intent.putExtra("id", String.valueOf(opendlist.get(getAdapterPosition()).getId()));
                        intent.putExtra("name_lbl", opendlist.get(getAdapterPosition()).getNameLbl());
                        intent.putExtra("smartcard_lbl", opendlist.get(getAdapterPosition()).getSmartcardLbl());
                        intent.putExtra("ticketno", opendlist.get(getAdapterPosition()).getTicketno());
                        intent.putExtra("priority", opendlist.get(getAdapterPosition()).getPriorityLbl());
                        intent.putExtra("location_lbl", opendlist.get(getAdapterPosition()).getLocationLbl());
                        context.startActivity(intent);
                    }else
                    {
                        Toasty.warning(context,"Complain already Closed").show();
                    }
                }
            });
        }
    }
}
