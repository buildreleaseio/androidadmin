package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.buildrelease.adminjpr.Model.ParingListData;
import io.buildrelease.adminjpr.R;

public class PairingListAdapter extends RecyclerView.Adapter<PairingListAdapter.DataObjectHolder> {


    View view;
    ParingListData paringListData;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;



    public PairingListAdapter(ParingListData paringListData, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.paringListData = paringListData;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pairing_row_item,viewGroup,false);
        return new PairingListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {


                holder.vcNumber.setText(paringListData.getData().get(i).getSmartcardno()!=null?paringListData.getData().get(i).getSmartcardno():"---");
                holder.status.setText(paringListData.getData().get(i).getStatusLbl()!=null?paringListData.getData().get(i).getStatusLbl():"---");
                holder.brand.setText(paringListData.getData().get(i).getBrandLbl()!=null?paringListData.getData().get(i).getBrandLbl():"---");
                holder.operator.setText(paringListData.getData().get(i).getOperatorLbl()!=null?paringListData.getData().get(i).getOperatorLbl():"---");

    }

    @Override
    public int getItemCount() {
        return paringListData.getData().size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView vcNumber,status,brand,operator;
        LinearLayout container;


        public DataObjectHolder(View itemView) {
            super(itemView);
            vcNumber     = itemView.findViewById(R.id.vcnumber_value);
            status   = itemView.findViewById(R.id.status_value);
            brand   = itemView.findViewById(R.id.brand_value);
            operator   = itemView.findViewById(R.id.operator_value);
            container   = itemView.findViewById(R.id.container);

        }
    }
}
