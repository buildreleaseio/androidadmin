package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Activity.CustomerDetailActivity;
import io.buildrelease.adminjpr.Model.RecentlyExpiredData;
import io.buildrelease.adminjpr.R;

public class RecentlyExpiredListAdapter extends RecyclerView.Adapter<RecentlyExpiredListAdapter.DataObjectHolder> {


    View view;
    RecentlyExpiredData recentlyExpiredData;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;
    List<RecentlyExpiredData.Datum> datumArrayListRecentlyExpired =new ArrayList<>();


    public RecentlyExpiredListAdapter(RecentlyExpiredData recentlyExpiredData, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.recentlyExpiredData = recentlyExpiredData;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;
    }

    public RecentlyExpiredListAdapter(FragmentManager fragmentManager, Context context, List<RecentlyExpiredData.Datum> datumArrayListRecentlyExpired) {
        this.fragmentManager = fragmentManager;
        this.context = context;
        this.datumArrayListRecentlyExpired = datumArrayListRecentlyExpired;
    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recently_expired_row_item,viewGroup,false);
        return new RecentlyExpiredListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {


        final RecentlyExpiredData.Datum  recentlyExpiredData = datumArrayListRecentlyExpired.get(i);

        holder.name.setText(recentlyExpiredData.getCustomerNameLbl()!=null?recentlyExpiredData.getCustomerNameLbl().trim():"---");
        holder.customerID.setText(recentlyExpiredData.getCustomerIdLbl()!=null?recentlyExpiredData.getCustomerIdLbl().trim():"---");
        holder.vcNumber.setText(recentlyExpiredData.getSmartcardnoLbl()!=null?recentlyExpiredData.getSmartcardnoLbl().trim():"---");
//                holder.status.setText(expire7DaysData.getData().get(i).getStatus()!=null?""+expire7DaysData.getData().get(i).getStatus():"---");
        holder.status.setText("EXPIRED");
        holder.service.setText(recentlyExpiredData.getName()!=null?recentlyExpiredData.getName().trim():"---");
        holder.expDate.setText(recentlyExpiredData.getDeactivationDate()!=null?recentlyExpiredData.getDeactivationDate().trim():"---");
        switch (recentlyExpiredData.getBouqueType())
        {
            case 1:
            {
                holder.bouqueType.setText("Base");
                break;
            }
            case 2:
            {
                holder.bouqueType.setText("Addon");
                break;
            }
            case 3:
            {
                holder.bouqueType.setText("A la carte");
                break;
            }
            default:
            {
                holder.bouqueType.setText("---");
            }
        }

//                if(recentlyExpiredData.getData().get(i).getStatus().trim().equalsIgnoreCase("active")){
                 holder.status.setTextColor(context.getResources().getColor(R.color.colorRed2));

    }

    @Override
    public int getItemCount() {
        return datumArrayListRecentlyExpired.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView vcNumber,status,service,expDate,name,customerID,bouqueType;
        LinearLayout container;


        public DataObjectHolder(View itemView) {
            super(itemView);
            vcNumber     = itemView.findViewById(R.id.vcnumber_value);
            customerID     = itemView.findViewById(R.id.customer_id_value);
            status   = itemView.findViewById(R.id.status_value);
            service   = itemView.findViewById(R.id.service_value);
            expDate   = itemView.findViewById(R.id.exp_date_value);
            name   = itemView.findViewById(R.id.name_tv);
            bouqueType   = itemView.findViewById(R.id.bouque_type_value);
            container   = itemView.findViewById(R.id.container);

            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CustomerDetailActivity.class);
                    intent.putExtra("customer_id",datumArrayListRecentlyExpired.get(getAdapterPosition()).getCustomerIdLbl());
                    context.startActivity(intent);
                }
            });

        }
    }
}
