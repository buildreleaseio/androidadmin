package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import io.buildrelease.adminjpr.Model.RechargeResponse;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.interfaces.SelectedPeriodInterfaceAddon;


public class RechargeListAdapter extends RecyclerView.Adapter<RechargeListAdapter.MyViewHolder> {

    Context context;
    View view;
    List<RechargeResponse.Data.Rp0> dataList;
    SelectedPeriodInterfaceAddon selectedPeriodInterfaceAddon;



    public RechargeListAdapter(Context context, List<RechargeResponse.Data.Rp0> dataList, SelectedPeriodInterfaceAddon selectedPeriodInterfaceAddon) {
        this.context = context;
        this.dataList = dataList;
        this.selectedPeriodInterfaceAddon = selectedPeriodInterfaceAddon;
    }

    public RechargeListAdapter() {

    }

    @NonNull
    @Override
    public RechargeListAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_recharge_period, viewGroup, false);
        RechargeListAdapter.MyViewHolder myViewHolder = new  RechargeListAdapter.MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RechargeListAdapter.MyViewHolder myViewHolder, int i) {
        RechargeResponse.Data.Rp0 data = dataList.get(i);
        if (data.getAmount() != null) {
            myViewHolder.tv_amount.setVisibility(View.VISIBLE);
            myViewHolder.tv_amount_heading.setVisibility(View.VISIBLE);
            myViewHolder.tv_amount.setText(data.getAmount() + "");
        } else
        {
            myViewHolder.tv_amount_heading.setVisibility(View.GONE);
            myViewHolder.tv_amount.setVisibility(View.GONE);
        }
         myViewHolder.tv_name.setText(data.getName());



    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView  tv_amount,tv_name,tv_amount_heading;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_name = itemView.findViewById(R.id.tv_name);
            tv_amount_heading = itemView.findViewById(R.id.tv_amount_heading);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedPeriodInterfaceAddon.selectedID(dataList.get(getAdapterPosition()).getId()+"");
                }
            });





        }
    }
}
