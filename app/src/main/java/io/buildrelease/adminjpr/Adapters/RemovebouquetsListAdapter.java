package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import io.buildrelease.adminjpr.Model.RemoveBouquetsResponse;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.interfaces.RemoveBouquetsInterface;

public class RemovebouquetsListAdapter extends RecyclerView.Adapter<RemovebouquetsListAdapter.DataObjectHolder> {


    View view;
    RecyclerView recyclerView;
    Context context;
    List<RemoveBouquetsResponse.Datum> dataList;
    RemoveBouquetsInterface lisner;




    public RemovebouquetsListAdapter(Context context , List<RemoveBouquetsResponse.Datum> dataList, RemoveBouquetsInterface lisner) {
        this.context = context;
        this.dataList = dataList;
       this.lisner = lisner;


    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item_remove_bouquets,viewGroup,false);
        return new RemovebouquetsListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {
        RemoveBouquetsResponse.Datum datum = dataList.get(i);
        holder.tv_amount.setText(datum.getAmount()+"");
        holder.tv_plan_name.setText(datum.getName());
        holder.tv_deactivation_Date.setText(CommonUtils.dateFormat(datum.getDeactivationDate()));
        holder.tv_status.setText(datum.getStatusLbl());


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {


        TextView tv_plan_name,tv_status,tv_amount,tv_deactivation_Date;
        ImageView iv_delete;

        public DataObjectHolder(View itemView) {
            super(itemView);

            tv_plan_name = itemView.findViewById(R.id.tv_plan_name);
            tv_status = itemView.findViewById(R.id.tv_status);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_deactivation_Date = itemView.findViewById(R.id.tv_deactivation_Date);
            iv_delete = itemView.findViewById(R.id.iv_delete);
            iv_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                lisner.remove(dataList.get(getAdapterPosition()).getId()+"");
                }
            });

        }
    }
}
