package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import io.buildrelease.adminjpr.Activity.CustomerDetailActivity;
import io.buildrelease.adminjpr.Activity.DashboardListingActivity;
import io.buildrelease.adminjpr.Model.SuspendedAccountData;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;

public class SuspendedListAdapter extends RecyclerView.Adapter<SuspendedListAdapter.DataObjectHolder> {


    View view;
    SuspendedAccountData suspendedAccountData;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;
    ArrayList<String> accountIdList = new ArrayList<>();


    public SuspendedListAdapter(SuspendedAccountData suspendedAccountData, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.suspendedAccountData = suspendedAccountData;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.active_row_item, viewGroup, false);
        return new SuspendedListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, final int i) {


        holder.name.setText(suspendedAccountData.getData().get(i).getCustomerName() != null ? suspendedAccountData.getData().get(i).getCustomerName().trim() : "---");
        holder.customerID.setText(suspendedAccountData.getData().get(i).getCustomerId() != null ? suspendedAccountData.getData().get(i).getCustomerId().trim() : "---");
        holder.address.setText(suspendedAccountData.getData().get(i).getAddress() != null ? suspendedAccountData.getData().get(i).getAddress().trim() : "---");
        holder.mobile.setText(suspendedAccountData.getData().get(i).getMobileNo() != null ? suspendedAccountData.getData().get(i).getMobileNo().trim() : "---");
        holder.vcNumber.setText(suspendedAccountData.getData().get(i).getSmartcardno() != null ? suspendedAccountData.getData().get(i).getSmartcardno().trim() : "---");
        holder.brand.setText(suspendedAccountData.getData().get(i).getBoxtypeLbl() != null ? suspendedAccountData.getData().get(i).getBoxtypeLbl().trim() : "---");
        holder.expDate.setText(CommonUtils.dateFormat(suspendedAccountData.getData().get(i).getDeactivationDate()) != null ? suspendedAccountData.getData().get(i).getDeactivationDate().trim() : "---");

        if (suspendedAccountData.getData().get(i).getStatusLbl().trim().equalsIgnoreCase("active")) {
            holder.status.setTextColor(context.getResources().getColor(R.color.colorGreen));
        } else {
            holder.status.setTextColor(context.getResources().getColor(R.color.colorRed));
        }

        holder.status.setText("SUSPENDED");

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (!accountIdList.contains(suspendedAccountData.getData().get(i).getId())) {
                        accountIdList.add(suspendedAccountData.getData().get(i).getId());
                    }
                } else {
                    int j = accountIdList.indexOf(suspendedAccountData.getData().get(i).getId());
                    accountIdList.remove(suspendedAccountData.getData().get(i).getId());
                }
                if(accountIdList.size()>0) {
                    DashboardListingActivity.bulkOperation(Params.LIST_SUSPENDED, accountIdList);
                }else{
                    DashboardListingActivity.bulk_button.setVisibility(View.GONE);
                }
                for (String s : accountIdList
                ) {
                    Log.d("???", "onCheckedChanged: " + s);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return suspendedAccountData.getData().size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView vcNumber, status, brand, expDate, name, address, mobile, customerID;
        CheckBox checkBox;
        LinearLayout container;


        public DataObjectHolder(View itemView) {
            super(itemView);
            vcNumber = itemView.findViewById(R.id.vcnumber_value);
            customerID = itemView.findViewById(R.id.customer_id_value);
            status = itemView.findViewById(R.id.status_value);
            brand = itemView.findViewById(R.id.brand_value);
            expDate = itemView.findViewById(R.id.exp_date_value);
            name = itemView.findViewById(R.id.name_tv);
            address = itemView.findViewById(R.id.address_tv);
            mobile = itemView.findViewById(R.id.mobile_value);
            checkBox = itemView.findViewById(R.id.checkbox_active_list);
            container = itemView.findViewById(R.id.container);

            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, CustomerDetailActivity.class);
                    intent.putExtra("customer_id",suspendedAccountData.getData().get(getAdapterPosition()).getCustomerId());
                    context.startActivity(intent);
                }
            });

        }
    }
}
