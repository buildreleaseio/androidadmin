package io.buildrelease.adminjpr.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import io.buildrelease.adminjpr.Fragments.ExpiredAccountSevenDaysFragment;
import io.buildrelease.adminjpr.Fragments.ExpiredBouquesSevenDaysFragment;

public class ViewPagerAdapterExpiredSevenDays extends FragmentPagerAdapter {


        public ViewPagerAdapterExpiredSevenDays(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0)
            {
                fragment = new ExpiredAccountSevenDaysFragment();
            }
            else if (position == 1)
            {
                fragment = new ExpiredBouquesSevenDaysFragment();
            }

            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0)
            {
                title = "Accounts";
            }
            else if (position == 1)
            {
                title = "Bouquets";
            }

            return title;
        }

}
