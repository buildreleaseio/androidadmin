package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.buildrelease.adminjpr.Model.WalletListData;
import io.buildrelease.adminjpr.R;

public class WalletListAdapter extends RecyclerView.Adapter<WalletListAdapter.DataObjectHolder> {


    View view;
    WalletListData walletListData;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;


    public WalletListAdapter(WalletListData walletListData, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.walletListData = walletListData;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.wallet_list_row_item, viewGroup, false);
        return new WalletListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {


        holder.name.setText(walletListData.getData().get(i).getName() != null ? walletListData.getData().get(i).getName() : "---");
        holder.balance.setText(walletListData.getData().get(i).getBalance() != null ? String.valueOf(walletListData.getData().get(i).getBalance()) : "---");
        holder.email.setText(walletListData.getData().get(i).getEmail() != null ? walletListData.getData().get(i).getEmail() : "---");
        holder.mobile.setText(walletListData.getData().get(i).getMobileNo() != null ? walletListData.getData().get(i).getMobileNo() : "---");

    }

    @Override
    public int getItemCount() {
        return walletListData.getData().size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView name, balance, email, mobile;
        LinearLayout container;


        public DataObjectHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name_value);
            balance = itemView.findViewById(R.id.balance_value);
            email = itemView.findViewById(R.id.email_value);
            mobile = itemView.findViewById(R.id.mobile_value);
            container = itemView.findViewById(R.id.container);

        }
    }
}
