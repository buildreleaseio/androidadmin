package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.buildrelease.adminjpr.Model.WalletTransactionListData;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;

public class WalletTransactionListAdapter extends RecyclerView.Adapter<WalletTransactionListAdapter.DataObjectHolder> {


    View view;
    WalletTransactionListData walletTransactionListData;
    FragmentManager fragmentManager;
    RecyclerView recyclerView;
    Context context;



    public WalletTransactionListAdapter(WalletTransactionListData walletTransactionListData, FragmentManager fragmentManager, RecyclerView recyclerView, Context context) {
        this.walletTransactionListData = walletTransactionListData;
        this.fragmentManager = fragmentManager;
        this.recyclerView = recyclerView;
        this.context = context;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.wallet_transaction_row_item,viewGroup,false);
        return new WalletTransactionListAdapter.DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataObjectHolder holder, int i) {

        String s= CommonUtils.dateFormat(walletTransactionListData.getData().get(i).getDecativationLbl());

                holder.vcNumber.setText(walletTransactionListData.getData().get(i).getSmartcardnoLbl()!=null?walletTransactionListData.getData().get(i).getSmartcardnoLbl():"---");
                holder.amount.setText(walletTransactionListData.getData().get(i).getAmount()!=null?walletTransactionListData.getData().get(i).getAmount():"---");
                holder.basePlan.setText(walletTransactionListData.getData().get(i).getBouqueLbl()!=null?walletTransactionListData.getData().get(i).getBouqueLbl():"---");
                if(walletTransactionListData.getData().get(i).getDecativationLbl().equals(null))
                {
                    holder.deactivationDate.setText("---");
                }
                else
                {
                    holder.deactivationDate.setText(CommonUtils.dateFormat(walletTransactionListData.getData().get(i).getDecativationLbl()));
                }
    }

    @Override
    public int getItemCount() {
        return walletTransactionListData.getData().size();
    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView vcNumber,amount,basePlan,deactivationDate;
        LinearLayout container;


        public DataObjectHolder(View itemView) {
            super(itemView);
            vcNumber     = itemView.findViewById(R.id.vcnumber_value);
            amount   = itemView.findViewById(R.id.amount_value);
            basePlan   = itemView.findViewById(R.id.base_plan_value);
            deactivationDate   = itemView.findViewById(R.id.deactivation_date_value);
            container   = itemView.findViewById(R.id.container);

        }
    }
}
