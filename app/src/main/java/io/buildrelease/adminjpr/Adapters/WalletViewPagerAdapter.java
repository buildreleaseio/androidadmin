package io.buildrelease.adminjpr.Adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import io.buildrelease.adminjpr.Fragments.WalletListFragment;
import io.buildrelease.adminjpr.Fragments.WalletTransactionFragment;

public class WalletViewPagerAdapter extends FragmentStatePagerAdapter {

    FragmentManager fm;
    Context context;
    ViewPager viewPager;


    public WalletViewPagerAdapter(FragmentManager fm, Context context, ViewPager viewPager) {
        super(fm);
        this.fm = fm;
        this.context = context;
        this.viewPager = viewPager;
    }

    @Override
    public Fragment getItem(int i) {
        if(i==1){
            return new WalletTransactionFragment();
        }else{
            return new WalletListFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if(position==1){
            return "TRANSACTIONS";
        }else {
            return "LIST";
        }
    }
}
