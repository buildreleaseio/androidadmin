package io.buildrelease.adminjpr.Fragments;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


import io.buildrelease.adminjpr.Activity.ActiveListActivity;
import io.buildrelease.adminjpr.Activity.BulkOperationsActivity;
import io.buildrelease.adminjpr.Activity.ClosedComplainActivity;
import io.buildrelease.adminjpr.Activity.CustomerDetailActivity;
import io.buildrelease.adminjpr.Activity.DashboardListingActivity;
import io.buildrelease.adminjpr.Activity.Expire7DaysActivity;
import io.buildrelease.adminjpr.Activity.InactiveListActivity;
import io.buildrelease.adminjpr.Activity.RecentlyExpiredListActivity;
import io.buildrelease.adminjpr.Activity.SuspendedListActivity;
import io.buildrelease.adminjpr.Activity.OpenComplainActivity;
import io.buildrelease.adminjpr.Activity.RechargeActivity;
import io.buildrelease.adminjpr.Model.ProfileData;
import io.buildrelease.adminjpr.Model.SearchCriteriaData;
import io.buildrelease.adminjpr.Model.SearchResponse;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.Network.ApiRequest;

import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.BULKBOUQUETREMOVAL;
import static io.buildrelease.adminjpr.Utility.Params.BULKBOUQUETSASSIGNMENT;
import static io.buildrelease.adminjpr.Utility.Params.BULKBOUQUETSREPLACEMENT;


public class DashBoardFragment extends Fragment {

    APIInterface apiInterface;
    String TAG = getClass().getSimpleName();
    ApiRequest apiRequest ;
    LinearLayout complaint_report_closed, complaint_report_open;
    LinearLayout subscriber_report_suspended, subscriber_report_active, subscriber_report_inactive, subscriber_report_ex7, subscriber_report_rEx,subscriber_report_bulk_removal,subscriber_report_bulk_replacement,subscriber_report_bulk_assignment;
    Button rechargeBTN, searchBTN;
    EditText search_et;
    Spinner spinner;
    TextView name_tv, balance_tv, mobile_tv, email_tv;
    FragmentActivity fragmentActivity;
    String spinnerSelected = "", spinnerKeySelected = "";


    public DashBoardFragment() {

    }


    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        if (getActivity() != null) {
            fragmentActivity = getActivity();
        }

    }

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        apiRequest = new ApiRequest(getActivity());
        apiInterface = APIClient.getClient().create(APIInterface.class);
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        complaint_report_closed = view.findViewById(R.id.complaint_report_closed);
        complaint_report_open = view.findViewById(R.id.complaint_report_open);
        subscriber_report_active = view.findViewById(R.id.subscriber_report_active);
        subscriber_report_inactive = view.findViewById(R.id.subscriber_report_inactive);
        subscriber_report_suspended = view.findViewById(R.id.subscriber_report_suspended);
        subscriber_report_ex7 = view.findViewById(R.id.subscriber_report_ex7);
        subscriber_report_rEx = view.findViewById(R.id.subscriber_report_rEx);

        //Bulk Operations Row
        subscriber_report_bulk_removal = view.findViewById(R.id.subscriber_report_bulk_removal);
        subscriber_report_bulk_replacement = view.findViewById(R.id.subscriber_report_bulk_replacement);
        subscriber_report_bulk_assignment = view.findViewById(R.id.subscriber_report_bulk_assignment);

        rechargeBTN = view.findViewById(R.id.rechange_btn);
        searchBTN = view.findViewById(R.id.search_btn);
        search_et = view.findViewById(R.id.search_et);
        spinner = view.findViewById(R.id.spinner_dashboard);

        name_tv = view.findViewById(R.id.name_tv);
        balance_tv = view.findViewById(R.id.balance_value_tv);
        mobile_tv = view.findViewById(R.id.mobile_tv);
        email_tv = view.findViewById(R.id.email_tv);
       // getProfile();

        rechargeBTN.setOnClickListener(rechargeBTNListeners);
        searchBTN.setOnClickListener(searchBTNListener);
//        for reports
//        subscriber_report_suspended.setOnClickListener(reportSuspendedListener);
        subscriber_report_suspended.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getActivity(), SuspendedListActivity.class));
            }
        });
        subscriber_report_active.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getActivity(),ActiveListActivity.class));
            }
        });
//        subscriber_report_inactive.setOnClickListener(reportInactiveListener);
        subscriber_report_inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getActivity(), InactiveListActivity.class));
            }
        });
        subscriber_report_ex7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), Expire7DaysActivity.class));
            }
        });
        subscriber_report_rEx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().startActivity(new Intent(getActivity(), RecentlyExpiredListActivity.class));
            }
        });
//      complaint
        complaint_report_closed.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               getActivity().startActivity(new Intent(getActivity(),ClosedComplainActivity.class));
           }
       }

        );
        complaint_report_open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().startActivity(new Intent(getActivity(),OpenComplainActivity.class));
            }
        });

        subscriber_report_bulk_removal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), BulkOperationsActivity.class);
                intent.putExtra("type",BULKBOUQUETREMOVAL);
                startActivity(intent);
            }
        });

        subscriber_report_bulk_replacement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), BulkOperationsActivity.class);
                intent.putExtra("type",BULKBOUQUETSREPLACEMENT);
                startActivity(intent);
            }
        });

        subscriber_report_bulk_assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), BulkOperationsActivity.class);
                intent.putExtra("type",BULKBOUQUETSASSIGNMENT);
                startActivity(intent);
            }
        });


        return view;

    }


    private void fillSpinner(final List<String> spinnerDataList, final List<String> spinnerKeyList) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Objects.requireNonNull(getActivity()),
                android.R.layout.simple_spinner_item, spinnerDataList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerSelected = spinnerDataList.get(position);
                spinnerKeySelected = spinnerKeyList.get(position);
                Log.d(TAG, "onItemSelected: " + spinnerSelected);
                Log.d(TAG, "onItemSelected: " + spinnerKeySelected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spinner.setSelection(0);
    }


    View.OnClickListener rechargeBTNListeners = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            moveToRecharge();
        }
    };

    public void moveToRecharge() {
        if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), RechargeActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getProfile();
    }


    public void getProfile() {
        Call<JsonObject> call = apiInterface.getProfile(UtilPref.getAuth(getActivity()));
        apiRequest.request(call, fetchDataListener2);

    }

    public void getSearchCriteriaData() {
        Call<JsonObject> call = apiInterface.getSearchCriteriaData(UtilPref.getAuth(getActivity()));
        apiRequest.request(call, fetchDataListener);

    }


    FetchDataListener fetchDataListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            List<String> spinnerDataList = new ArrayList<>();
            List<String> spinnerKeyList = new ArrayList<>();
//            setting default in list
            spinnerDataList.add(0, "Select criteria");
            spinnerKeyList.add(0, "default");
            Log.d(TAG, "onFetchComplete: " + data);
            SearchCriteriaData response = new Gson().fromJson(data, SearchCriteriaData.class);
            for (SearchCriteriaData.Datum criteria : response.getData()
            ) {
                spinnerDataList.add(criteria.getLabel());
                spinnerKeyList.add(criteria.getKey());
            }
            fillSpinner(spinnerDataList, spinnerKeyList);

        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(Objects.requireNonNull(getActivity()), msg, Params.TOASTY_ERROR);
            Log.d(TAG, "onFetchFailure: " + msg);

        }

        @Override
        public void onFetchStart() {
            RequestQueueService.showProgressDialog(getActivity());
        }
    };


    FetchDataListener fetchDataListener2 = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            ProfileData profile = new Gson().fromJson(data, ProfileData.class);
            Log.d(TAG, "onFetchComplete: " + profile.getData().getMobileNo());
            setDashboardData(profile);
            getSearchCriteriaData();
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            if (fragmentActivity != null)
                CommonUtils.showToasty(fragmentActivity, msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            if (fragmentActivity != null)
                RequestQueueService.showProgressDialog(fragmentActivity);
        }
    };

    private void setDashboardData(ProfileData dashboardData) {
        if (isAdded()) {
            name_tv.setText(dashboardData.getData().getName());
            email_tv.setText(dashboardData.getData().getEmail());
            balance_tv.setText(String.valueOf(dashboardData.getData().getBalance()));
            mobile_tv.setText(dashboardData.getData().getMobileNo());
        }
    }


    View.OnClickListener searchBTNListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (spinnerSelected.isEmpty() || spinnerSelected.equalsIgnoreCase("select criteria")) {
                if (getActivity() != null) {
                    CommonUtils.showToasty(getActivity(), "Select criteria", Params.TOASTY_WARN);
                }
            } else if (search_et.getText().toString().trim().isEmpty()) {
                search_et.setError("Cannot be empty");
            } else {
                //call the search api with one of the filter selected amongst a list of
            /*
                &filter[customer_id]=customer_id%-customer_id
                &filter[smartcardno]=smartcardno%-smartcardno
                &filter[stbno]=stbno%-stbno
                &filter[mobile_no]=9999999999%
             */
//                callSearchApi();
                moveToCustomerDetails();
            }
        }
    };

    private void moveToCustomerDetails() {
        Intent intent = new Intent(getActivity(), CustomerDetailActivity.class);
        String value = search_et.getText().toString().trim();
        intent.putExtra(spinnerKeySelected,value);
        startActivity(intent);
    }



//    bypassed for now
    private void callSearchApi() {
        String value = search_et.getText().toString().trim();
        if (spinnerKeySelected.equalsIgnoreCase("mobile_no")) {
            Call<JsonObject> call = apiInterface.getCustomerDetail(UtilPref.getAuth(getActivity()), "", "", "", value);
            apiRequest.request(call, callSearchApiListener);
        } else if (spinnerKeySelected.equalsIgnoreCase("customer_id")) {
            Call<JsonObject> call = apiInterface.getCustomerDetail(UtilPref.getAuth(getActivity()), value, "", "", "");
            apiRequest.request(call, callSearchApiListener);
        } else if (spinnerKeySelected.equalsIgnoreCase("smartcardno")) {
            Call<JsonObject> call = apiInterface.getCustomerDetail(UtilPref.getAuth(getActivity()), "", value, "", "");
            apiRequest.request(call, callSearchApiListener);
        } else if (spinnerKeySelected.equalsIgnoreCase("stbno")) {
            Call<JsonObject> call = apiInterface.getCustomerDetail(UtilPref.getAuth(getActivity()), "", "", value, "");
            apiRequest.request(call, callSearchApiListener);
        }

    }


    FetchDataListener callSearchApiListener = new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            SearchResponse searchResponse = new Gson().fromJson(data, SearchResponse.class);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            if (fragmentActivity != null)
                CommonUtils.showToasty(fragmentActivity, msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            if (fragmentActivity != null)
                RequestQueueService.showProgressDialog(fragmentActivity);
        }
    };


    View.OnClickListener reportActiveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getActivity() != null) {
                movetoDashboardListing(Params.LIST_ACTIVE);
            }
        }
    };

    View.OnClickListener reportSuspendedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            movetoDashboardListing(Params.LIST_SUSPENDED);
        }
    };

    View.OnClickListener reportInactiveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (getActivity() != null) {
                movetoDashboardListing(Params.LIST_INACTIVE);
            }
        }
    };


    View.OnClickListener reportrExListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            movetoDashboardListing(Params.LIST_EXPIREDTODAY);
        }
    };
    View.OnClickListener reportex7Listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            movetoDashboardListing(Params.LIST_EXPIRED7DAYS);
        }
    };


    View.OnClickListener complaintOpenListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            movetoDashboardListing(Params.LIST_OPEN_COMPLAINTS);
        }
    };
    View.OnClickListener complaintClosedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            movetoDashboardListing(Params.LIST_CLOSED_COMPLAINTS);
        }
    };


    private void movetoDashboardListing(String listType) {
        if(getActivity()!=null) {
            Intent intent = new Intent(getActivity(), DashboardListingActivity.class);
            intent.putExtra("listingType", listType);
            getActivity().startActivity(intent);
        }
    }











}
