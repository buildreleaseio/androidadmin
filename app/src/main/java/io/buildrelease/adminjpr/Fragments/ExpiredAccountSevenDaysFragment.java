package io.buildrelease.adminjpr.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Activity.RechargePeriodActivity;
import io.buildrelease.adminjpr.Adapters.Expire7DaysListAdapter;
import io.buildrelease.adminjpr.Model.BulkAccountRenewalPayload;
import io.buildrelease.adminjpr.Model.BulkBouqoutesRenewalPayload;
import io.buildrelease.adminjpr.Model.expird7daysbouquetswiseresponse;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import io.buildrelease.adminjpr.interfaces.SelectedIdListInterface;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class ExpiredAccountSevenDaysFragment extends Fragment implements SelectedIdListInterface {

    List<String> ids =null;
    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    int pageno =1 ,pageCount = 50;
    Expire7DaysListAdapter mAdapter ;
    boolean newData = true;
    LinearLayoutManager layoutManager;
    boolean isScrolled = false;
    List<expird7daysbouquetswiseresponse.Datum> datumArrayListExpire7DaysData =new ArrayList<>();
    String totalPageCount = "0";
    Button btn_renew;
    String bulkBouquetsIds = null;


    public ExpiredAccountSevenDaysFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if(getArguments()!=null) {
            bulkBouquetsIds = getArguments().getString("boqid", null);
        }
        View rootView = inflater.inflate(R.layout.fragment_expired_account_seven_days, container, false);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        connnectUI(rootView);
        getListOfSubsciber();

        return rootView;
    }

    public void getListOfSubsciber()
    {
        if(bulkBouquetsIds!=null) {
            getRecentlyExpiredListDataBouquetWise();
        }else
        {
            getRecentlyExpiredListData();
        }
    }

    public void getRecentlyExpiredListData() {
        ApiRequest apiRequest = new ApiRequest(getActivity());
        Call<JsonObject> call = apiInterface.get7DaysExpireAccountList(UtilPref.getAuth(getActivity()),pageno,pageCount);
        apiRequest.request(call, getRecentlyExpiredListDataListener);
    }


    public void getRecentlyExpiredListDataBouquetWise() {
        ApiRequest apiRequest = new ApiRequest(getActivity());
        Call<JsonObject> call = apiInterface.get7DaysExpireListBouqoutesWise(UtilPref.getAuth(getActivity()), bulkBouquetsIds,pageno,pageCount);
        apiRequest.request(call, getRecentlyExpiredListDataListener);
    }


    @Override
    public void onResume() {
        super.onResume();
        reload();
    }


    public void reload()
    {
        newData = true;
        pageno =1;

        mAdapter.clearSelectedList();
        getListOfSubsciber();

    }

    FetchDataPaginatedListener getRecentlyExpiredListDataListener =new FetchDataPaginatedListener() {

        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            expird7daysbouquetswiseresponse recentlyExpiredData = new Gson().fromJson(data, expird7daysbouquetswiseresponse.class);
            inflateExpire7daysAdapter(recentlyExpiredData);
            totalPageCount = pageCount;
        }

        @Override
        public void onFetchFailure(String msg) {

            RequestQueueService.cancelProgressDialog();
            if(getActivity()!=null)
            {
                CommonUtils.showToasty(getActivity(),msg, Params.TOASTY_ERROR);
            }

        }

        @Override
        public void onFetchStart() {
            if(getActivity()!=null) {
                RequestQueueService.showProgressDialog(getActivity());
            }
        }
    };

    public void inflateExpire7daysAdapter(expird7daysbouquetswiseresponse expire7DaysData){

        if(newData) {
            datumArrayListExpire7DaysData.clear();
            datumArrayListExpire7DaysData.addAll(expire7DaysData.getData());
            mAdapter.notifyDataSetChanged();
        }else
        {
            datumArrayListExpire7DaysData.addAll(expire7DaysData.getData());
            mAdapter.notifyDataSetChanged();
        }
        if(expire7DaysData.getData()!=null && expire7DaysData.getData().size()==0)
        {
            no_data_ll.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            btn_renew.setVisibility(View.GONE);
        }
        else
        {
            no_data_ll.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void connnectUI( View rootView )
    {

        btn_renew = rootView.findViewById(R.id.btn_renew);
        btn_renew.setOnClickListener(renewLisener);
        no_data_ll = rootView.findViewById(R.id.no_data_ll);
        recyclerView = rootView.findViewById(R.id.recyclerView_recentlyExpiredList);
        mAdapter = new Expire7DaysListAdapter(getActivity(),datumArrayListExpire7DaysData,this);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolled = true;
                    loadmoreData();
                }
            }
        });

    }
    public void loadmoreData() {
        int totalListItemCount = layoutManager.getItemCount();
        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();

        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
            isScrolled = false;
            if(totalPageCount!=null)
            {
                int total = Integer.parseInt(totalPageCount);
                Log.d(TAG, "loadmoreData: total"+total+" pageno "+pageno + " pagecount "+pageCount);
                if(total>pageno*pageCount) {
                    pageno += 1;
                    newData = false;
                    getRecentlyExpiredListData();
                }
            }
        }
    }

    @Override
    public void selectedIds(List<String> ids) {
        this.ids = ids;
        if(ids.size()>0)
        {
            btn_renew.setVisibility(View.VISIBLE);
        }else
        {
            btn_renew.setVisibility(View.GONE);
        }
    }

   View.OnClickListener renewLisener = new View.OnClickListener() {
       @Override
       public void onClick(View view) {
            showRemarkDialog();
       }
   };

    public void showRemarkDialog(){
        try {
            final MaterialDialog.Builder builder = new MaterialDialog.Builder(getActivity());
            builder.autoDismiss(true);
            builder.cancelable(true);

            builder.title("Remark")
                    .inputRange(0, 255)
                    .positiveColor(getResources().getColor(R.color.colorPrimary))
                    .negativeColor(getResources().getColor(R.color.colorPrimary))
                    .input("Type your comment here", "", false, new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                            performBulkOperation(input.toString());
                        }
                    })
                    .positiveText("Submit")
//                    .onPositive(new MaterialDialog.SingleButtonCallback() {
//                        @Override
//                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
//
//                        }
//                    })
                    .negativeText("Cancel")
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            builder.cancelable(true);
                        }
                    })
                    .canceledOnTouchOutside(true)
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void performBulkOperation(String s) {
        try {



            if(getActivity()!=null) {
                Intent intent = new Intent(getActivity(), RechargePeriodActivity.class);
                if(bulkBouquetsIds!=null)
                {
                    List<String> bouqids = new ArrayList<>();
                    bouqids.clear();
                    bouqids.add(bulkBouquetsIds);
                    BulkBouqoutesRenewalPayload bulkBouqoutesRenewalPayload = new BulkBouqoutesRenewalPayload();
                    bulkBouqoutesRenewalPayload.setAccountIds(ids);
                    bulkBouqoutesRenewalPayload.setRemark(s);
                    bulkBouqoutesRenewalPayload.setBouqueIds(bouqids);
                    String data = new Gson().toJson(bulkBouqoutesRenewalPayload);
                    intent.putExtra("type", Params.BULKBOUQUETSRENEWAL);
                    intent.putExtra("bulkbouquets", data);
                }else
                {

                    BulkAccountRenewalPayload bulkAccountRenewalPayload = new BulkAccountRenewalPayload();
                    bulkAccountRenewalPayload.setAccountIds(ids);
                    bulkAccountRenewalPayload.setRemark(s);
                    bulkAccountRenewalPayload.setRenewOnlyBase(0);
                    String data = new Gson().toJson(bulkAccountRenewalPayload);
                    intent.putExtra("type", Params.BULKACCOUNTRENEWAL);
                    intent.putExtra("bulkaccount", data);
                }


                getActivity().startActivity(intent);
            }


        }catch (Exception e){
            e.printStackTrace();
        }
    }




}
