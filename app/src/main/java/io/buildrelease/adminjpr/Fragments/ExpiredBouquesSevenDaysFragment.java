package io.buildrelease.adminjpr.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import io.buildrelease.adminjpr.Adapters.Expire7DaysBouquetsListAdapter;
import io.buildrelease.adminjpr.Model.ExpiredSevenDaysBouques;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataPaginatedListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;

public class ExpiredBouquesSevenDaysFragment extends Fragment {

    LinearLayout no_data_ll;
    RecyclerView recyclerView;
    APIInterface apiInterface;
    int pageno =1 ,pageCount = 50;
    Expire7DaysBouquetsListAdapter mAdapter ;
    boolean newData = true;
    LinearLayoutManager layoutManager;
    boolean isScrolled = false;
    List<ExpiredSevenDaysBouques.Datum> datumArrayListExpire7DaysData =new ArrayList<>();
    String totalPageCount = "0";


    public ExpiredBouquesSevenDaysFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_expired_account_seven_days, container, false);
        apiInterface = APIClient.getClient().create(APIInterface.class);
        connnectUI(rootView);
        getRecentlyExpiredListData();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getActivity()!=null)
        {
            getRecentlyExpiredListData();
        }
    }

    public void getRecentlyExpiredListData() {
        ApiRequest apiRequest = new ApiRequest(getActivity());
        Call<JsonObject> call = apiInterface.getExpiredSevenDaysBouquets(UtilPref.getAuth(getActivity()));
        apiRequest.request(call, getRecentlyExpiredListDataListener);
    }


    FetchDataPaginatedListener getRecentlyExpiredListDataListener =new FetchDataPaginatedListener() {

        @Override
        public void onFetchComplete(String data, String pageCount) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            ExpiredSevenDaysBouques recentlyExpiredData = new Gson().fromJson(data, ExpiredSevenDaysBouques.class);
            inflateExpire7daysAdapter(recentlyExpiredData);
        }

        @Override
        public void onFetchFailure(String msg) {

            RequestQueueService.cancelProgressDialog();
            if(getActivity()!=null)
            {
                CommonUtils.showToasty(getActivity(),msg, Params.TOASTY_ERROR);
            }

        }

        @Override
        public void onFetchStart() {
            if(getActivity()!=null) {
                RequestQueueService.showProgressDialog(getActivity());
            }
        }
    };

    public void inflateExpire7daysAdapter(ExpiredSevenDaysBouques expire7DaysData){

        if(newData) {
            datumArrayListExpire7DaysData.clear();
            datumArrayListExpire7DaysData.addAll(expire7DaysData.getData());
            mAdapter.notifyDataSetChanged();
        }else
        {
            datumArrayListExpire7DaysData.addAll(expire7DaysData.getData());
            mAdapter.notifyDataSetChanged();
        }
        if(expire7DaysData.getData()!=null && expire7DaysData.getData().size()==0)
        {
            no_data_ll.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        else
        {
            no_data_ll.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    public void connnectUI( View rootView )
    {
        no_data_ll = rootView.findViewById(R.id.no_data_ll);
        recyclerView = rootView.findViewById(R.id.recyclerView_recentlyExpiredList);
        mAdapter = new Expire7DaysBouquetsListAdapter(getActivity(),datumArrayListExpire7DaysData);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//
//                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//                    isScrolled = true;
//                    loadmoreData();
//                }
//            }
//        });

    }
//    public void loadmoreData() {
//        int totalListItemCount = layoutManager.getItemCount();
//        int totalChildCurrentlyVisibleCount = layoutManager.getChildCount();
//        int totalChildItemScrolledCount = layoutManager.findFirstVisibleItemPosition();
//
//        if (isScrolled && totalChildCurrentlyVisibleCount + totalChildItemScrolledCount >= totalListItemCount) {
//            isScrolled = false;
//            if(totalPageCount!=null)
//            {
//                int total = Integer.parseInt(totalPageCount);
//                Log.d(TAG, "loadmoreData: total"+total+" pageno "+pageno + " pagecount "+pageCount);
//                if(total>pageno*pageCount) {
//                    pageno += 1;
//                    newData = false;
//                    getRecentlyExpiredListData();
//                }
//            }
//        }
//    }

}
