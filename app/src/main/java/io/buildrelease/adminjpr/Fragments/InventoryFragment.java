package io.buildrelease.adminjpr.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.buildrelease.adminjpr.Adapters.InventoryAdapter;
import io.buildrelease.adminjpr.Model.InventoryData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class InventoryFragment extends Fragment {


    RecyclerView recyclerView;
    LinearLayout no_data_ll;
    APIInterface apiInterface;
    FragmentActivity fragmentActivity;



    public InventoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        if(getActivity()!=null) {
            fragmentActivity = getActivity();
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        apiInterface = APIClient.getClient().create(APIInterface.class);
        View view = inflater.inflate(R.layout.fragment_inventory, container, false);

        recyclerView = view.findViewById(R.id.recyclerView_inventory);
        no_data_ll = view.findViewById(R.id.no_data_ll);


        getInventoryData();



        return view;
    }



    public void getInventoryData() {
        ApiRequest apiRequest = new ApiRequest(getActivity());
        Call<JsonObject> call = apiInterface.getInvertoryData(UtilPref.getAuth(getActivity()));
        apiRequest.request(call, getInventoryDataListener);

    }


    FetchDataListener getInventoryDataListener =new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            InventoryData inventoryData = new Gson().fromJson(data, InventoryData.class);
            inflateInventoryAdapter(inventoryData);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            CommonUtils.showToasty(fragmentActivity,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            if(getActivity()!=null)
                RequestQueueService.showProgressDialog(fragmentActivity);
        }
    };



    public void inflateInventoryAdapter(InventoryData inventoryData){
        InventoryAdapter mAdapter =
                new InventoryAdapter(
//                        TODO CHECK FOR VALID DECIMAL
                        inventoryData,
                        getChildFragmentManager(),
                        recyclerView,
                        getActivity());
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();
    }

}
