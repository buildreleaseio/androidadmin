package io.buildrelease.adminjpr.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.buildrelease.adminjpr.Adapters.WalletViewPagerAdapter;
import io.buildrelease.adminjpr.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class WalletFragment extends Fragment {

    ViewPager viewPager;
    TabLayout tablayout;
    public WalletFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_wallet, container, false);
        viewPager   = view.findViewById(R.id.pager);
        tablayout   = view.findViewById(R.id.tablayout);

        inflateViewPager();
        return view;
    }

    private void inflateViewPager(){
        WalletViewPagerAdapter myPagerAdapter = new WalletViewPagerAdapter(getChildFragmentManager(),getActivity(),viewPager);
        viewPager.setAdapter(myPagerAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(0);
//        viewPager.setCurrentItem(1);//for favourite
        tablayout.setupWithViewPager(viewPager);

    }

}
