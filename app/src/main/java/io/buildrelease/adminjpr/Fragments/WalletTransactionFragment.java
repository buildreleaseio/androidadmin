package io.buildrelease.adminjpr.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import io.buildrelease.adminjpr.Adapters.WalletTransactionListAdapter;
import io.buildrelease.adminjpr.Model.WalletTransactionListData;
import io.buildrelease.adminjpr.Network.APIClient;
import io.buildrelease.adminjpr.Network.APIInterface;
import io.buildrelease.adminjpr.Network.ApiRequest;
import io.buildrelease.adminjpr.Network.FetchDataListener;
import io.buildrelease.adminjpr.R;
import io.buildrelease.adminjpr.Utility.CommonUtils;
import io.buildrelease.adminjpr.Utility.Params;
import io.buildrelease.adminjpr.Utility.RequestQueueService;
import io.buildrelease.adminjpr.Utility.UtilPref;
import retrofit2.Call;

import static io.buildrelease.adminjpr.Utility.Params.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class WalletTransactionFragment extends Fragment {


    RecyclerView recyclerView;
    LinearLayout no_data_ll;
    APIInterface apiInterface;
    FragmentActivity fragmentActivity;

    public WalletTransactionFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(getActivity()!=null)
        fragmentActivity = getActivity();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        apiInterface = APIClient.getClient().create(APIInterface.class);
        View view = inflater.inflate(R.layout.fragment_wallet_transaction, container, false);
        recyclerView = view.findViewById(R.id.recyclerView_inventory);
        no_data_ll = view.findViewById(R.id.no_data_ll);

        getWalletTransactionListData();

        return view;
    }



    public void getWalletTransactionListData() {
        ApiRequest apiRequest = new ApiRequest(getActivity());
        Call<JsonObject> call = apiInterface.getWalletTransactionListData(UtilPref.getAuth(getActivity()));
        apiRequest.request(call, getWalletTransactionListener);

    }


    FetchDataListener getWalletTransactionListener =new FetchDataListener() {
        @Override
        public void onFetchComplete(String data) {
            RequestQueueService.cancelProgressDialog();
            Log.d(TAG, "onFetchComplete: " + data);
            WalletTransactionListData walletTransactionListData = new Gson().fromJson(data, WalletTransactionListData.class);
            inflateWalletTransactionListAdapter(walletTransactionListData);
        }

        @Override
        public void onFetchFailure(String msg) {
            RequestQueueService.cancelProgressDialog();
            if(fragmentActivity!=null)
            CommonUtils.showToasty(fragmentActivity,msg, Params.TOASTY_ERROR);
        }

        @Override
        public void onFetchStart() {
            if(getActivity()!=null)
                RequestQueueService.showProgressDialog(fragmentActivity);
        }
    };



    public void inflateWalletTransactionListAdapter(WalletTransactionListData walletTransactionListData){
        WalletTransactionListAdapter mAdapter =
                new WalletTransactionListAdapter(
//                        TODO CHECK FOR VALID DECIMAL
                        walletTransactionListData,
                        getChildFragmentManager(),
                        recyclerView,
                        getActivity());
        recyclerView.hasFixedSize();
        recyclerView.setAdapter(mAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setNestedScrollingEnabled(true);
        mAdapter.notifyDataSetChanged();
    }

}
