package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class AddonResponse {


    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("rate")
        @Expose
        private List<Rate> rate = null;
        @SerializedName("boxtype_lbl")
        @Expose
        private String boxtypeLbl;
        @SerializedName("type_lbl")
        @Expose
        private String typeLbl;

        private boolean selected;

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<Rate> getRate() {
            return rate;
        }

        public void setRate(List<Rate> rate) {
            this.rate = rate;
        }

        public String getBoxtypeLbl() {
            return boxtypeLbl;
        }

        public void setBoxtypeLbl(String boxtypeLbl) {
            this.boxtypeLbl = boxtypeLbl;
        }

        public String getTypeLbl() {
            return typeLbl;
        }

        public void setTypeLbl(String typeLbl) {
            this.typeLbl = typeLbl;
        }

    }


    public class Rate {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("months")
        @Expose
        private String months;
        @SerializedName("days")
        @Expose
        private String days;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("tax_amount")
        @Expose
        private String taxAmount;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("rental")
        @Expose
        private String rental;
        @SerializedName("name")
        @Expose
        private String name;

        public Rate() {
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMonths() {
            return months;
        }

        public void setMonths(String months) {
            this.months = months;
        }

        public String getDays() {
            return days;
        }

        public void setDays(String days) {
            this.days = days;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getTaxAmount() {
            return taxAmount;
        }

        public void setTaxAmount(String taxAmount) {
            this.taxAmount = taxAmount;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getRental() {
            return rental;
        }

        public void setRental(String rental) {
            this.rental = rental;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
