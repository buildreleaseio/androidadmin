package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BulkAccountRenewalPayload {

    @SerializedName("account_ids")
    @Expose
    private List<String> accountIds = null;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("renew_only_base")
    @Expose
    private Integer renewOnlyBase;
    @SerializedName("rperiod_id")
    @Expose
    private String rperiodId;


    public List<String> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<String> accountIds) {
        this.accountIds = accountIds;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getRenewOnlyBase() {
        return renewOnlyBase;
    }

    public void setRenewOnlyBase(Integer renewOnlyBase) {
        this.renewOnlyBase = renewOnlyBase;
    }

    public String getRperiodId() {
        return rperiodId;
    }

    public void setRperiodId(String rperiodId) {
        this.rperiodId = rperiodId;
    }
}
