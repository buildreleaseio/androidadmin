package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BulkBouqoutesRenewalPayload {

    @SerializedName("account_ids")
    @Expose
    private List<String> accountIds = null;
    @SerializedName("bouque_ids")
    @Expose
    private List<String> bouqueIds = null;
    @SerializedName("Remark")
    @Expose
    private String remark;
    @SerializedName("rperiod_id")
    @Expose
    private String rperiodId;

    public List<String> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<String> accountIds) {
        this.accountIds = accountIds;
    }

    public List<String> getBouqueIds() {
        return bouqueIds;
    }

    public void setBouqueIds(List<String> bouqueIds) {
        this.bouqueIds = bouqueIds;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRperiodId() {
        return rperiodId;
    }

    public void setRperiodId(String rperiodId) {
        this.rperiodId = rperiodId;
    }
}
