package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BulkRemoveBouqetPayload {

    public BulkRemoveBouqetPayload(List<Integer> accountId, String remark,List<Integer> bouqueIds) {
        this.accountId = accountId;
        this.remark = remark;
        this.bouqueIds = bouqueIds;
    }

    @SerializedName("account_ids")
    @Expose
    private List<Integer> accountId;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("bouque_ids")
    @Expose
    private List<Integer> bouqueIds;

    public List<Integer> getAccountId() {
        return accountId;
    }

    public void setAccountId(List<Integer> accountId) {
        this.accountId = accountId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<Integer> getBouqueIds() {
        return bouqueIds;
    }

    public void setBouqueIds(List<Integer> bouqueIds) {
        this.bouqueIds = bouqueIds;
    }

}
