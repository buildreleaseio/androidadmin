package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class BulkSubcriberAssignmentResponse {

    @SerializedName("success")
    private boolean success;
    @SerializedName("status")
    private int status;
    @SerializedName("data")
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        @SerializedName("id")
        private String id;
        @SerializedName("subscriber_id")
        private String subscriberId;
        @SerializedName("customer_id")
        private String customerId;
        @SerializedName("operator_id")
        private int operatorId;
        @SerializedName("location_id")
        private int locationId;
        @SerializedName("sublocation_id")
        private int sublocationId;
        @SerializedName("pairing_id")
        private int pairingId;
        @SerializedName("daysLeft")
        private String daysLeft;
        @SerializedName("isLocked")
        private boolean isLocked;
        @SerializedName("isRefreshAble")
        private boolean isRefreshAble;
        @SerializedName("smartcardno")
        private String smartcardno;
        @SerializedName("stbno")
        private String stbno;
        @SerializedName("lockTimeLeft")
        private int lockTimeLeft;
        @SerializedName("stb_id")
        private int stbId;
        @SerializedName("sc_id")
        private int scId;
        @SerializedName("cas_id")
        private int casId;
        @SerializedName("isHD")
        private int isHD;
        @SerializedName("is_embeded")
        private Object isEmbeded;
        @SerializedName("stbbrand_id")
        private int stbbrandId;
        @SerializedName("scheme_id")
        private int schemeId;
        @SerializedName("bouque_id")
        private int bouqueId;
        @SerializedName("description")
        private Object description;
        @SerializedName("activation_date")
        private String activationDate;
        @SerializedName("deactivation_date")
        private String deactivationDate;
        @SerializedName("outstanding")
        private OutstandingBean outstanding;
        @SerializedName("status")
        private int status;
        @SerializedName("remark")
        private String remark;
        @SerializedName("mobile_no")
        private String mobileNo;
        @SerializedName("address")
        private String address;
        @SerializedName("operator_name_lbl")
        private String operatorNameLbl;
        @SerializedName("operator_code_lbl")
        private String operatorCodeLbl;
        @SerializedName("cas_status")
        private int casStatus;
        @SerializedName("created_at")
        private String createdAt;
        @SerializedName("updated_at")
        private String updatedAt;
        @SerializedName("created_by")
        private int createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("boxtype_lbl")
        private String boxtypeLbl;
        @SerializedName("status_lbl")
        private String statusLbl;
        @SerializedName("created_by_lbl")
        private String createdByLbl;
        @SerializedName("operator_lbl")
        private String operatorLbl;
        @SerializedName("location_lbl")
        private String locationLbl;
        @SerializedName("sublocation_lbl")
        private String sublocationLbl;
        @SerializedName("cas_lbl")
        private String casLbl;
        @SerializedName("brand_lbl")
        private String brandLbl;
        @SerializedName("customer_name")
        private String customerName;
        @SerializedName("bouqueD")
        private List<BouqueDBean> bouqueD;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSubscriberId() {
            return subscriberId;
        }

        public void setSubscriberId(String subscriberId) {
            this.subscriberId = subscriberId;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public int getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(int operatorId) {
            this.operatorId = operatorId;
        }

        public int getLocationId() {
            return locationId;
        }

        public void setLocationId(int locationId) {
            this.locationId = locationId;
        }

        public int getSublocationId() {
            return sublocationId;
        }

        public void setSublocationId(int sublocationId) {
            this.sublocationId = sublocationId;
        }

        public int getPairingId() {
            return pairingId;
        }

        public void setPairingId(int pairingId) {
            this.pairingId = pairingId;
        }

        public String getDaysLeft() {
            return daysLeft;
        }

        public void setDaysLeft(String daysLeft) {
            this.daysLeft = daysLeft;
        }

        public boolean isIsLocked() {
            return isLocked;
        }

        public void setIsLocked(boolean isLocked) {
            this.isLocked = isLocked;
        }

        public boolean isIsRefreshAble() {
            return isRefreshAble;
        }

        public void setIsRefreshAble(boolean isRefreshAble) {
            this.isRefreshAble = isRefreshAble;
        }

        public String getSmartcardno() {
            return smartcardno;
        }

        public void setSmartcardno(String smartcardno) {
            this.smartcardno = smartcardno;
        }

        public String getStbno() {
            return stbno;
        }

        public void setStbno(String stbno) {
            this.stbno = stbno;
        }

        public int getLockTimeLeft() {
            return lockTimeLeft;
        }

        public void setLockTimeLeft(int lockTimeLeft) {
            this.lockTimeLeft = lockTimeLeft;
        }

        public int getStbId() {
            return stbId;
        }

        public void setStbId(int stbId) {
            this.stbId = stbId;
        }

        public int getScId() {
            return scId;
        }

        public void setScId(int scId) {
            this.scId = scId;
        }

        public int getCasId() {
            return casId;
        }

        public void setCasId(int casId) {
            this.casId = casId;
        }

        public int getIsHD() {
            return isHD;
        }

        public void setIsHD(int isHD) {
            this.isHD = isHD;
        }

        public Object getIsEmbeded() {
            return isEmbeded;
        }

        public void setIsEmbeded(Object isEmbeded) {
            this.isEmbeded = isEmbeded;
        }

        public int getStbbrandId() {
            return stbbrandId;
        }

        public void setStbbrandId(int stbbrandId) {
            this.stbbrandId = stbbrandId;
        }

        public int getSchemeId() {
            return schemeId;
        }

        public void setSchemeId(int schemeId) {
            this.schemeId = schemeId;
        }

        public int getBouqueId() {
            return bouqueId;
        }

        public void setBouqueId(int bouqueId) {
            this.bouqueId = bouqueId;
        }

        public Object getDescription() {
            return description;
        }

        public void setDescription(Object description) {
            this.description = description;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public OutstandingBean getOutstanding() {
            return outstanding;
        }

        public void setOutstanding(OutstandingBean outstanding) {
            this.outstanding = outstanding;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getOperatorNameLbl() {
            return operatorNameLbl;
        }

        public void setOperatorNameLbl(String operatorNameLbl) {
            this.operatorNameLbl = operatorNameLbl;
        }

        public String getOperatorCodeLbl() {
            return operatorCodeLbl;
        }

        public void setOperatorCodeLbl(String operatorCodeLbl) {
            this.operatorCodeLbl = operatorCodeLbl;
        }

        public int getCasStatus() {
            return casStatus;
        }

        public void setCasStatus(int casStatus) {
            this.casStatus = casStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getBoxtypeLbl() {
            return boxtypeLbl;
        }

        public void setBoxtypeLbl(String boxtypeLbl) {
            this.boxtypeLbl = boxtypeLbl;
        }

        public String getStatusLbl() {
            return statusLbl;
        }

        public void setStatusLbl(String statusLbl) {
            this.statusLbl = statusLbl;
        }

        public String getCreatedByLbl() {
            return createdByLbl;
        }

        public void setCreatedByLbl(String createdByLbl) {
            this.createdByLbl = createdByLbl;
        }

        public String getOperatorLbl() {
            return operatorLbl;
        }

        public void setOperatorLbl(String operatorLbl) {
            this.operatorLbl = operatorLbl;
        }

        public String getLocationLbl() {
            return locationLbl;
        }

        public void setLocationLbl(String locationLbl) {
            this.locationLbl = locationLbl;
        }

        public String getSublocationLbl() {
            return sublocationLbl;
        }

        public void setSublocationLbl(String sublocationLbl) {
            this.sublocationLbl = sublocationLbl;
        }

        public String getCasLbl() {
            return casLbl;
        }

        public void setCasLbl(String casLbl) {
            this.casLbl = casLbl;
        }

        public String getBrandLbl() {
            return brandLbl;
        }

        public void setBrandLbl(String brandLbl) {
            this.brandLbl = brandLbl;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

        public List<BouqueDBean> getBouqueD() {
            return bouqueD;
        }

        public void setBouqueD(List<BouqueDBean> bouqueD) {
            this.bouqueD = bouqueD;
        }

        public static class OutstandingBean implements Serializable {
            @SerializedName("amount")
            private String amount;
            @SerializedName("remark")
            private String remark;

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getRemark() {
                return remark;
            }

            public void setRemark(String remark) {
                this.remark = remark;
            }
        }

        public static class BouqueDBean implements Serializable {
            @SerializedName("bouque_id")
            private int bouqueId;
            @SerializedName("account_id")
            private String accountId;
            @SerializedName("bouque_type")
            private int bouqueType;
            @SerializedName("activation_date")
            private String activationDate;
            @SerializedName("deactivation_date")
            private String deactivationDate;
            @SerializedName("amount")
            private String amount;
            @SerializedName("tax")
            private String tax;
            @SerializedName("left")
            private String left;
            @SerializedName("bouque_name")
            private String bouqueName;
            @SerializedName("refundAble")
            private RefundAbleBean refundAble;

            public int getBouqueId() {
                return bouqueId;
            }

            public void setBouqueId(int bouqueId) {
                this.bouqueId = bouqueId;
            }

            public String getAccountId() {
                return accountId;
            }

            public void setAccountId(String accountId) {
                this.accountId = accountId;
            }

            public int getBouqueType() {
                return bouqueType;
            }

            public void setBouqueType(int bouqueType) {
                this.bouqueType = bouqueType;
            }

            public String getActivationDate() {
                return activationDate;
            }

            public void setActivationDate(String activationDate) {
                this.activationDate = activationDate;
            }

            public String getDeactivationDate() {
                return deactivationDate;
            }

            public void setDeactivationDate(String deactivationDate) {
                this.deactivationDate = deactivationDate;
            }

            public String getAmount() {
                return amount;
            }

            public void setAmount(String amount) {
                this.amount = amount;
            }

            public String getTax() {
                return tax;
            }

            public void setTax(String tax) {
                this.tax = tax;
            }

            public String getLeft() {
                return left;
            }

            public void setLeft(String left) {
                this.left = left;
            }

            public String getBouqueName() {
                return bouqueName;
            }

            public void setBouqueName(String bouqueName) {
                this.bouqueName = bouqueName;
            }

            public RefundAbleBean getRefundAble() {
                return refundAble;
            }

            public void setRefundAble(RefundAbleBean refundAble) {
                this.refundAble = refundAble;
            }

            public static class RefundAbleBean implements Serializable {
                @SerializedName("amt")
                private double amt;
                @SerializedName("tax")
                private double tax;

                public double getAmt() {
                    return amt;
                }

                public void setAmt(double amt) {
                    this.amt = amt;
                }

                public double getTax() {
                    return tax;
                }

                public void setTax(double tax) {
                    this.tax = tax;
                }
            }
        }
    }
}
