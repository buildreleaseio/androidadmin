package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerDetail {


    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }




    public class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("status_lbl")
        @Expose
        private String statusLbl;
        @SerializedName("connection_lbl")
        @Expose
        private Integer connectionLbl;
        @SerializedName("account_list")
        @Expose
        private List<AccountList> accountList = null;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getStatusLbl() {
            return statusLbl;
        }

        public void setStatusLbl(String statusLbl) {
            this.statusLbl = statusLbl;
        }

        public Integer getConnectionLbl() {
            return connectionLbl;
        }

        public void setConnectionLbl(Integer connectionLbl) {
            this.connectionLbl = connectionLbl;
        }

        public List<AccountList> getAccountList() {
            return accountList;
        }

        public void setAccountList(List<AccountList> accountList) {
            this.accountList = accountList;
        }

    }

    public class AccountList {

        @SerializedName("account_id")
        @Expose
        private String accountId;
        @SerializedName("smartcardno")
        @Expose
        private String smartcardno;
        @SerializedName("stbno")
        @Expose
        private String stbno;
        @SerializedName("activation_date")
        @Expose
        private String activationDate;
        @SerializedName("deactivation_date")
        @Expose
        private String deactivationDate;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("boxtype_lbl")
        @Expose
        private String boxtypeLbl;
        @SerializedName("status_lbl")
        @Expose
        private String statusLbl;
        @SerializedName("stbbrand_id")
        @Expose
        private Integer stbbrandId;

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public String getSmartcardno() {
            return smartcardno;
        }

        public void setSmartcardno(String smartcardno) {
            this.smartcardno = smartcardno;
        }

        public String getStbno() {
            return stbno;
        }

        public void setStbno(String stbno) {
            this.stbno = stbno;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getBoxtypeLbl() {
            return boxtypeLbl;
        }

        public void setBoxtypeLbl(String boxtypeLbl) {
            this.boxtypeLbl = boxtypeLbl;
        }

        public String getStatusLbl() {
            return statusLbl;
        }

        public void setStatusLbl(String statusLbl) {
            this.statusLbl = statusLbl;
        }

        public Integer getStbbrandId() {
            return stbbrandId;
        }

        public void setStbbrandId(Integer stbbrandId) {
            this.stbbrandId = stbbrandId;
        }

    }
}
