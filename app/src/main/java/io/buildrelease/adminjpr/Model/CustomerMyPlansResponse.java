package io.buildrelease.adminjpr.Model;

import java.io.Serializable;
import java.util.List;

public class CustomerMyPlansResponse {

    /**
     * success : true
     * status : 200
     * data : [{"id":"1759138","bouque_id":8,"bouque_type":1,"name":"HD PREMIUM PACKAGE","account_id":"168380","subscriber_id":"151019","activation_date":"2019-03-12","deactivation_date":"2019-04-10","left":"1 day","noofmonths":"1 Month","refundAble":{"amt":11.6013,"tax":2.0883},"status":1,"cas_status":1,"amount":"348.04","tax":"62.65","refund_amount":null,"refund_tax":null,"refund_date":null,"remark":"[2019-03-12 18:50:47][Bouquet HD PREMIUM PACKAGE Renewed for Account (JPRC0151032/1000100083165180) from 2019-03-12 to 2019-04-11 for a period of 29 days and charged 410.69][Base Bouque Changed from DD PACCHIS(429) to HD PREMIUM PACKAGE(8)][By jitin]\n","created_at":"2019-03-12 18:50:47","updated_at":"2019-03-12 18:50:47","created_by":3,"updated_by":3,"status_lbl":"Active ","created_by_lbl":"Jitin","boxtype_lbl":"HD","type_lbl":"Base"},{"id":"1871344","bouque_id":48,"bouque_type":3,"name":"CNN","account_id":"168380","subscriber_id":"151019","activation_date":"2019-03-18","deactivation_date":"2019-04-10","left":"1 day","noofmonths":"24 days","refundAble":{"amt":0.02,"tax":0.0038},"status":1,"cas_status":1,"amount":"0.48","tax":"0.09","refund_amount":null,"refund_tax":null,"refund_date":null,"remark":"[2019-03-18 19:53:05][Bouquet CNN(48) Added to Account (JPRC0151032/1000100083165180)  from 2019-03-18 to 2019-04-11  and charged 0.57][By jitin]\n","created_at":"2019-03-18 19:53:05","updated_at":"2019-03-18 19:53:05","created_by":3,"updated_by":3,"status_lbl":"Active ","created_by_lbl":"Jitin","boxtype_lbl":"SD","type_lbl":"Alacarte"},{"id":"1871345","bouque_id":54,"bouque_type":3,"name":"BBC World","account_id":"168380","subscriber_id":"151019","activation_date":"2019-03-18","deactivation_date":"2019-04-10","left":"1 day","noofmonths":"24 days","refundAble":{"amt":0.03,"tax":0.0054},"status":1,"cas_status":1,"amount":"0.72","tax":"0.13","refund_amount":null,"refund_tax":null,"refund_date":null,"remark":"[2019-03-18 19:53:05][Bouquet BBC World(54) Added to Account (JPRC0151032/1000100083165180)  from 2019-03-18 to 2019-04-11  and charged 0.85][By jitin]\n","created_at":"2019-03-18 19:53:05","updated_at":"2019-03-18 19:53:05","created_by":3,"updated_by":3,"status_lbl":"Active ","created_by_lbl":"Jitin","boxtype_lbl":"SD","type_lbl":"Alacarte"},{"id":"2144296","bouque_id":469,"bouque_type":2,"name":"ZEE PRIME PACK ENGLISH HD","account_id":"168380","subscriber_id":"151019","activation_date":"2019-04-03","deactivation_date":"2019-04-10","left":"1 day","noofmonths":"8 days","refundAble":{"amt":0.67,"tax":0.12},"status":1,"cas_status":1,"amount":"5.36","tax":"0.96","refund_amount":null,"refund_tax":null,"refund_date":null,"remark":"[2019-04-03 16:27:03][Bouquet ZEE PRIME PACK ENGLISH HD(469) Added to Account (JPRC0151032/1000100083165180)  from 2019-04-03 to 2019-04-11  and charged 6.32][CHANGE_BASE_PLAN][By testinglmo]\n","created_at":"2019-04-03 16:27:03","updated_at":"2019-04-03 16:27:03","created_by":677,"updated_by":677,"status_lbl":"Active ","created_by_lbl":"TESTING LMO","boxtype_lbl":"HD","type_lbl":"Addon"}]
     */

    private boolean success;
    private int status;
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Serializable {
        /**
         * id : 1759138
         * bouque_id : 8
         * bouque_type : 1
         * name : HD PREMIUM PACKAGE
         * account_id : 168380
         * subscriber_id : 151019
         * activation_date : 2019-03-12
         * deactivation_date : 2019-04-10
         * left : 1 day
         * noofmonths : 1 Month
         * refundAble : {"amt":11.6013,"tax":2.0883}
         * status : 1
         * cas_status : 1
         * amount : 348.04
         * tax : 62.65
         * refund_amount : null
         * refund_tax : null
         * refund_date : null
         * remark : [2019-03-12 18:50:47][Bouquet HD PREMIUM PACKAGE Renewed for Account (JPRC0151032/1000100083165180) from 2019-03-12 to 2019-04-11 for a period of 29 days and charged 410.69][Base Bouque Changed from DD PACCHIS(429) to HD PREMIUM PACKAGE(8)][By jitin]
         * created_at : 2019-03-12 18:50:47
         * updated_at : 2019-03-12 18:50:47
         * created_by : 3
         * updated_by : 3
         * status_lbl : Active
         * created_by_lbl : Jitin
         * boxtype_lbl : HD
         * type_lbl : Base
         */

        private String id;
        private int bouque_id;
        private int bouque_type;
        private String name;
        private String account_id;
        private String subscriber_id;
        private String activation_date;
        private String deactivation_date;
        private String left;
        private String noofmonths;
        private RefundAbleBean refundAble;
        private int status;
        private int cas_status;
        private String amount;
        private String tax;
        private Object refund_amount;
        private Object refund_tax;
        private Object refund_date;
        private String remark;
        private String created_at;
        private String updated_at;
        private int created_by;
        private int updated_by;
        private String status_lbl;
        private String created_by_lbl;
        private String boxtype_lbl;
        private String type_lbl;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public int getBouque_id() {
            return bouque_id;
        }

        public void setBouque_id(int bouque_id) {
            this.bouque_id = bouque_id;
        }

        public int getBouque_type() {
            return bouque_type;
        }

        public void setBouque_type(int bouque_type) {
            this.bouque_type = bouque_type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAccount_id() {
            return account_id;
        }

        public void setAccount_id(String account_id) {
            this.account_id = account_id;
        }

        public String getSubscriber_id() {
            return subscriber_id;
        }

        public void setSubscriber_id(String subscriber_id) {
            this.subscriber_id = subscriber_id;
        }

        public String getActivation_date() {
            return activation_date;
        }

        public void setActivation_date(String activation_date) {
            this.activation_date = activation_date;
        }

        public String getDeactivation_date() {
            return deactivation_date;
        }

        public void setDeactivation_date(String deactivation_date) {
            this.deactivation_date = deactivation_date;
        }

        public String getLeft() {
            return left;
        }

        public void setLeft(String left) {
            this.left = left;
        }

        public String getNoofmonths() {
            return noofmonths;
        }

        public void setNoofmonths(String noofmonths) {
            this.noofmonths = noofmonths;
        }

        public RefundAbleBean getRefundAble() {
            return refundAble;
        }

        public void setRefundAble(RefundAbleBean refundAble) {
            this.refundAble = refundAble;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getCas_status() {
            return cas_status;
        }

        public void setCas_status(int cas_status) {
            this.cas_status = cas_status;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public Object getRefund_amount() {
            return refund_amount;
        }

        public void setRefund_amount(Object refund_amount) {
            this.refund_amount = refund_amount;
        }

        public Object getRefund_tax() {
            return refund_tax;
        }

        public void setRefund_tax(Object refund_tax) {
            this.refund_tax = refund_tax;
        }

        public Object getRefund_date() {
            return refund_date;
        }

        public void setRefund_date(Object refund_date) {
            this.refund_date = refund_date;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public int getCreated_by() {
            return created_by;
        }

        public void setCreated_by(int created_by) {
            this.created_by = created_by;
        }

        public int getUpdated_by() {
            return updated_by;
        }

        public void setUpdated_by(int updated_by) {
            this.updated_by = updated_by;
        }

        public String getStatus_lbl() {
            return status_lbl;
        }

        public void setStatus_lbl(String status_lbl) {
            this.status_lbl = status_lbl;
        }

        public String getCreated_by_lbl() {
            return created_by_lbl;
        }

        public void setCreated_by_lbl(String created_by_lbl) {
            this.created_by_lbl = created_by_lbl;
        }

        public String getBoxtype_lbl() {
            return boxtype_lbl;
        }

        public void setBoxtype_lbl(String boxtype_lbl) {
            this.boxtype_lbl = boxtype_lbl;
        }

        public String getType_lbl() {
            return type_lbl;
        }

        public void setType_lbl(String type_lbl) {
            this.type_lbl = type_lbl;
        }

        public static class RefundAbleBean implements Serializable {
            /**
             * amt : 11.6013
             * tax : 2.0883
             */

            private double amt;
            private double tax;

            public double getAmt() {
                return amt;
            }

            public void setAmt(double amt) {
                this.amt = amt;
            }

            public double getTax() {
                return tax;
            }

            public void setTax(double tax) {
                this.tax = tax;
            }
        }
    }
}
