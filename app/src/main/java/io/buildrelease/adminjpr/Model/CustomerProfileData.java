package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CustomerProfileData {


    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class SublocationDetails {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("location_id")
        @Expose
        private Integer locationId;
        @SerializedName("location_lbl")
        @Expose
        private String locationLbl;
        @SerializedName("operator_id")
        @Expose
        private Integer operatorId;
        @SerializedName("operator_lbl")
        @Expose
        private String operatorLbl;
        @SerializedName("mso_id")
        @Expose
        private Integer msoId;
        @SerializedName("mso_lbl")
        @Expose
        private String msoLbl;
        @SerializedName("branch_id")
        @Expose
        private Integer branchId;
        @SerializedName("branch_lbl")
        @Expose
        private String branchLbl;
        @SerializedName("distributor_id")
        @Expose
        private Integer distributorId;
        @SerializedName("distributor_lbl")
        @Expose
        private String distributorLbl;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getLocationId() {
            return locationId;
        }

        public void setLocationId(Integer locationId) {
            this.locationId = locationId;
        }

        public String getLocationLbl() {
            return locationLbl;
        }

        public void setLocationLbl(String locationLbl) {
            this.locationLbl = locationLbl;
        }

        public Integer getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(Integer operatorId) {
            this.operatorId = operatorId;
        }

        public String getOperatorLbl() {
            return operatorLbl;
        }

        public void setOperatorLbl(String operatorLbl) {
            this.operatorLbl = operatorLbl;
        }

        public Integer getMsoId() {
            return msoId;
        }

        public void setMsoId(Integer msoId) {
            this.msoId = msoId;
        }

        public String getMsoLbl() {
            return msoLbl;
        }

        public void setMsoLbl(String msoLbl) {
            this.msoLbl = msoLbl;
        }

        public Integer getBranchId() {
            return branchId;
        }

        public void setBranchId(Integer branchId) {
            this.branchId = branchId;
        }

        public String getBranchLbl() {
            return branchLbl;
        }

        public void setBranchLbl(String branchLbl) {
            this.branchLbl = branchLbl;
        }

        public Integer getDistributorId() {
            return distributorId;
        }

        public void setDistributorId(Integer distributorId) {
            this.distributorId = distributorId;
        }

        public String getDistributorLbl() {
            return distributorLbl;
        }

        public void setDistributorLbl(String distributorLbl) {
            this.distributorLbl = distributorLbl;
        }

    }

    public class Profile {

        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("formno")
        @Expose
        private String formno;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("fname")
        @Expose
        private String fname;
        @SerializedName("lname")
        @Expose
        private String lname;
        @SerializedName("mname")
        @Expose
        private String mname;
        @SerializedName("billing_address")
        @Expose
        private BillingAddress billingAddress;
        @SerializedName("pincode")
        @Expose
        private String pincode;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("mobile_no")
        @Expose
        private String mobileNo;
        @SerializedName("phone_no")
        @Expose
        private String phoneNo;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("customer_type")
        @Expose
        private String customerType;
        @SerializedName("operator")
        @Expose
        private String operator;
        @SerializedName("operator_contactno")
        @Expose
        private String operatorContactno;
        @SerializedName("sublocation_details")
        @Expose
        private SublocationDetails sublocationDetails;
        @SerializedName("address")
        @Expose
        private Address address;

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public String getFormno() {
            return formno;
        }

        public void setFormno(String formno) {
            this.formno = formno;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getLname() {
            return lname;
        }

        public void setLname(String lname) {
            this.lname = lname;
        }

        public String getMname() {
            return mname;
        }

        public void setMname(String mname) {
            this.mname = mname;
        }

        public BillingAddress getBillingAddress() {
            return billingAddress;
        }

        public void setBillingAddress(BillingAddress billingAddress) {
            this.billingAddress = billingAddress;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getCustomerType() {
            return customerType;
        }

        public void setCustomerType(String customerType) {
            this.customerType = customerType;
        }

        public String getOperator() {
            return operator;
        }

        public void setOperator(String operator) {
            this.operator = operator;
        }

        public String getOperatorContactno() {
            return operatorContactno;
        }

        public void setOperatorContactno(String operatorContactno) {
            this.operatorContactno = operatorContactno;
        }

        public SublocationDetails getSublocationDetails() {
            return sublocationDetails;
        }

        public void setSublocationDetails(SublocationDetails sublocationDetails) {
            this.sublocationDetails = sublocationDetails;
        }

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

    }
    public static  class Data {

        @SerializedName("profile")
        @Expose
        private Profile profile;
        @SerializedName("accounts")
        @Expose
        private List<Account> accounts = null;

        public Profile getProfile() {
            return profile;
        }

        public void setProfile(Profile profile) {
            this.profile = profile;
        }

        public List<Account> getAccounts() {
            return accounts;
        }

        public void setAccounts(List<Account> accounts) {
            this.accounts = accounts;
        }

    }

    public class BillingAddress {

        @SerializedName("addr")
        @Expose
        private String addr;
        @SerializedName("pincode")
        @Expose
        private String pincode;

        public String getAddr() {
            return addr;
        }

        public void setAddr(String addr) {
            this.addr = addr;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

    }
    public class Address {

        @SerializedName("flatno")
        @Expose
        private Object flatno;
        @SerializedName("floor")
        @Expose
        private Object floor;
        @SerializedName("wing")
        @Expose
        private Object wing;
        @SerializedName("installation_address")
        @Expose
        private Object installationAddress;

        public Object getFlatno() {
            return flatno;
        }

        public void setFlatno(Object flatno) {
            this.flatno = flatno;
        }

        public Object getFloor() {
            return floor;
        }

        public void setFloor(Object floor) {
            this.floor = floor;
        }

        public Object getWing() {
            return wing;
        }

        public void setWing(Object wing) {
            this.wing = wing;
        }

        public Object getInstallationAddress() {
            return installationAddress;
        }

        public void setInstallationAddress(Object installationAddress) {
            this.installationAddress = installationAddress;
        }

    }

    public class Account {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("smartcardno")
        @Expose
        private String smartcardno;
        @SerializedName("stbno")
        @Expose
        private String stbno;
        @SerializedName("activation_date")
        @Expose
        private String activationDate;
        @SerializedName("deactivation_date")
        @Expose
        private String deactivationDate;
        @SerializedName("outstanding")
        @Expose
        private Integer outstanding;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("brand_id")
        @Expose
        private Integer brandId;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSmartcardno() {
            return smartcardno;
        }

        public void setSmartcardno(String smartcardno) {
            this.smartcardno = smartcardno;
        }

        public String getStbno() {
            return stbno;
        }

        public void setStbno(String stbno) {
            this.stbno = stbno;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public Integer getOutstanding() {
            return outstanding;
        }

        public void setOutstanding(Integer outstanding) {
            this.outstanding = outstanding;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public Integer getBrandId() {
            return brandId;
        }

        public void setBrandId(Integer brandId) {
            this.brandId = brandId;
        }

    }
}
