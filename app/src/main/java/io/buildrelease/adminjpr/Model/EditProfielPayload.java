package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EditProfielPayload {

    @SerializedName("billing_address")
    @Expose
    private BillingAddress billingAddress;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("flatno")
    @Expose
    private String flatno;
    @SerializedName("floor")
    @Expose
    private String floor;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("formno")
    @Expose
    private String formno;
    @SerializedName("installation_address")
    @Expose
    private String installationAddress;
    @SerializedName("lname")
    @Expose
    private String lname;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("pincode")
    @Expose
    private String pincode;
    @SerializedName("wing")
    @Expose
    private String wing;
    @SerializedName("upload_proof")
    @Expose
    private List<UploadProof> uploadProof = null;

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFlatno() {
        return flatno;
    }

    public void setFlatno(String flatno) {
        this.flatno = flatno;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getFormno() {
        return formno;
    }

    public void setFormno(String formno) {
        this.formno = formno;
    }

    public String getInstallationAddress() {
        return installationAddress;
    }

    public void setInstallationAddress(String installationAddress) {
        this.installationAddress = installationAddress;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getWing() {
        return wing;
    }

    public void setWing(String wing) {
        this.wing = wing;
    }

    public List<UploadProof> getUploadProof() {
        return uploadProof;
    }

    public void setUploadProof(List<UploadProof> uploadProof) {
        this.uploadProof = uploadProof;
    }




    public static class BillingAddress {

        @SerializedName("addr")
        @Expose
        private String addr;
        @SerializedName("pincode")
        @Expose
        private String pincode;

        public String getAddr() {
            return addr;
        }

        public void setAddr(String addr) {
            this.addr = addr;
        }

        public String getPincode() {
            return pincode;
        }

        public void setPincode(String pincode) {
            this.pincode = pincode;
        }

    }

    public  static class UploadProof {

        @SerializedName("data")
        @Expose
        private String data;
        @SerializedName("ext")
        @Expose
        private String ext;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("proof_type")
        @Expose
        private String proofType;
        @SerializedName("type")
        @Expose
        private String type;

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public String getExt() {
            return ext;
        }

        public void setExt(String ext) {
            this.ext = ext;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProofType() {
            return proofType;
        }

        public void setProofType(String proofType) {
            this.proofType = proofType;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }}
}
