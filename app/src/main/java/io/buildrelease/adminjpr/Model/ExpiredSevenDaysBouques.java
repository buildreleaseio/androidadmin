package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExpiredSevenDaysBouques {

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;


    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Bouque {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("isHD")
        @Expose
        private String isHD;
        @SerializedName("type")
        @Expose
        private String type;


        public Bouque() {
        }


        public Bouque(String id, String name, String isHD, String type) {
            super();
            this.id = id;
            this.name = name;
            this.isHD = isHD;
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIsHD() {
            return isHD;
        }

        public void setIsHD(String isHD) {
            this.isHD = isHD;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }



    }

    public class Datum {

        @SerializedName("counts")
        @Expose
        private String counts;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("bouque_id")
        @Expose
        private String bouqueId;
        @SerializedName("bouque")
        @Expose
        private Bouque bouque;


        public Datum() {
        }


        public Datum(String counts, String name, String bouqueId, Bouque bouque) {
            super();
            this.counts = counts;
            this.name = name;
            this.bouqueId = bouqueId;
            this.bouque = bouque;
        }

        public String getCounts() {
            return counts;
        }

        public void setCounts(String counts) {
            this.counts = counts;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getBouqueId() {
            return bouqueId;
        }

        public void setBouqueId(String bouqueId) {
            this.bouqueId = bouqueId;
        }

        public Bouque getBouque() {
            return bouque;
        }

        public void setBouque(Bouque bouque) {
            this.bouque = bouque;
        }

    }
}
