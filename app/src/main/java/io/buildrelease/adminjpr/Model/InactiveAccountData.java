package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InactiveAccountData {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("subscriber_id")
        @Expose
        private String subscriberId;
        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("operator_id")
        @Expose
        private Integer operatorId;
        @SerializedName("location_id")
        @Expose
        private Integer locationId;
        @SerializedName("sublocation_id")
        @Expose
        private Integer sublocationId;
        @SerializedName("pairing_id")
        @Expose
        private Integer pairingId;
        @SerializedName("daysLeft")
        @Expose
        private String daysLeft;
        @SerializedName("isLocked")
        @Expose
        private Boolean isLocked;
        @SerializedName("isRefreshAble")
        @Expose
        private Boolean isRefreshAble;
        @SerializedName("smartcardno")
        @Expose
        private String smartcardno;
        @SerializedName("stbno")
        @Expose
        private String stbno;
        @SerializedName("lockTimeLeft")
        @Expose
        private Integer lockTimeLeft;
        @SerializedName("stb_id")
        @Expose
        private Integer stbId;
        @SerializedName("sc_id")
        @Expose
        private Integer scId;
        @SerializedName("cas_id")
        @Expose
        private Integer casId;
        @SerializedName("isHD")
        @Expose
        private Integer isHD;
        @SerializedName("is_embeded")
        @Expose
        private Object isEmbeded;
        @SerializedName("stbbrand_id")
        @Expose
        private Integer stbbrandId;
        @SerializedName("scheme_id")
        @Expose
        private Integer schemeId;
        @SerializedName("bouque_id")
        @Expose
        private Integer bouqueId;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("activation_date")
        @Expose
        private String activationDate;
        @SerializedName("deactivation_date")
        @Expose
        private String deactivationDate;
        @SerializedName("outstanding")
        @Expose
        private Object outstanding;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("remark")
        @Expose
        private String remark;
        @SerializedName("mobile_no")
        @Expose
        private String mobileNo;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("operator_name_lbl")
        @Expose
        private String operatorNameLbl;
        @SerializedName("operator_code_lbl")
        @Expose
        private String operatorCodeLbl;
        @SerializedName("cas_status")
        @Expose
        private Integer casStatus;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_by")
        @Expose
        private Integer createdBy;
        @SerializedName("updated_by")
        @Expose
        private Integer updatedBy;
        @SerializedName("boxtype_lbl")
        @Expose
        private String boxtypeLbl;
        @SerializedName("status_lbl")
        @Expose
        private String statusLbl;
        @SerializedName("created_by_lbl")
        @Expose
        private String createdByLbl;
        @SerializedName("operator_lbl")
        @Expose
        private String operatorLbl;
        @SerializedName("location_lbl")
        @Expose
        private String locationLbl;
        @SerializedName("sublocation_lbl")
        @Expose
        private String sublocationLbl;
        @SerializedName("cas_lbl")
        @Expose
        private String casLbl;
        @SerializedName("brand_lbl")
        @Expose
        private String brandLbl;
        @SerializedName("bouqueD")
        @Expose
        private List<BouqueD> bouqueD = null;
        @SerializedName("bouque_lbl")
        @Expose
        private String bouqueLbl;
        @SerializedName("customer_name")
        @Expose
        private String customerName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSubscriberId() {
            return subscriberId;
        }

        public void setSubscriberId(String subscriberId) {
            this.subscriberId = subscriberId;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public Integer getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(Integer operatorId) {
            this.operatorId = operatorId;
        }

        public Integer getLocationId() {
            return locationId;
        }

        public void setLocationId(Integer locationId) {
            this.locationId = locationId;
        }

        public Integer getSublocationId() {
            return sublocationId;
        }

        public void setSublocationId(Integer sublocationId) {
            this.sublocationId = sublocationId;
        }

        public Integer getPairingId() {
            return pairingId;
        }

        public void setPairingId(Integer pairingId) {
            this.pairingId = pairingId;
        }

        public String getDaysLeft() {
            return daysLeft;
        }

        public void setDaysLeft(String daysLeft) {
            this.daysLeft = daysLeft;
        }

        public Boolean getIsLocked() {
            return isLocked;
        }

        public void setIsLocked(Boolean isLocked) {
            this.isLocked = isLocked;
        }

        public Boolean getIsRefreshAble() {
            return isRefreshAble;
        }

        public void setIsRefreshAble(Boolean isRefreshAble) {
            this.isRefreshAble = isRefreshAble;
        }

        public String getSmartcardno() {
            return smartcardno;
        }

        public void setSmartcardno(String smartcardno) {
            this.smartcardno = smartcardno;
        }

        public String getStbno() {
            return stbno;
        }

        public void setStbno(String stbno) {
            this.stbno = stbno;
        }

        public Integer getLockTimeLeft() {
            return lockTimeLeft;
        }

        public void setLockTimeLeft(Integer lockTimeLeft) {
            this.lockTimeLeft = lockTimeLeft;
        }

        public Integer getStbId() {
            return stbId;
        }

        public void setStbId(Integer stbId) {
            this.stbId = stbId;
        }

        public Integer getScId() {
            return scId;
        }

        public void setScId(Integer scId) {
            this.scId = scId;
        }

        public Integer getCasId() {
            return casId;
        }

        public void setCasId(Integer casId) {
            this.casId = casId;
        }

        public Integer getIsHD() {
            return isHD;
        }

        public void setIsHD(Integer isHD) {
            this.isHD = isHD;
        }

        public Object getIsEmbeded() {
            return isEmbeded;
        }

        public void setIsEmbeded(Object isEmbeded) {
            this.isEmbeded = isEmbeded;
        }

        public Integer getStbbrandId() {
            return stbbrandId;
        }

        public void setStbbrandId(Integer stbbrandId) {
            this.stbbrandId = stbbrandId;
        }

        public Integer getSchemeId() {
            return schemeId;
        }

        public void setSchemeId(Integer schemeId) {
            this.schemeId = schemeId;
        }

        public Integer getBouqueId() {
            return bouqueId;
        }

        public void setBouqueId(Integer bouqueId) {
            this.bouqueId = bouqueId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public Object getOutstanding() {
            return outstanding;
        }

        public void setOutstanding(Object outstanding) {
            this.outstanding = outstanding;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getOperatorNameLbl() {
            return operatorNameLbl;
        }

        public void setOperatorNameLbl(String operatorNameLbl) {
            this.operatorNameLbl = operatorNameLbl;
        }

        public String getOperatorCodeLbl() {
            return operatorCodeLbl;
        }

        public void setOperatorCodeLbl(String operatorCodeLbl) {
            this.operatorCodeLbl = operatorCodeLbl;
        }

        public Integer getCasStatus() {
            return casStatus;
        }

        public void setCasStatus(Integer casStatus) {
            this.casStatus = casStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Integer createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Integer updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getBoxtypeLbl() {
            return boxtypeLbl;
        }

        public void setBoxtypeLbl(String boxtypeLbl) {
            this.boxtypeLbl = boxtypeLbl;
        }

        public String getStatusLbl() {
            return statusLbl;
        }

        public void setStatusLbl(String statusLbl) {
            this.statusLbl = statusLbl;
        }

        public String getCreatedByLbl() {
            return createdByLbl;
        }

        public void setCreatedByLbl(String createdByLbl) {
            this.createdByLbl = createdByLbl;
        }

        public String getOperatorLbl() {
            return operatorLbl;
        }

        public void setOperatorLbl(String operatorLbl) {
            this.operatorLbl = operatorLbl;
        }

        public String getLocationLbl() {
            return locationLbl;
        }

        public void setLocationLbl(String locationLbl) {
            this.locationLbl = locationLbl;
        }

        public String getSublocationLbl() {
            return sublocationLbl;
        }

        public void setSublocationLbl(String sublocationLbl) {
            this.sublocationLbl = sublocationLbl;
        }

        public String getCasLbl() {
            return casLbl;
        }

        public void setCasLbl(String casLbl) {
            this.casLbl = casLbl;
        }

        public String getBrandLbl() {
            return brandLbl;
        }

        public void setBrandLbl(String brandLbl) {
            this.brandLbl = brandLbl;
        }

        public List<BouqueD> getBouqueD() {
            return bouqueD;
        }

        public void setBouqueD(List<BouqueD> bouqueD) {
            this.bouqueD = bouqueD;
        }

        public String getBouqueLbl() {
            return bouqueLbl;
        }

        public void setBouqueLbl(String bouqueLbl) {
            this.bouqueLbl = bouqueLbl;
        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }

    }

    public static class BouqueD {

        @SerializedName("bouque_id")
        @Expose
        private Integer bouqueId;
        @SerializedName("account_id")
        @Expose
        private String accountId;
        @SerializedName("bouque_type")
        @Expose
        private Integer bouqueType;
        @SerializedName("activation_date")
        @Expose
        private String activationDate;
        @SerializedName("deactivation_date")
        @Expose
        private String deactivationDate;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("tax")
        @Expose
        private String tax;
        @SerializedName("left")
        @Expose
        private String left;
        @SerializedName("bouque_name")
        @Expose
        private String bouqueName;
        @SerializedName("refundAble")
        @Expose
        private RefundAble refundAble;

        public Integer getBouqueId() {
            return bouqueId;
        }

        public void setBouqueId(Integer bouqueId) {
            this.bouqueId = bouqueId;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public Integer getBouqueType() {
            return bouqueType;
        }

        public void setBouqueType(Integer bouqueType) {
            this.bouqueType = bouqueType;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getLeft() {
            return left;
        }

        public void setLeft(String left) {
            this.left = left;
        }

        public String getBouqueName() {
            return bouqueName;
        }

        public void setBouqueName(String bouqueName) {
            this.bouqueName = bouqueName;
        }

        public RefundAble getRefundAble() {
            return refundAble;
        }

        public void setRefundAble(RefundAble refundAble) {
            this.refundAble = refundAble;
        }

    }

    public static class RefundAble {

        @SerializedName("amt")
        @Expose
        private Double amt;
        @SerializedName("tax")
        @Expose
        private Double tax;

        public Double getAmt() {
            return amt;
        }

        public void setAmt(Double amt) {
            this.amt = amt;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

    }




}
