package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InventoryData {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public static class Data {

        @SerializedName("embeded")
        @Expose
        private Embeded embeded;
        @SerializedName("smartcard")
        @Expose
        private Smartcard smartcard;
        @SerializedName("stb")
        @Expose
        private Stb stb;
        @SerializedName("pairing")
        @Expose
        private Pairing pairing;

        public Embeded getEmbeded() {
            return embeded;
        }

        public void setEmbeded(Embeded embeded) {
            this.embeded = embeded;
        }

        public Smartcard getSmartcard() {
            return smartcard;
        }

        public void setSmartcard(Smartcard smartcard) {
            this.smartcard = smartcard;
        }

        public Stb getStb() {
            return stb;
        }

        public void setStb(Stb stb) {
            this.stb = stb;
        }

        public Pairing getPairing() {
            return pairing;
        }

        public void setPairing(Pairing pairing) {
            this.pairing = pairing;
        }

    }
    public static  class Embeded {

        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("stock")
        @Expose
        private String stock;
        @SerializedName("alloted")
        @Expose
        private String alloted;
        @SerializedName("assigned")
        @Expose
        private String assigned;
        @SerializedName("active")
        @Expose
        private String active;
        @SerializedName("faulty")
        @Expose
        private String faulty;
        @SerializedName("blacklisted")
        @Expose
        private String blacklisted;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getAlloted() {
            return alloted;
        }

        public void setAlloted(String alloted) {
            this.alloted = alloted;
        }

        public String getAssigned() {
            return assigned;
        }

        public void setAssigned(String assigned) {
            this.assigned = assigned;
        }

        public String getActive() {
            return active;
        }

        public void setActive(String active) {
            this.active = active;
        }

        public String getFaulty() {
            return faulty;
        }

        public void setFaulty(String faulty) {
            this.faulty = faulty;
        }

        public String getBlacklisted() {
            return blacklisted;
        }

        public void setBlacklisted(String blacklisted) {
            this.blacklisted = blacklisted;
        }

    }

    public class Pairing {

        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("stock")
        @Expose
        private String stock;
        @SerializedName("alloted")
        @Expose
        private String alloted;
        @SerializedName("assigned")
        @Expose
        private String assigned;
        @SerializedName("active")
        @Expose
        private String active;
        @SerializedName("faulty")
        @Expose
        private String faulty;
        @SerializedName("blacklisted")
        @Expose
        private String blacklisted;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getAlloted() {
            return alloted;
        }

        public void setAlloted(String alloted) {
            this.alloted = alloted;
        }

        public String getAssigned() {
            return assigned;
        }

        public void setAssigned(String assigned) {
            this.assigned = assigned;
        }

        public String getActive() {
            return active;
        }

        public void setActive(String active) {
            this.active = active;
        }

        public String getFaulty() {
            return faulty;
        }

        public void setFaulty(String faulty) {
            this.faulty = faulty;
        }

        public String getBlacklisted() {
            return blacklisted;
        }

        public void setBlacklisted(String blacklisted) {
            this.blacklisted = blacklisted;
        }

    }

    public class Smartcard {

        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("stock")
        @Expose
        private String stock;
        @SerializedName("alloted")
        @Expose
        private String alloted;
        @SerializedName("assigned")
        @Expose
        private String assigned;
        @SerializedName("active")
        @Expose
        private String active;
        @SerializedName("faulty")
        @Expose
        private String faulty;
        @SerializedName("blacklisted")
        @Expose
        private String blacklisted;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getAlloted() {
            return alloted;
        }

        public void setAlloted(String alloted) {
            this.alloted = alloted;
        }

        public String getAssigned() {
            return assigned;
        }

        public void setAssigned(String assigned) {
            this.assigned = assigned;
        }

        public String getActive() {
            return active;
        }

        public void setActive(String active) {
            this.active = active;
        }

        public String getFaulty() {
            return faulty;
        }

        public void setFaulty(String faulty) {
            this.faulty = faulty;
        }

        public String getBlacklisted() {
            return blacklisted;
        }

        public void setBlacklisted(String blacklisted) {
            this.blacklisted = blacklisted;
        }

    }

    public class Stb {

        @SerializedName("total")
        @Expose
        private String total;
        @SerializedName("stock")
        @Expose
        private String stock;
        @SerializedName("alloted")
        @Expose
        private String alloted;
        @SerializedName("assigned")
        @Expose
        private String assigned;
        @SerializedName("active")
        @Expose
        private String active;
        @SerializedName("faulty")
        @Expose
        private String faulty;
        @SerializedName("blacklisted")
        @Expose
        private String blacklisted;

        public String getTotal() {
            return total;
        }

        public void setTotal(String total) {
            this.total = total;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getAlloted() {
            return alloted;
        }

        public void setAlloted(String alloted) {
            this.alloted = alloted;
        }

        public String getAssigned() {
            return assigned;
        }

        public void setAssigned(String assigned) {
            this.assigned = assigned;
        }

        public String getActive() {
            return active;
        }

        public void setActive(String active) {
            this.active = active;
        }

        public String getFaulty() {
            return faulty;
        }

        public void setFaulty(String faulty) {
            this.faulty = faulty;
        }

        public String getBlacklisted() {
            return blacklisted;
        }

        public void setBlacklisted(String blacklisted) {
            this.blacklisted = blacklisted;
        }

    }


}
