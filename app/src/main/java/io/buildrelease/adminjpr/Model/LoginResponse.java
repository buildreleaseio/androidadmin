package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("role")
        @Expose
        private Integer role;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile_no")
        @Expose
        private String mobileNo;
        @SerializedName("force_reset_password")
        @Expose
        private Boolean forceResetPassword;
        @SerializedName("type")
        @Expose
        private Integer type;
        @SerializedName("last_login_at")
        @Expose
        private String lastLoginAt;
        @SerializedName("type_label")
        @Expose
        private String typeLabel;
        @SerializedName("operator_id")
        @Expose
        private String operatorId;
        @SerializedName("session_id")
        @Expose
        private String sessionId;
        @SerializedName("subscriber_id")
        @Expose
        private String subscriberId;
        @SerializedName("balance")
        @Expose
        private Double balance;
        @SerializedName("operator_lbl")
        @Expose
        private String operatorLbl;
        @SerializedName("access_token")
        @Expose
        private String accessToken;
        @SerializedName("expiry")
        @Expose
        private String expiry;
        @SerializedName("permission")
        @Expose
        private Permission permission;
        @SerializedName("app_permission")
        @Expose
        private AppPermission appPermission;
        @SerializedName("MRP_ENABLED")
        @Expose
        private Integer mRPENABLED;
        @SerializedName("cas")
        @Expose
        private List<Cas> cas = null;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRole() {
            return role;
        }

        public void setRole(Integer role) {
            this.role = role;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public Boolean getForceResetPassword() {
            return forceResetPassword;
        }

        public void setForceResetPassword(Boolean forceResetPassword) {
            this.forceResetPassword = forceResetPassword;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getLastLoginAt() {
            return lastLoginAt;
        }

        public void setLastLoginAt(String lastLoginAt) {
            this.lastLoginAt = lastLoginAt;
        }

        public String getTypeLabel() {
            return typeLabel;
        }

        public void setTypeLabel(String typeLabel) {
            this.typeLabel = typeLabel;
        }

        public String getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(String operatorId) {
            this.operatorId = operatorId;
        }

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public String getSubscriberId() {
            return subscriberId;
        }

        public void setSubscriberId(String subscriberId) {
            this.subscriberId = subscriberId;
        }

        public Double getBalance() {
            return balance;
        }

        public void setBalance(Double balance) {
            this.balance = balance;
        }

        public String getOperatorLbl() {
            return operatorLbl;
        }

        public void setOperatorLbl(String operatorLbl) {
            this.operatorLbl = operatorLbl;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getExpiry() {
            return expiry;
        }

        public void setExpiry(String expiry) {
            this.expiry = expiry;
        }

        public Permission getPermission() {
            return permission;
        }

        public void setPermission(Permission permission) {
            this.permission = permission;
        }

        public AppPermission getAppPermission() {
            return appPermission;
        }

        public void setAppPermission(AppPermission appPermission) {
            this.appPermission = appPermission;
        }

        public Integer getMRPENABLED() {
            return mRPENABLED;
        }

        public void setMRPENABLED(Integer mRPENABLED) {
            this.mRPENABLED = mRPENABLED;
        }

        public List<Cas> getCas() {
            return cas;
        }

        public void setCas(List<Cas> cas) {
            this.cas = cas;
        }

    }

    public class AppPermission {

        @SerializedName("Profile")
        @Expose
        private Integer profile;
        @SerializedName("UpdateProfile")
        @Expose
        private Integer updateProfile;
        @SerializedName("UploadKyc")
        @Expose
        private Integer uploadKyc;
        @SerializedName("Renew")
        @Expose
        private Integer renew;
        @SerializedName("AddAddon")
        @Expose
        private Integer addAddon;
        @SerializedName("CMS")
        @Expose
        private Integer cMS;
        @SerializedName("UpdateStatus")
        @Expose
        private Integer updateStatus;

        public Integer getProfile() {
            return profile;
        }

        public void setProfile(Integer profile) {
            this.profile = profile;
        }

        public Integer getUpdateProfile() {
            return updateProfile;
        }

        public void setUpdateProfile(Integer updateProfile) {
            this.updateProfile = updateProfile;
        }

        public Integer getUploadKyc() {
            return uploadKyc;
        }

        public void setUploadKyc(Integer uploadKyc) {
            this.uploadKyc = uploadKyc;
        }

        public Integer getRenew() {
            return renew;
        }

        public void setRenew(Integer renew) {
            this.renew = renew;
        }

        public Integer getAddAddon() {
            return addAddon;
        }

        public void setAddAddon(Integer addAddon) {
            this.addAddon = addAddon;
        }

        public Integer getCMS() {
            return cMS;
        }

        public void setCMS(Integer cMS) {
            this.cMS = cMS;
        }

        public Integer getUpdateStatus() {
            return updateStatus;
        }

        public void setUpdateStatus(Integer updateStatus) {
            this.updateStatus = updateStatus;
        }

    }

    public class Cas {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("name")
        @Expose
        private String name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class Permission {

        @SerializedName("list")
        @Expose
        private List<String> list = null;
        @SerializedName("manage")
        @Expose
        private List<String> manage = null;
        @SerializedName("view")
        @Expose
        private List<String> view = null;

        public List<String> getList() {
            return list;
        }

        public void setList(List<String> list) {
            this.list = list;
        }

        public List<String> getManage() {
            return manage;
        }

        public void setManage(List<String> manage) {
            this.manage = manage;
        }

        public List<String> getView() {
            return view;
        }

        public void setView(List<String> view) {
            this.view = view;
        }


    }

}
