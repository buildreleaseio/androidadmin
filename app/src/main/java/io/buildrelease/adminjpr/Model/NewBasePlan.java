package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NewBasePlan {


    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("mrp")
        @Expose
        private Double mrp;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("rate")
        @Expose
        private List<Rate> rate = null;
        @SerializedName("boxtype_lbl")
        @Expose
        private String boxtypeLbl;
        @SerializedName("type_lbl")
        @Expose
        private String typeLbl;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public Double getMrp() {
            return mrp;
        }

        public void setMrp(Double mrp) {
            this.mrp = mrp;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<Rate> getRate() {
            return rate;
        }

        public void setRate(List<Rate> rate) {
            this.rate = rate;
        }

        public String getBoxtypeLbl() {
            return boxtypeLbl;
        }

        public void setBoxtypeLbl(String boxtypeLbl) {
            this.boxtypeLbl = boxtypeLbl;
        }

        public String getTypeLbl() {
            return typeLbl;
        }

        public void setTypeLbl(String typeLbl) {
            this.typeLbl = typeLbl;
        }

    }

    public class Rate {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("months")
        @Expose
        private Integer months;
        @SerializedName("days")
        @Expose
        private Integer days;
        @SerializedName("amount")
        @Expose
        private Double amount;
        @SerializedName("tax_amount")
        @Expose
        private Double taxAmount;
        @SerializedName("price")
        @Expose
        private Double price;
        @SerializedName("rental")
        @Expose
        private Double rental;
        @SerializedName("name")
        @Expose
        private String name;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getMonths() {
            return months;
        }

        public void setMonths(Integer months) {
            this.months = months;
        }

        public Integer getDays() {
            return days;
        }

        public void setDays(Integer days) {
            this.days = days;
        }

        public Double getAmount() {
            return amount;
        }

        public void setAmount(Double amount) {
            this.amount = amount;
        }

        public Double getTaxAmount() {
            return taxAmount;
        }

        public void setTaxAmount(Double taxAmount) {
            this.taxAmount = taxAmount;
        }

        public Double getPrice() {
            return price;
        }

        public void setPrice(Double price) {
            this.price = price;
        }

        public Double getRental() {
            return rental;
        }

        public void setRental(Double rental) {
            this.rental = rental;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }


    }
}