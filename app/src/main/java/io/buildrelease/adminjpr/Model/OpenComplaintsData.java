package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OpenComplaintsData {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("ticketno")
        @Expose
        private String ticketno;
        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("subcategory_id")
        @Expose
        private Integer subcategoryId;
        @SerializedName("opening_remark")
        @Expose
        private String openingRemark;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("subscriber_id")
        @Expose
        private String subscriberId;
        @SerializedName("operator_id")
        @Expose
        private Integer operatorId;
        @SerializedName("account_id")
        @Expose
        private String accountId;
        @SerializedName("closing_remark")
        @Expose
        private Object closingRemark;
        @SerializedName("is_closed")
        @Expose
        private Integer isClosed;
        @SerializedName("location_id")
        @Expose
        private Integer locationId;
        @SerializedName("sublocation_id")
        @Expose
        private Integer sublocationId;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_by")
        @Expose
        private Integer createdBy;
        @SerializedName("updated_by")
        @Expose
        private Integer updatedBy;
        @SerializedName("priority")
        @Expose
        private Integer priority;
        @SerializedName("smartcard")
        @Expose
        private String smartcard;
        @SerializedName("stb")
        @Expose
        private String stb;
        @SerializedName("msg")
        @Expose
        private Object msg;
        @SerializedName("status_lbl")
        @Expose
        private String statusLbl;
        @SerializedName("created_by_lbl")
        @Expose
        private String createdByLbl;
        @SerializedName("location_lbl")
        @Expose
        private String locationLbl;
        @SerializedName("sublocation_lbl")
        @Expose
        private String sublocationLbl;
        @SerializedName("category_lbl")
        @Expose
        private String categoryLbl;
        @SerializedName("subcategory_lbl")
        @Expose
        private String subcategoryLbl;
        @SerializedName("smartcard_lbl")
        @Expose
        private String smartcardLbl;
        @SerializedName("stb_lbl")
        @Expose
        private String stbLbl;
        @SerializedName("name_lbl")
        @Expose
        private String nameLbl;
        @SerializedName("priority_lbl")
        @Expose
        private String priorityLbl;

        public Integer getId() {
            return id;
        }
        public void setId(Integer id) {
            this.id = id;
        }
        public String getTicketno() {
            return ticketno;
        }

        public void setTicketno(String ticketno) {
            this.ticketno = ticketno;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public Integer getSubcategoryId() {
            return subcategoryId;
        }

        public void setSubcategoryId(Integer subcategoryId) {
            this.subcategoryId = subcategoryId;
        }

        public String getOpeningRemark() {
            return openingRemark;
        }

        public void setOpeningRemark(String openingRemark) {
            this.openingRemark = openingRemark;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getSubscriberId() {
            return subscriberId;
        }

        public void setSubscriberId(String subscriberId) {
            this.subscriberId = subscriberId;
        }

        public Integer getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(Integer operatorId) {
            this.operatorId = operatorId;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public Object getClosingRemark() {
            return closingRemark;
        }

        public void setClosingRemark(Object closingRemark) {
            this.closingRemark = closingRemark;
        }

        public Integer getIsClosed() {
            return isClosed;
        }

        public void setIsClosed(Integer isClosed) {
            this.isClosed = isClosed;
        }

        public Integer getLocationId() {
            return locationId;
        }

        public void setLocationId(Integer locationId) {
            this.locationId = locationId;
        }

        public Integer getSublocationId() {
            return sublocationId;
        }

        public void setSublocationId(Integer sublocationId) {
            this.sublocationId = sublocationId;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Integer createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Integer updatedBy) {
            this.updatedBy = updatedBy;
        }

        public Integer getPriority() {
            return priority;
        }

        public void setPriority(Integer priority) {
            this.priority = priority;
        }

        public String getSmartcard() {
            return smartcard;
        }

        public void setSmartcard(String smartcard) {
            this.smartcard = smartcard;
        }

        public String getStb() {
            return stb;
        }

        public void setStb(String stb) {
            this.stb = stb;
        }

        public Object getMsg() {
            return msg;
        }

        public void setMsg(Object msg) {
            this.msg = msg;
        }

        public String getStatusLbl() {
            return statusLbl;
        }

        public void setStatusLbl(String statusLbl) {
            this.statusLbl = statusLbl;
        }

        public String getCreatedByLbl() {
            return createdByLbl;
        }

        public void setCreatedByLbl(String createdByLbl) {
            this.createdByLbl = createdByLbl;
        }

        public String getLocationLbl() {
            return locationLbl;
        }

        public void setLocationLbl(String locationLbl) {
            this.locationLbl = locationLbl;
        }

        public String getSublocationLbl() {
            return sublocationLbl;
        }

        public void setSublocationLbl(String sublocationLbl) {
            this.sublocationLbl = sublocationLbl;
        }

        public String getCategoryLbl() {
            return categoryLbl;
        }

        public void setCategoryLbl(String categoryLbl) {
            this.categoryLbl = categoryLbl;
        }

        public String getSubcategoryLbl() {
            return subcategoryLbl;
        }

        public void setSubcategoryLbl(String subcategoryLbl) {
            this.subcategoryLbl = subcategoryLbl;
        }

        public String getSmartcardLbl() {
            return smartcardLbl;
        }

        public void setSmartcardLbl(String smartcardLbl) {
            this.smartcardLbl = smartcardLbl;
        }

        public String getStbLbl() {
            return stbLbl;
        }

        public void setStbLbl(String stbLbl) {
            this.stbLbl = stbLbl;
        }

        public String getNameLbl() {
            return nameLbl;
        }

        public void setNameLbl(String nameLbl) {
            this.nameLbl = nameLbl;
        }

        public String getPriorityLbl() {
            return priorityLbl;
        }

        public void setPriorityLbl(String priorityLbl) {
            this.priorityLbl = priorityLbl;
        }

    }
}
