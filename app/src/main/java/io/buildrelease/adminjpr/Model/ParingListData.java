package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParingListData {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("sc_id")
        @Expose
        private Integer scId;
        @SerializedName("smartcardno")
        @Expose
        private String smartcardno;
        @SerializedName("cas_id")
        @Expose
        private Integer casId;
        @SerializedName("stb_id")
        @Expose
        private Integer stbId;
        @SerializedName("stbno")
        @Expose
        private String stbno;
        @SerializedName("isHD")
        @Expose
        private Integer isHD;
        @SerializedName("is_embeded")
        @Expose
        private Integer isEmbeded;
        @SerializedName("operator_id")
        @Expose
        private Integer operatorId;
        @SerializedName("alloted_on")
        @Expose
        private String allotedOn;
        @SerializedName("account_id")
        @Expose
        private Object accountId;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("isLocked")
        @Expose
        private Integer isLocked;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_by")
        @Expose
        private Integer createdBy;
        @SerializedName("updated_by")
        @Expose
        private Integer updatedBy;
        @SerializedName("boxtype_lbl")
        @Expose
        private String boxtypeLbl;
        @SerializedName("status_lbl")
        @Expose
        private String statusLbl;
        @SerializedName("created_by_lbl")
        @Expose
        private String createdByLbl;
        @SerializedName("brand_lbl")
        @Expose
        private String brandLbl;
        @SerializedName("cas_lbl")
        @Expose
        private String casLbl;
        @SerializedName("is_embeded_lbl")
        @Expose
        private String isEmbededLbl;
        @SerializedName("operator_lbl")
        @Expose
        private String operatorLbl;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getScId() {
            return scId;
        }

        public void setScId(Integer scId) {
            this.scId = scId;
        }

        public String getSmartcardno() {
            return smartcardno;
        }

        public void setSmartcardno(String smartcardno) {
            this.smartcardno = smartcardno;
        }

        public Integer getCasId() {
            return casId;
        }

        public void setCasId(Integer casId) {
            this.casId = casId;
        }

        public Integer getStbId() {
            return stbId;
        }

        public void setStbId(Integer stbId) {
            this.stbId = stbId;
        }

        public String getStbno() {
            return stbno;
        }

        public void setStbno(String stbno) {
            this.stbno = stbno;
        }

        public Integer getIsHD() {
            return isHD;
        }

        public void setIsHD(Integer isHD) {
            this.isHD = isHD;
        }

        public Integer getIsEmbeded() {
            return isEmbeded;
        }

        public void setIsEmbeded(Integer isEmbeded) {
            this.isEmbeded = isEmbeded;
        }

        public Integer getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(Integer operatorId) {
            this.operatorId = operatorId;
        }

        public String getAllotedOn() {
            return allotedOn;
        }

        public void setAllotedOn(String allotedOn) {
            this.allotedOn = allotedOn;
        }

        public Object getAccountId() {
            return accountId;
        }

        public void setAccountId(Object accountId) {
            this.accountId = accountId;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getIsLocked() {
            return isLocked;
        }

        public void setIsLocked(Integer isLocked) {
            this.isLocked = isLocked;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Integer createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Integer updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getBoxtypeLbl() {
            return boxtypeLbl;
        }

        public void setBoxtypeLbl(String boxtypeLbl) {
            this.boxtypeLbl = boxtypeLbl;
        }

        public String getStatusLbl() {
            return statusLbl;
        }

        public void setStatusLbl(String statusLbl) {
            this.statusLbl = statusLbl;
        }

        public String getCreatedByLbl() {
            return createdByLbl;
        }

        public void setCreatedByLbl(String createdByLbl) {
            this.createdByLbl = createdByLbl;
        }

        public String getBrandLbl() {
            return brandLbl;
        }

        public void setBrandLbl(String brandLbl) {
            this.brandLbl = brandLbl;
        }

        public String getCasLbl() {
            return casLbl;
        }

        public void setCasLbl(String casLbl) {
            this.casLbl = casLbl;
        }

        public String getIsEmbededLbl() {
            return isEmbededLbl;
        }

        public void setIsEmbededLbl(String isEmbededLbl) {
            this.isEmbededLbl = isEmbededLbl;
        }

        public String getOperatorLbl() {
            return operatorLbl;
        }

        public void setOperatorLbl(String operatorLbl) {
            this.operatorLbl = operatorLbl;
        }

    }
}
