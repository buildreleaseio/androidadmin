package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RecentlyExpiredData {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("bouque_id")
        @Expose
        private Integer bouqueId;
        @SerializedName("bouque_type")
        @Expose
        private Integer bouqueType;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("account_id")
        @Expose
        private String accountId;
        @SerializedName("subscriber_id")
        @Expose
        private String subscriberId;
        @SerializedName("activation_date")
        @Expose
        private String activationDate;
        @SerializedName("deactivation_date")
        @Expose
        private String deactivationDate;
        @SerializedName("left")
        @Expose
        private String left;
        @SerializedName("noofmonths")
        @Expose
        private String noofmonths;
        @SerializedName("refundAble")
        @Expose
        private RefundAble refundAble;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("cas_status")
        @Expose
        private Integer casStatus;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("tax")
        @Expose
        private String tax;
        @SerializedName("refund_amount")
        @Expose
        private Object refundAmount;
        @SerializedName("refund_tax")
        @Expose
        private Object refundTax;
        @SerializedName("refund_date")
        @Expose
        private Object refundDate;
        @SerializedName("remark")
        @Expose
        private String remark;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_by")
        @Expose
        private Integer createdBy;
        @SerializedName("updated_by")
        @Expose
        private Integer updatedBy;
        @SerializedName("created_by_lbl")
        @Expose
        private String createdByLbl;
        @SerializedName("smartcardno_lbl")
        @Expose
        private String smartcardnoLbl;
        @SerializedName("stbno_lbl")
        @Expose
        private String stbnoLbl;
        @SerializedName("customer_id_lbl")
        @Expose
        private String customerIdLbl;
        @SerializedName("customer_name_lbl")
        @Expose
        private String customerNameLbl;
        @SerializedName("operator_lbl")
        @Expose
        private String operatorLbl;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Integer getBouqueId() {
            return bouqueId;
        }

        public void setBouqueId(Integer bouqueId) {
            this.bouqueId = bouqueId;
        }

        public Integer getBouqueType() {
            return bouqueType;
        }

        public void setBouqueType(Integer bouqueType) {
            this.bouqueType = bouqueType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public String getSubscriberId() {
            return subscriberId;
        }

        public void setSubscriberId(String subscriberId) {
            this.subscriberId = subscriberId;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public String getLeft() {
            return left;
        }

        public void setLeft(String left) {
            this.left = left;
        }

        public String getNoofmonths() {
            return noofmonths;
        }

        public void setNoofmonths(String noofmonths) {
            this.noofmonths = noofmonths;
        }

        public RefundAble getRefundAble() {
            return refundAble;
        }

        public void setRefundAble(RefundAble refundAble) {
            this.refundAble = refundAble;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getCasStatus() {
            return casStatus;
        }

        public void setCasStatus(Integer casStatus) {
            this.casStatus = casStatus;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public Object getRefundAmount() {
            return refundAmount;
        }

        public void setRefundAmount(Object refundAmount) {
            this.refundAmount = refundAmount;
        }

        public Object getRefundTax() {
            return refundTax;
        }

        public void setRefundTax(Object refundTax) {
            this.refundTax = refundTax;
        }

        public Object getRefundDate() {
            return refundDate;
        }

        public void setRefundDate(Object refundDate) {
            this.refundDate = refundDate;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Integer createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Integer updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getCreatedByLbl() {
            return createdByLbl;
        }

        public void setCreatedByLbl(String createdByLbl) {
            this.createdByLbl = createdByLbl;
        }

        public String getSmartcardnoLbl() {
            return smartcardnoLbl;
        }

        public void setSmartcardnoLbl(String smartcardnoLbl) {
            this.smartcardnoLbl = smartcardnoLbl;
        }

        public String getStbnoLbl() {
            return stbnoLbl;
        }

        public void setStbnoLbl(String stbnoLbl) {
            this.stbnoLbl = stbnoLbl;
        }

        public String getCustomerIdLbl() {
            return customerIdLbl;
        }

        public void setCustomerIdLbl(String customerIdLbl) {
            this.customerIdLbl = customerIdLbl;
        }

        public String getCustomerNameLbl() {
            return customerNameLbl;
        }

        public void setCustomerNameLbl(String customerNameLbl) {
            this.customerNameLbl = customerNameLbl;
        }

        public String getOperatorLbl() {
            return operatorLbl;
        }

        public void setOperatorLbl(String operatorLbl) {
            this.operatorLbl = operatorLbl;
        }

    }
    public static class RefundAble {

        @SerializedName("amt")
        @Expose
        private Integer amt;
        @SerializedName("tax")
        @Expose
        private Integer tax;

        public Integer getAmt() {
            return amt;
        }

        public void setAmt(Integer amt) {
            this.amt = amt;
        }

        public Integer getTax() {
            return tax;
        }

        public void setTax(Integer tax) {
            this.tax = tax;
        }

    }
}
