package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RechargeResponse {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }



    public class Data {


        @SerializedName("rp0")
        @Expose
        private List<Rp0> rp0 = null;
        @SerializedName("rpsimilarity")
        @Expose
        private List<Rp0> rpsimilarity = null;


        public List<Rp0> getRp0() {
            return rp0;
        }

        public void setRp0(List<Rp0> rp0) {
            this.rp0 = rp0;
        }

        public List<Rp0> getRpsimilarity() {
            return rpsimilarity;
        }

        public void setRpsimilarity(List<Rp0> rpsimilarity) {
            this.rpsimilarity = rpsimilarity;
        }


        public class Rp0 {

            @SerializedName("id")
            @Expose
            private Integer id;
            @SerializedName("months")
            @Expose
            private Integer months;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("days")
            @Expose
            private String days;
            @SerializedName("price")
            @Expose
            private Double price;
            @SerializedName("rental")
            @Expose
            private Double rental;
            @SerializedName("tax")
            @Expose
            private Double tax;
            @SerializedName("amount")
            @Expose
            private Double amount;
            @SerializedName("activation_date")
            @Expose
            private String activationDate;
            @SerializedName("expiry")
            @Expose
            private String expiry;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getMonths() {
                return months;
            }

            public void setMonths(Integer months) {
                this.months = months;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDays() {
                return days;
            }

            public void setDays(String days) {
                this.days = days;
            }

            public Double getPrice() {
                return price;
            }

            public void setPrice(Double price) {
                this.price = price;
            }

            public Double getRental() {
                return rental;
            }

            public void setRental(Double rental) {
                this.rental = rental;
            }

            public Double getTax() {
                return tax;
            }

            public void setTax(Double tax) {
                this.tax = tax;
            }

            public Double getAmount() {
                return amount;
            }

            public void setAmount(Double amount) {
                this.amount = amount;
            }

            public String getActivationDate() {
                return activationDate;
            }

            public void setActivationDate(String activationDate) {
                this.activationDate = activationDate;
            }

            public String getExpiry() {
                return expiry;
            }

            public void setExpiry(String expiry) {
                this.expiry = expiry;
            }

        }
    }
}
