package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RemoveBouquetsResponse {


    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {



        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("bouque_id")
        @Expose
        private Integer bouqueId;
        @SerializedName("bouque_type")
        @Expose
        private Integer bouqueType;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("account_id")
        @Expose
        private String accountId;
        @SerializedName("subscriber_id")
        @Expose
        private String subscriberId;
        @SerializedName("activation_date")
        @Expose
        private String activationDate;
        @SerializedName("deactivation_date")
        @Expose
        private String deactivationDate;
        @SerializedName("left")
        @Expose
        private String left;
        @SerializedName("noofmonths")
        @Expose
        private String noofmonths;
        @SerializedName("refundAble")
        @Expose
        private RefundAble refundAble;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("cas_status")
        @Expose
        private Integer casStatus;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("tax")
        @Expose
        private String tax;
        @SerializedName("refund_amount")
        @Expose
        private String refundAmount;
        @SerializedName("refund_tax")
        @Expose
        private String refundTax;
        @SerializedName("refund_date")
        @Expose
        private String refundDate;
        @SerializedName("remark")
        @Expose
        private String remark;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_by")
        @Expose
        private Integer createdBy;
        @SerializedName("updated_by")
        @Expose
        private Integer updatedBy;
        @SerializedName("status_lbl")
        @Expose
        private String statusLbl;
        @SerializedName("boxtype_lbl")
        @Expose
        private String boxtypeLbl;
        @SerializedName("type_lbl")
        @Expose
        private String typeLbl;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Integer getBouqueId() {
            return bouqueId;
        }

        public void setBouqueId(Integer bouqueId) {
            this.bouqueId = bouqueId;
        }

        public Integer getBouqueType() {
            return bouqueType;
        }

        public void setBouqueType(Integer bouqueType) {
            this.bouqueType = bouqueType;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public String getSubscriberId() {
            return subscriberId;
        }

        public void setSubscriberId(String subscriberId) {
            this.subscriberId = subscriberId;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public String getLeft() {
            return left;
        }

        public void setLeft(String left) {
            this.left = left;
        }

        public String getNoofmonths() {
            return noofmonths;
        }

        public void setNoofmonths(String noofmonths) {
            this.noofmonths = noofmonths;
        }

        public RefundAble getRefundAble() {
            return refundAble;
        }

        public void setRefundAble(RefundAble refundAble) {
            this.refundAble = refundAble;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getCasStatus() {
            return casStatus;
        }

        public void setCasStatus(Integer casStatus) {
            this.casStatus = casStatus;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getRefundAmount() {
            return refundAmount;
        }

        public void setRefundAmount(String refundAmount) {
            this.refundAmount = refundAmount;
        }

        public String getRefundTax() {
            return refundTax;
        }

        public void setRefundTax(String refundTax) {
            this.refundTax = refundTax;
        }

        public String getRefundDate() {
            return refundDate;
        }

        public void setRefundDate(String refundDate) {
            this.refundDate = refundDate;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Integer createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Integer updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getStatusLbl() {
            return statusLbl;
        }

        public void setStatusLbl(String statusLbl) {
            this.statusLbl = statusLbl;
        }

        public String getBoxtypeLbl() {
            return boxtypeLbl;
        }

        public void setBoxtypeLbl(String boxtypeLbl) {
            this.boxtypeLbl = boxtypeLbl;
        }

        public String getTypeLbl() {
            return typeLbl;
        }

        public void setTypeLbl(String typeLbl) {
            this.typeLbl = typeLbl;
        }

    }


    public class RefundAble {

        @SerializedName("amt")
        @Expose
        private Double amt;
        @SerializedName("tax")
        @Expose
        private Double tax;

        public Double getAmt() {
            return amt;
        }

        public void setAmt(Double amt) {
            this.amt = amt;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

    }

}
