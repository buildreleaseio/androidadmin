package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RemovebouqePayload {

    @SerializedName("account_ids")
    @Expose
    private String accountId;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("subs_tran_ids")
    @Expose
    private String subsTranIds;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSubsTranIds() {
        return subsTranIds;
    }

    public void setSubsTranIds(String subsTranIds) {
        this.subsTranIds = subsTranIds;
    }

}
