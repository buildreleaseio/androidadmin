package io.buildrelease.adminjpr.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SavePayload {

    @SerializedName("account_id")
    @Expose
    private String accountId;
    @SerializedName("bouque_ids")
    @Expose
    private List<Integer> bouqueIds;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("rperiod_id")
    @Expose
    private String rperiodId;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public List<Integer> getBouqueIds() {
        return bouqueIds;
    }

    public void setBouqueIds(List<Integer> bouqueIds) {
        this.bouqueIds = bouqueIds;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRperiodId() {
        return rperiodId;
    }

    public void setRperiodId(String rperiodId) {
        this.rperiodId = rperiodId;
    }

}
