package io.buildrelease.adminjpr.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SavePayloadForBulkAssignment {


    @SerializedName("account_ids")
    @Expose
    private List<Integer> accountIds;
    @SerializedName("bouque_ids")
    @Expose
    private List<Integer> newbouqueIds;

    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("rperiod_id")
    @Expose
    private String rperiodId;


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRperiodId() {
        return rperiodId;
    }

    public void setRperiodId(String rperiodId) {
        this.rperiodId = rperiodId;
    }

    public List<Integer> getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(List<Integer> accountIds) {
        this.accountIds = accountIds;
    }

    public List<Integer> getNewbouqueIds() {
        return newbouqueIds;
    }

    public void setNewbouqueIds(List<Integer> newbouqueIds) {
        this.newbouqueIds = newbouqueIds;
    }
}
