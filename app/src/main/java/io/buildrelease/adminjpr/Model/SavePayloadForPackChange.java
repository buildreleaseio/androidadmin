package io.buildrelease.adminjpr.Model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SavePayloadForPackChange {


    @SerializedName("account_ids")
    @Expose
    private String accountIds;
    @SerializedName("new_bouque_id")
    @Expose
    private int newbouqueIds;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("rperiod_id")
    @Expose
    private String rperiodId;


    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRperiodId() {
        return rperiodId;
    }

    public void setRperiodId(String rperiodId) {
        this.rperiodId = rperiodId;
    }

    public String getAccountIds() {
        return accountIds;
    }

    public void setAccountIds(String accountIds) {
        this.accountIds = accountIds;
    }

    public int getNewbouqueIds() {
        return newbouqueIds;
    }

    public void setNewbouqueIds(int newbouqueIds) {
        this.newbouqueIds = newbouqueIds;
    }
}
