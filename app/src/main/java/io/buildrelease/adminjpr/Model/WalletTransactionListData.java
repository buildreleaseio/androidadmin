package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WalletTransactionListData {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("subs_tran_id")
        @Expose
        private String subsTranId;
        @SerializedName("account_id")
        @Expose
        private String accountId;
        @SerializedName("subscriber_id")
        @Expose
        private String subscriberId;
        @SerializedName("operator_id")
        @Expose
        private Integer operatorId;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("type")
        @Expose
        private Integer type;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_by")
        @Expose
        private Integer createdBy;
        @SerializedName("updated_by")
        @Expose
        private Integer updatedBy;
        @SerializedName("scheme_id")
        @Expose
        private Object schemeId;
        @SerializedName("tax")
        @Expose
        private String tax;
        @SerializedName("created_by_lbl")
        @Expose
        private String createdByLbl;
        @SerializedName("operator_lbl")
        @Expose
        private String operatorLbl;
        @SerializedName("branch_lbl")
        @Expose
        private BranchLbl branchLbl;
        @SerializedName("distributor_lbl")
        @Expose
        private String distributorLbl;
        @SerializedName("smartcardno_lbl")
        @Expose
        private String smartcardnoLbl;
        @SerializedName("bouque_lbl")
        @Expose
        private String bouqueLbl;
        @SerializedName("activation_lbl")
        @Expose
        private String activationLbl;
        @SerializedName("decativation_lbl")
        @Expose
        private String decativationLbl;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSubsTranId() {
            return subsTranId;
        }

        public void setSubsTranId(String subsTranId) {
            this.subsTranId = subsTranId;
        }

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public String getSubscriberId() {
            return subscriberId;
        }

        public void setSubscriberId(String subscriberId) {
            this.subscriberId = subscriberId;
        }

        public Integer getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(Integer operatorId) {
            this.operatorId = operatorId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Integer getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Integer createdBy) {
            this.createdBy = createdBy;
        }

        public Integer getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Integer updatedBy) {
            this.updatedBy = updatedBy;
        }

        public Object getSchemeId() {
            return schemeId;
        }

        public void setSchemeId(Object schemeId) {
            this.schemeId = schemeId;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getCreatedByLbl() {
            return createdByLbl;
        }

        public void setCreatedByLbl(String createdByLbl) {
            this.createdByLbl = createdByLbl;
        }

        public String getOperatorLbl() {
            return operatorLbl;
        }

        public void setOperatorLbl(String operatorLbl) {
            this.operatorLbl = operatorLbl;
        }

        public BranchLbl getBranchLbl() {
            return branchLbl;
        }

        public void setBranchLbl(BranchLbl branchLbl) {
            this.branchLbl = branchLbl;
        }

        public String getDistributorLbl() {
            return distributorLbl;
        }

        public void setDistributorLbl(String distributorLbl) {
            this.distributorLbl = distributorLbl;
        }

        public String getSmartcardnoLbl() {
            return smartcardnoLbl;
        }

        public void setSmartcardnoLbl(String smartcardnoLbl) {
            this.smartcardnoLbl = smartcardnoLbl;
        }

        public String getBouqueLbl() {
            return bouqueLbl;
        }

        public void setBouqueLbl(String bouqueLbl) {
            this.bouqueLbl = bouqueLbl;
        }

        public String getActivationLbl() {
            return activationLbl;
        }

        public void setActivationLbl(String activationLbl) {
            this.activationLbl = activationLbl;
        }

        public String getDecativationLbl() {
            return decativationLbl;
        }

        public void setDecativationLbl(String decativationLbl) {
            this.decativationLbl = decativationLbl;
        }

    }

    public static class BranchLbl {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("contact_person")
        @Expose
        private Object contactPerson;
        @SerializedName("email")
        @Expose
        private Object email;
        @SerializedName("mobile_no")
        @Expose
        private Object mobileNo;
        @SerializedName("phone_no")
        @Expose
        private Object phoneNo;
        @SerializedName("fax_no")
        @Expose
        private Object faxNo;
        @SerializedName("addr")
        @Expose
        private Object addr;
        @SerializedName("status")
        @Expose
        private Object status;
        @SerializedName("gstno")
        @Expose
        private Object gstno;
        @SerializedName("panno")
        @Expose
        private Object panno;
        @SerializedName("status_lbl")
        @Expose
        private String statusLbl;
        @SerializedName("type")
        @Expose
        private Integer type;
        @SerializedName("type_lbl")
        @Expose
        private String typeLbl;
        @SerializedName("mso_id")
        @Expose
        private Integer msoId;
        @SerializedName("branch_id")
        @Expose
        private Object branchId;
        @SerializedName("distributor_id")
        @Expose
        private Object distributorId;
        @SerializedName("created_at")
        @Expose
        private Object createdAt;
        @SerializedName("updated_at")
        @Expose
        private Object updatedAt;
        @SerializedName("created_by")
        @Expose
        private Object createdBy;
        @SerializedName("updated_by")
        @Expose
        private Object updatedBy;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Object getContactPerson() {
            return contactPerson;
        }

        public void setContactPerson(Object contactPerson) {
            this.contactPerson = contactPerson;
        }

        public Object getEmail() {
            return email;
        }

        public void setEmail(Object email) {
            this.email = email;
        }

        public Object getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(Object mobileNo) {
            this.mobileNo = mobileNo;
        }

        public Object getPhoneNo() {
            return phoneNo;
        }

        public void setPhoneNo(Object phoneNo) {
            this.phoneNo = phoneNo;
        }

        public Object getFaxNo() {
            return faxNo;
        }

        public void setFaxNo(Object faxNo) {
            this.faxNo = faxNo;
        }

        public Object getAddr() {
            return addr;
        }

        public void setAddr(Object addr) {
            this.addr = addr;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public Object getGstno() {
            return gstno;
        }

        public void setGstno(Object gstno) {
            this.gstno = gstno;
        }

        public Object getPanno() {
            return panno;
        }

        public void setPanno(Object panno) {
            this.panno = panno;
        }

        public String getStatusLbl() {
            return statusLbl;
        }

        public void setStatusLbl(String statusLbl) {
            this.statusLbl = statusLbl;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getTypeLbl() {
            return typeLbl;
        }

        public void setTypeLbl(String typeLbl) {
            this.typeLbl = typeLbl;
        }

        public Integer getMsoId() {
            return msoId;
        }

        public void setMsoId(Integer msoId) {
            this.msoId = msoId;
        }

        public Object getBranchId() {
            return branchId;
        }

        public void setBranchId(Object branchId) {
            this.branchId = branchId;
        }

        public Object getDistributorId() {
            return distributorId;
        }

        public void setDistributorId(Object distributorId) {
            this.distributorId = distributorId;
        }

        public Object getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(Object createdAt) {
            this.createdAt = createdAt;
        }

        public Object getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(Object updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Object getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Object createdBy) {
            this.createdBy = createdBy;
        }

        public Object getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Object updatedBy) {
            this.updatedBy = updatedBy;
        }

    }



}
