package io.buildrelease.adminjpr.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class expird7daysbouquetswiseresponse {

    @SerializedName("success")
    @Expose
    private boolean success;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public expird7daysbouquetswiseresponse() {
    }


    public expird7daysbouquetswiseresponse(boolean success, int status, List<Datum> data) {
        super();
        this.success = success;
        this.status = status;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private String id;

        public String getBouque_lbl() {
            return bouque_lbl;
        }

        public void setBouque_lbl(String bouque_lbl) {
            this.bouque_lbl = bouque_lbl;
        }

        @SerializedName("bouque_lbl")
        @Expose
        private String bouque_lbl;
        @SerializedName("subscriber_id")
        @Expose
        private String subscriberId;
        @SerializedName("customer_id")
        @Expose
        private String customerId;
        @SerializedName("operator_id")
        @Expose
        private int operatorId;
        @SerializedName("location_id")
        @Expose
        private int locationId;
        @SerializedName("sublocation_id")
        @Expose
        private int sublocationId;
        @SerializedName("pairing_id")
        @Expose
        private int pairingId;
        @SerializedName("daysLeft")
        @Expose
        private String daysLeft;
        @SerializedName("isLocked")
        @Expose
        private boolean isLocked;
        @SerializedName("isRefreshAble")
        @Expose
        private boolean isRefreshAble;
        @SerializedName("smartcardno")
        @Expose
        private String smartcardno;
        @SerializedName("stbno")
        @Expose
        private String stbno;
        @SerializedName("lockTimeLeft")
        @Expose
        private int lockTimeLeft;
        @SerializedName("stb_id")
        @Expose
        private int stbId;
        @SerializedName("sc_id")
        @Expose
        private int scId;
        @SerializedName("cas_id")
        @Expose
        private int casId;
        @SerializedName("isHD")
        @Expose
        private Object isHD;
        @SerializedName("is_embeded")
        @Expose
        private Object isEmbeded;
        @SerializedName("stbbrand_id")
        @Expose
        private int stbbrandId;
        @SerializedName("scheme_id")
        @Expose
        private int schemeId;
        @SerializedName("bouque_id")
        @Expose
        private int bouqueId;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("activation_date")
        @Expose
        private String activationDate;
        @SerializedName("deactivation_date")
        @Expose
        private String deactivationDate;
        @SerializedName("outstanding")
        @Expose
        private Object outstanding;
        @SerializedName("status")
        @Expose
        private int status;
        @SerializedName("remark")
        @Expose
        private String remark;
        @SerializedName("mobile_no")
        @Expose
        private String mobileNo;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("operator_name_lbl")
        @Expose
        private String operatorNameLbl;
        @SerializedName("operator_code_lbl")
        @Expose
        private String operatorCodeLbl;
        @SerializedName("cas_status")
        @Expose
        private int casStatus;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("created_by")
        @Expose
        private int createdBy;
        @SerializedName("updated_by")
        @Expose
        private int updatedBy;
        @SerializedName("boxtype_lbl")
        @Expose
        private String boxtypeLbl;
        @SerializedName("status_lbl")
        @Expose
        private String statusLbl;
        @SerializedName("created_by_lbl")
        @Expose
        private String createdByLbl;
        @SerializedName("operator_lbl")
        @Expose
        private String operatorLbl;
        @SerializedName("location_lbl")
        @Expose
        private String locationLbl;
        @SerializedName("sublocation_lbl")
        @Expose
        private String sublocationLbl;
        @SerializedName("cas_lbl")
        @Expose
        private String casLbl;
        @SerializedName("brand_lbl")
        @Expose
        private String brandLbl;

//        @SerializedName("bouqueD")
//        @Expose
//        private BouqueD bouqueD;


        @SerializedName("customer_name")
        @Expose
        private String customerName;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSubscriberId() {
            return subscriberId;
        }

        public void setSubscriberId(String subscriberId) {
            this.subscriberId = subscriberId;
        }

        public String getCustomerId() {
            return customerId;
        }

        public void setCustomerId(String customerId) {
            this.customerId = customerId;
        }

        public int getOperatorId() {
            return operatorId;
        }

        public void setOperatorId(int operatorId) {
            this.operatorId = operatorId;
        }

        public int getLocationId() {
            return locationId;
        }

        public void setLocationId(int locationId) {
            this.locationId = locationId;
        }

        public int getSublocationId() {
            return sublocationId;
        }

        public void setSublocationId(int sublocationId) {
            this.sublocationId = sublocationId;
        }

        public int getPairingId() {
            return pairingId;
        }

        public void setPairingId(int pairingId) {
            this.pairingId = pairingId;
        }

        public String getDaysLeft() {
            return daysLeft;
        }

        public void setDaysLeft(String daysLeft) {
            this.daysLeft = daysLeft;
        }

        public boolean isLocked() {
            return isLocked;
        }

        public void setLocked(boolean locked) {
            isLocked = locked;
        }

        public boolean isRefreshAble() {
            return isRefreshAble;
        }

        public void setRefreshAble(boolean refreshAble) {
            isRefreshAble = refreshAble;
        }

        public String getSmartcardno() {
            return smartcardno;
        }

        public void setSmartcardno(String smartcardno) {
            this.smartcardno = smartcardno;
        }

        public String getStbno() {
            return stbno;
        }

        public void setStbno(String stbno) {
            this.stbno = stbno;
        }

        public int getLockTimeLeft() {
            return lockTimeLeft;
        }

        public void setLockTimeLeft(int lockTimeLeft) {
            this.lockTimeLeft = lockTimeLeft;
        }

        public int getStbId() {
            return stbId;
        }

        public void setStbId(int stbId) {
            this.stbId = stbId;
        }

        public int getScId() {
            return scId;
        }

        public void setScId(int scId) {
            this.scId = scId;
        }

        public int getCasId() {
            return casId;
        }

        public void setCasId(int casId) {
            this.casId = casId;
        }

        public Object getIsHD() {
            return isHD;
        }

        public void setIsHD(Object isHD) {
            this.isHD = isHD;
        }

        public Object getIsEmbeded() {
            return isEmbeded;
        }

        public void setIsEmbeded(Object isEmbeded) {
            this.isEmbeded = isEmbeded;
        }

        public int getStbbrandId() {
            return stbbrandId;
        }

        public void setStbbrandId(int stbbrandId) {
            this.stbbrandId = stbbrandId;
        }

        public int getSchemeId() {
            return schemeId;
        }

        public void setSchemeId(int schemeId) {
            this.schemeId = schemeId;
        }

        public int getBouqueId() {
            return bouqueId;
        }

        public void setBouqueId(int bouqueId) {
            this.bouqueId = bouqueId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getActivationDate() {
            return activationDate;
        }

        public void setActivationDate(String activationDate) {
            this.activationDate = activationDate;
        }

        public String getDeactivationDate() {
            return deactivationDate;
        }

        public void setDeactivationDate(String deactivationDate) {
            this.deactivationDate = deactivationDate;
        }

        public Object getOutstanding() {
            return outstanding;
        }

        public void setOutstanding(Object outstanding) {
            this.outstanding = outstanding;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getOperatorNameLbl() {
            return operatorNameLbl;
        }

        public void setOperatorNameLbl(String operatorNameLbl) {
            this.operatorNameLbl = operatorNameLbl;
        }

        public String getOperatorCodeLbl() {
            return operatorCodeLbl;
        }

        public void setOperatorCodeLbl(String operatorCodeLbl) {
            this.operatorCodeLbl = operatorCodeLbl;
        }

        public int getCasStatus() {
            return casStatus;
        }

        public void setCasStatus(int casStatus) {
            this.casStatus = casStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getBoxtypeLbl() {
            return boxtypeLbl;
        }

        public void setBoxtypeLbl(String boxtypeLbl) {
            this.boxtypeLbl = boxtypeLbl;
        }

        public String getStatusLbl() {
            return statusLbl;
        }

        public void setStatusLbl(String statusLbl) {
            this.statusLbl = statusLbl;
        }

        public String getCreatedByLbl() {
            return createdByLbl;
        }

        public void setCreatedByLbl(String createdByLbl) {
            this.createdByLbl = createdByLbl;
        }

        public String getOperatorLbl() {
            return operatorLbl;
        }

        public void setOperatorLbl(String operatorLbl) {
            this.operatorLbl = operatorLbl;
        }

        public String getLocationLbl() {
            return locationLbl;
        }

        public void setLocationLbl(String locationLbl) {
            this.locationLbl = locationLbl;
        }

        public String getSublocationLbl() {
            return sublocationLbl;
        }

        public void setSublocationLbl(String sublocationLbl) {
            this.sublocationLbl = sublocationLbl;
        }

        public String getCasLbl() {
            return casLbl;
        }

        public void setCasLbl(String casLbl) {
            this.casLbl = casLbl;
        }

        public String getBrandLbl() {
            return brandLbl;
        }

        public void setBrandLbl(String brandLbl) {
            this.brandLbl = brandLbl;
        }

//        public BouqueD getBouqueD() {
//            return bouqueD;
//        }
//
//        public void setBouqueD(BouqueD bouqueD) {
//            this.bouqueD = bouqueD;
//        }

        public String getCustomerName() {
            return customerName;
        }

        public void setCustomerName(String customerName) {
            this.customerName = customerName;
        }
    }
//    public class BouqueD {
//
//        @SerializedName("bouque_id")
//        @Expose
//        private int bouqueId;
//        @SerializedName("account_id")
//        @Expose
//        private String accountId;
//        @SerializedName("bouque_type")
//        @Expose
//        private int bouqueType;
//        @SerializedName("activation_date")
//        @Expose
//        private String activationDate;
//        @SerializedName("deactivation_date")
//        @Expose
//        private String deactivationDate;
//        @SerializedName("amount")
//        @Expose
//        private String amount;
//        @SerializedName("tax")
//        @Expose
//        private String tax;
//        @SerializedName("left")
//        @Expose
//        private String left;
//        @SerializedName("bouque_name")
//        @Expose
//        private String bouqueName;
//        @SerializedName("refundAble")
//        @Expose
//        private RefundAble refundAble;
//
//        public BouqueD() {
//        }
//
//
//        public BouqueD(int bouqueId, String accountId, int bouqueType, String activationDate, String deactivationDate, String amount, String tax, String left, String bouqueName, RefundAble refundAble) {
//            super();
//            this.bouqueId = bouqueId;
//            this.accountId = accountId;
//            this.bouqueType = bouqueType;
//            this.activationDate = activationDate;
//            this.deactivationDate = deactivationDate;
//            this.amount = amount;
//            this.tax = tax;
//            this.left = left;
//            this.bouqueName = bouqueName;
//            this.refundAble = refundAble;
//        }
//
//        public int getBouqueId() {
//            return bouqueId;
//        }
//
//        public void setBouqueId(int bouqueId) {
//            this.bouqueId = bouqueId;
//        }
//
//        public String getAccountId() {
//            return accountId;
//        }
//
//        public void setAccountId(String accountId) {
//            this.accountId = accountId;
//        }
//
//        public int getBouqueType() {
//            return bouqueType;
//        }
//
//        public void setBouqueType(int bouqueType) {
//            this.bouqueType = bouqueType;
//        }
//
//        public String getActivationDate() {
//            return activationDate;
//        }
//
//        public void setActivationDate(String activationDate) {
//            this.activationDate = activationDate;
//        }
//
//        public String getDeactivationDate() {
//            return deactivationDate;
//        }
//
//        public void setDeactivationDate(String deactivationDate) {
//            this.deactivationDate = deactivationDate;
//        }
//
//        public String getAmount() {
//            return amount;
//        }
//
//        public void setAmount(String amount) {
//            this.amount = amount;
//        }
//
//        public String getTax() {
//            return tax;
//        }
//
//        public void setTax(String tax) {
//            this.tax = tax;
//        }
//
//        public String getLeft() {
//            return left;
//        }
//
//        public void setLeft(String left) {
//            this.left = left;
//        }
//
//        public String getBouqueName() {
//            return bouqueName;
//        }
//
//        public void setBouqueName(String bouqueName) {
//            this.bouqueName = bouqueName;
//        }
//
//        public RefundAble getRefundAble() {
//            return refundAble;
//        }
//
//        public void setRefundAble(RefundAble refundAble) {
//            this.refundAble = refundAble;
//        }
//    }




    public class RefundAble {

        @SerializedName("amt")
        @Expose
        private double amt;
        @SerializedName("tax")
        @Expose
        private double tax;

        public RefundAble() {
        }


        public RefundAble(double amt, double tax) {
            super();
            this.amt = amt;
            this.tax = tax;
        }

        public double getAmt() {
            return amt;
        }

        public void setAmt(double amt) {
            this.amt = amt;
        }

        public double getTax() {
            return tax;
        }

        public void setTax(double tax) {
            this.tax = tax;
        }

    }
}
