package io.buildrelease.adminjpr.Network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;

   public  static Retrofit getClient() {


       //Create a new Interceptor.
//       Interceptor headerAuthorizationInterceptor = new Interceptor() {
//           @Override
//           public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
//               okhttp3.Request request = chain.request();
//               Headers headers = request.headers().newBuilder().add("Authorization", UtilPref.getAuth()).build();
//               request = request.newBuilder().headers(headers).build();
//               return chain.proceed(request);
//           }
//       };

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                //timeout
                .readTimeout(15, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build();




        retrofit = new Retrofit.Builder()
                .baseUrl(APIURL.getBASE_URL())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();



        return retrofit;
    }

}
