package io.buildrelease.adminjpr.Network;

import com.google.gson.JsonObject;

import io.buildrelease.adminjpr.Model.ActionPayload;
import io.buildrelease.adminjpr.Model.BulkAccountRenewalPayload;
import io.buildrelease.adminjpr.Model.BulkBouqoutesRenewalPayload;
import io.buildrelease.adminjpr.Model.BulkRemoveBouqetPayload;
import io.buildrelease.adminjpr.Model.ComplainPayload;
import io.buildrelease.adminjpr.Model.CreateComplainPayload;
import io.buildrelease.adminjpr.Model.EditProfielPayload;
import io.buildrelease.adminjpr.Model.RechargePayload;
import io.buildrelease.adminjpr.Model.Remarks;
import io.buildrelease.adminjpr.Model.RemovebouqePayload;
import io.buildrelease.adminjpr.Model.SavePayload;
import io.buildrelease.adminjpr.Model.SavePayloadForBulkAssignment;
import io.buildrelease.adminjpr.Model.SavePayloadForBulkPackChange;
import io.buildrelease.adminjpr.Model.SavePayloadForPackChange;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public  interface
APIInterface {



//    @Headers({"Content-Type:application/json","authkey:xqibzknznwb29de15s44"})
    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @POST("v1/user/login")
    Call<JsonObject> login(@Body JsonObject jsonObject);


    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/user/me")
    Call<JsonObject> getProfile(@Header("Authorization") String token);


    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/pairing/dashboard")
    Call<JsonObject> getInvertoryData(@Header("Authorization") String token);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/subscriber/search/adminapp")
    Call<JsonObject> getSearchCriteriaData(@Header("Authorization") String token);


    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/pairing-allotment?expand=created_by_lbl,status_lbl,cas_lbl,brand_lbl,boxtype_lbl,is_embeded_lbl,operator_lbl&filter[status]=1&filter[account_id]=0&page=1&per-page=50")
    Call<JsonObject> getPairingListData(@Header("Authorization") String token);


//    TODO : PAGINATION
    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/operator-transaction?expand=operator_lbl,branch_lbl,distributor_lbl,customerid_lbl,smartcardno_lbl,bouque_lbl,activation_lbl,decativation_lbl,created_by_lbl,status_lbl&page=1&per-page=50")
    Call<JsonObject> getWalletTransactionListData(@Header("Authorization") String token);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/operator?expand=balance,distributor,status&page=1&per-page=200")
    Call<JsonObject> getWalletListData(@Header("Authorization") String token);


    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/account?expand=cas_lbl,brand_lbl,boxtype_lbl,operator_lbl,branch_lbl,location_lbl,sublocation_lbl,created_by_lbl,status_lbl,mobile_no,bouqueD,bouque_lbl,customer_name&filter[status]=1")
    Call<JsonObject> getActiveAccountList(@Header("Authorization") String token,@Query("page") int page,@Query("per-page") int perPage);
//    v1/account?expand=cas_lbl,brand_lbl,boxtype_lbl,operator_lbl,branch_lbl,location_lbl,sublocation_lbl,created_by_lbl,status_lbl,mobile_no,bouqueD,bouque_lbl,customer_name&filter[status]=1&page=1&per-page=50

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/account?expand=cas_lbl,brand_lbl,boxtype_lbl,operator_lbl,location_lbl,sublocation_lbl,created_by_lbl,status_lbl,mobile_no,bouqueD,bouque_lbl,customer_name&filter[status]=0&page=1&per-page=50")
    Call<JsonObject> getInactiveAccountList(@Header("Authorization") String token,@Query("page") int page,@Query("per-page") int perPage);


    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/account-bouque?filter[expired]=1&expand=stbno_lbl,smartcardno_lbl,customer_id_lbl,customer_name_lbl,operator_lbl,created_by_lbl,mobile_no_lbl&page=1&per-page=50")
    Call<JsonObject> getExpiredtList(@Header("Authorization") String token,@Query("page") int page,@Query("per-page") int perPage);


    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/account-bouque?expand=stbno_lbl,smartcardno_lbl,customer_id_lbl,customer_name_lbl,operator_lbl,created_by_lbl ,mobile_no_lb,brand_lbl&page=1&per-page=50")
//    filter[FRM_deactivation_date]=2018-12-31 00:00:00&filter[TO_deactivation_date]=2018-12-31 23:59:59&
    Call<JsonObject> getExpiredTodayAccountList(@Header("Authorization") String token,@Query("filter[FRM_deactivation_date]") String fromDateTime,@Query("filter[TO_deactivation_date]") String toDateTime);

//    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
//    @GET("v1/account?expand=status_lbl,boxtype_lbl,brand_lbl,cas_lbl,operator_lbl,location_lbl,sublocation_lbl,customer_name,created_by_lbl,brand_lbl&filter[status]=1&filter[cas_status]=1&filter[notlocked]=1&sort=deactivation_date&page=1&per-page=50")
//    Call<JsonObject> getSuspendedAccountList(@Header("Authorization") String token);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/account?expand=status_lbl,boxtype_lbl,brand_lbl,cas_lbl,operator_lbl,location_lbl,sublocation_lbl,customer_name,created_by_lbl,bouqueD&filter[status]=0&filter[cas_status]=1&filter[notlocked]=1&sort=deactivation_date&page=1&per-page=50")
    Call<JsonObject> getSuspendedAccountList(@Header("Authorization") String token,@Query("page") int page,@Query("per-page") int perPage);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/complaint?filter[status]=2&expand=priority_lbl,created_by_lbl,operator\n" +
            "_lbl,location_lbl,sublocation_lbl,category_lbl,subcategory_lbl,smartcard_lbl,stb_lbl,status_lbl,reply\n" +
            "_lbl,name_lbl")
    Call<JsonObject> getOpenComplaintlist(@Header("Authorization") String token,@Query("page") int page,@Query("per-page") int perPage);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/complaint?expand=priority_lbl,created_by_lbl,operator_lbl,location_lbl,sublocation_lbl,category_lbl,subcategory_lbl,smartcard_lbl,stb_lbl,status_lbl,reply_lbl,name_lbl")
    Call<JsonObject> getCMSlist(@Header("Authorization") String token,@Query("filter[account_id]") String accountID,@Query("page") int page,@Query("per-page") int perPage);


    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/complaint?filter[status]=3&expand=priority_lbl,created_by_lbl,operator_lbl,location_lbl,sublocation_lbl,category_lbl,subcategory_lbl,smartcard_lbl,stb_lbl,status_lbl,reply_lbl,name_lbl&pa\n" +
            "ge=1&per-page=50")
    Call<JsonObject> getClosedComplaintlist(@Header("Authorization") String token,@Query("page") int page,@Query("per-page") int perPage);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/account?filter[status]=1&filter[days_left]=7&expand=bouqueD,bouque_lbl,status_lbl,boxtype_lbl,brand_lbl,cas_lbl,operator_lbl,location_lbl,sublocation_lbl,customer_name,created_by_lbl&filter[renewable]=1&filter[notlocked]=1&sort=deactivation_date")
    Call<JsonObject> get7DaysExpireAccountList(@Header("Authorization") String token, @Query("page") int page,@Query("per-page") int perPage);
//    http://api.jprnetworksms.net/v1/account-bouque?filter%5BFRM_deactivation_date%5D=2019-2-24%2023:59:59&filter%5BTO_deactivation_date%5D=2019-3-03%2000:00:00&expand=stbno_lbl,smartcardno_lbl,customer_id_lbl,customer_name_lbl,operator_lbl,created_by_lbl,brand_lbl,mobile_%20no_lbl%E2%80%8B&page=1&per-page=50


    //bulk bouquetswiselists
    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/account?filter[status]=1&filter[days_left]=7&expand=bouqueD,bouque_lbl,status_lbl,boxtype_lbl,brand_lbl,cas_lbl,operator_lbl,location_lbl,sublocation_lbl,customer_name,created_by_lbl&filter[notlocked]=1&sort=deactivation_date")
    Call<JsonObject> get7DaysExpireListBouqoutesWise(@Header("Authorization") String token, @Query("filter[bouque_id]") String bouqid,@Query("page") int page,@Query("per-page") int perPage);


    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/subscriber?expand=status_lbl,connection_lbl,operator_lbl,account_list,&page=1&per-page=50&fields=id,customer_id,name,mobile_no")
    Call<JsonObject> getCustomerDetail(@Header("Authorization") String token,@Query("filter[customer_id]") String customerID,@Query("filter[smartcardno]") String smartcardNo,@Query("filter[stbno]") String stnno,@Query("filter[mobile_no]") String mobile);

//    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
//    @GET("v1/subscriber?expand=status_lbl,connection_lbl,account_list,&page=1&per-page=50&fields=id,customer_id,name,mobile_no")
//    Call<JsonObject> Search(@Header("Authorization") String token,@Query("filter["+Params.SEARCH_FILTER_KEY+"]") String keyValue);
//    Call<JsonObject> getCustomerDetail(@Header("Authorization") String token,@Query("filter[customer_id]") String customerID,@Query("filter[smartcardno]") String smartcardNo,@Query("filter[stbno]") String stnno,@Query("filter[mobile_no]") String mobile);



    @Headers({"Content-Type:application/json"})
    @GET("v1/bouque/list?fields=id,name,description,rate,type&expand=type_lbl,boxtype_lbl,mrp")
    Call<JsonObject> getRenewals(@Header("Authorization") String token, @Query("filter[account_id]") String id);


    //renewal other than bulk operaiton

    @Headers({"Content-Type:application/json", "Authkey:"+APIURL.AUTH_KEY})
    @POST("v1/recharge-period/{id}/appmultiview")
    Call<JsonObject> getRechargeRenewal(@Header("Authorization") String token, @Path("id") String id, @Body RechargePayload data);

    //renewal for bulk operation

    @Headers({"Content-Type:application/json", "Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/recharge-period/0/appmultiview")
    Call<JsonObject> getRechargePeriodBulkAccount(@Header("Authorization") String token);

    @Headers({"Content-Type:application/json", "Authkey:"+APIURL.AUTH_KEY})
    @GET("v1/recharge-period/{id}/appmultiview")
    Call<JsonObject> getRechargePeriodBulkBouquets(@Header("Authorization") String token,@Path("id") String id);



    // http://120.88.185.39:82/v1/account-bouque/0
   @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
   @PUT("v1/account-bouque/0")
   Call<JsonObject> bulkBouqueRenewal(@Header("Authorization") String token,@Body BulkBouqoutesRenewalPayload bulkBouqoutesRenewalPayload);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @PUT("v1/account/0/resume")
    Call<JsonObject> bulkResume(@Header("Authorization") String token,@Body ActionPayload actionPayload);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @PUT("v1/account/0/suspend")
    Call<JsonObject> bulkSuspend(@Header("Authorization") String token,@Body ActionPayload actionPayload);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @PUT("v1/complaint/{id}")
    Call<JsonObject> closeComplaint(@Header("Authorization") String token,@Path("id") String id,@Body JsonObject data);


    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @PUT("v1/complaint/{id}")
    Call<JsonObject> closeComplaint(@Header("Authorization") String token,@Path("id") String id,@Body ComplainPayload data);

    @Headers({"Content-Type:application/json","Authkey:"+APIURL.AUTH_KEY})
    @PUT("v1/complaint/{id}")
    Call<JsonObject> replyComplain(@Header("Authorization") String token,@Path("id") String id,@Body ComplainPayload data);

    @Headers({"Content-Type:application/json"})
    @GET("v1/bouque/{id}?fields=id,name&expand=channels")
    Call<JsonObject> getChannelList(@Header("Authorization") String token, @Path("id") String id);


    @Headers({"Content-Type:application/json"})
    @POST("v1/account-bouque")
    Call<JsonObject> saveBouquets(@Header("Authorization") String token,@Body SavePayload data);


//    //save the bulk account operaoin
//    http://120.88.185.39:82/v1/account/0/renew

    @Headers({"Content-Type:application/json"})
    @POST("v1/account/0/renew")
    Call<JsonObject> renewBulkAccounts(@Header("Authorization") String token,@Body BulkAccountRenewalPayload data);

    @Headers({"Content-Type:application/json"})
    @PUT("v1/account/{id}/suspend")
    Call<JsonObject> suspend(@Header("Authorization") String token, @Path("id") String id,@Body Remarks remarks);
//
//    @Headers({"Content-Type:application/json"})
//    @PUT("v1/account/{id}/suspend")
//    Call<JsonObject> singleSuspend(@Header("Authorization") String token, @Path("id") String id,@Body Remarks remarks);

    @Headers({"Content-Type:application/json"})
    @PUT("v1/account/{id}/resume")
    Call<JsonObject> singleResume(@Header("Authorization") String token, @Path("id") String id,@Body Remarks remarks);




    @Headers({"Content-Type:application/json"})
    @GET("v1/account-bouque?expand=boxtype_lbl,type_lbl,status_lbl,refundAble&sort=created_at")
    Call<JsonObject> getAllotedBoques(@Header("Authorization") String token,@Query("filter[account_id]") String id);


    @Headers({"Content-Type:application/json"})
    @POST("v1/account-bouque/0/remove")
    Call<JsonObject> removeSingleBouquets(@Header("Authorization") String token,@Body RemovebouqePayload data);






    @Headers({"Content-Type:application/json"})
    @GET("v1/bouque/list?fields=id,name,description,rate&expand=type_lbl,boxtype_lbl&notfilter[type]=1")
    Call<JsonObject> getAddoList(@Header("Authorization") String token, @Query("notfilter[account_id]") String accid, @Query("filter[brand_id]") String brandId);
   // v1/bouque/list?fields=id,name,description,rate&expand=type_lbl,boxtype_lbl&notfilter[account_id]=3059&notfilter[type]=1&filter[brand_id]=2



    @Headers({"Content-Type:application/json"})
    @POST("v1/complaint")
    Call<JsonObject> postComplain(@Header("Authorization") String token, @Body CreateComplainPayload body);


    @Headers({"Content-Type:application/json"})
    @GET("v1/subscriber/profile/{id}")
    Call<JsonObject> getCustomerProfile(@Header("Authorization") String token,  @Path("id") String id);

    @Headers({"Content-Type:application/json"})
    @GET("v1/complaint-category/list?filter[status]=1&fields=id,name")
    Call<JsonObject> getMainComplainCategory(@Header("Authorization") String token);



    @Headers({"Content-Type:application/json"})
    @GET("v1/complaint-subcategory/list?filter[status]=1&fields=id,name")
    Call<JsonObject> getSubComplainCategory(@Header("Authorization") String token,@Query("filter[category_id]") String id);


    @Headers({"Content-Type:application/json"})
    @PUT("v1/subscriber/{id}")
    Call<JsonObject> updateProfile(@Header("Authorization") String token, @Path("id") String id,@Body EditProfielPayload customerProfileData);

    @Headers({"Content-Type:application/json"})
    @POST("v1/account/{id}/refresh")
    Call<JsonObject> refreshCommand(@Header("Authorization") String token, @Path("id") String id);


    @Headers({"Content-Type:application/json"})
    @GET("v1/account-bouque?filter[bouque_type]=1&expand=boxtype_lbl,type_lbl,status_lbl,refundAble&sort=created_at")
    Call<JsonObject> getActiveBasePlan(@Header("Authorization") String token, @Query("filter[account_id]") String id);


    @Headers({"Content-Type:application/json"})
    @GET("v1/bouque/list?fields=id,name&filter[type]=1")
    Call<JsonObject> getNewBasePlan(@Header("Authorization") String token, @Query("notfilter[id]") int accid);

    @Headers({"Content-Type:application/json"})
    @POST("v1/account/0/replace-bouque")
    Call<JsonObject> changeBasePlan(@Header("Authorization") String token,@Body SavePayloadForPackChange data);



    //Bulk Renewal Options :

    @Headers({"Content-Type:application/json"})
    @GET("v1/account-bouque/bouque-count")
    Call<JsonObject> getExpiredSevenDaysBouquets(@Header("Authorization") String token);

    @Headers({"Content-Type:application/json"})
    @GET("v1/bouque/list?fields=id,name&status=1")
    Call<JsonObject> getBulkBouqueList(@Header("Authorization") String token,@Query("filter[type]") int bouquetype);

    @Headers({"Content-Type:application/json"})
    @GET("v1/bouque/list?fields=id,name&filter[type]=1")
    Call<JsonObject> getBulkBouqueList(@Header("Authorization") String token);

    @Headers({"Content-Type:application/json"})
    @GET("v1/bouque/list?fields=id,name&filter[type]=1")
    Call<JsonObject> getReplacementBulkBouqueList(@Header("Authorization") String token,@Query("notfilter[id]") int bouqueid);


    @Headers({"Content-Type:application/json"})
    @GET("v1/account?filter[status]=1&expand=bouqueD,status_lbl,boxtype_lbl,brand_lbl,cas_lbl,operator_lbl,location_lbl,sublocation_lbl,customer_name,created_by_lbl&filter[notlocked]=1&sort=deactivation_date")
    Call<JsonObject> getBulkSubscriberList(@Header("Authorization") String token,@Query("filter[bouque_id]") int bouqueid,@Query("page") int page,@Query("per-page") int perPage);

    @Headers({"Content-Type:application/json"})
    @GET("v1/account?expand=bouqueD,status_lbl,boxtype_lbl,brand_lbl,cas_lbl,operator_lbl,location_lbl,sublocation_lbl,customer_name,created_by_lbl&filter[status]=30")
    Call<JsonObject> getBulkAssignmentSubscriberList(@Header("Authorization") String token,@Query("notfilter[bouque_id]") int bouqueid,@Query("page") int page,@Query("per-page") int perPage);

    @Headers({"Content-Type:application/json"})
    @GET("v1/account?filter[status]=1&expand=bouqueD,status_lbl,boxtype_lbl,brand_lbl,cas_lbl,operator_lbl,location_lbl,sublocation_lbl,customer_name,created_by_lbl&filter[notlocked]=1&sort=deactivation_date")
    Call<JsonObject> getBulkSubscriberList(@Header("Authorization") String token,@Query("filter[bouque_id]") int bouqueid,@Query("filter[renewable]") int renewableType,@Query("page") int page,@Query("per-page") int perPage);

    @Headers({"Content-Type:application/json"})
    @GET("v1/recharge-period/{id}/appmultiview")
    Call<JsonObject> getBulkRechargePeriod(@Header("Authorization") String token,@Path("id") int bouqueid);

    @Headers({"Content-Type:application/json"})
    @POST("v1/account-bouque/0/remove")
    Call<JsonObject> bulkBouquetRemove(@Header("Authorization") String token,@Body BulkRemoveBouqetPayload bulkRemoveBouqetPayload);


    @Headers({"Content-Type:application/json"})
    @POST("v1/account/0/replace-bouque")
    Call<JsonObject> bulkreplaceBasePlan(@Header("Authorization") String token,@Body SavePayloadForBulkPackChange data);

    @Headers({"Content-Type:application/json"})
    @POST("v1/account-bouque")
    Call<JsonObject> bulkAssign(@Header("Authorization") String token,@Body SavePayloadForBulkAssignment data);

    @Headers({"Content-Type:application/json"})
    @GET("v1/account-bouque?expand=created_by_lbl,boxtype_lbl,type_lbl,status_lbl,refundAble&sort=created_at")
    Call<JsonObject> getMyPlans(@Header("Authorization") String token,@Query("filter[account_id]") int accountid,@Query("page") int page,@Query("per-page") int perPage);


    //http://120.88.185.39:82//v1/subscriber/<suscriber_id from 1.1>


    //http://120.88.185.39:82/v1/complaint-subcategory/list?filter[status]=1&filter[category_id]=<id from 6.4>&fields=id,name




/*
if subscriberType == SubscriberType.active{
           actionBtn.setTitle("SUSPEND", for: .normal)
           self.navigationItem.title = "Active Subscribers"
           apiUrl = kActiveSubscriberList
       }else if subscriberType == SubscriberType.inactive{
           self.navigationItem.title = "InActive Subscribers"
           apiUrl = kInactiveSubscriberList
       }else if subscriberType == SubscriberType.expired{
           self.navigationItem.title = "Expired Subscribers"
           apiUrl = kExpiredSubscriberList
       }else if subscriberType == SubscriberType.suspended{
           actionBtn.setTitle("RESUME ACCOUNT", for: .normal)
           self.navigationItem.title = "Suspended Subscribers"
           apiUrl = kSuspendedSubscriberList
       }
 */



}