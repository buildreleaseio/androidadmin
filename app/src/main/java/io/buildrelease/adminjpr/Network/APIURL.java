package io.buildrelease.adminjpr.Network;

public class APIURL {

   // public static final String BASE_URL = "http://smssbbvision.com:82";

    //public static final String PAYMENT_GATEWAY = "smssbbvision.com";



    public static  String USER_TYPE = "jpr";
    //package naem : io.buildrelease.adminradiant
   // public static String USER_TYPE = "jpr";
    //jpr packagename : io.buildrelease.adminjpr

    //jpr
//    public static final String PAYMENT_GATEWAY = "jprnetworksms.net";
//    public static final String BASE_URL = "http://api.jprnetworksms.net";
//    public static final String BASE_URL_PAYMENT = "http://jprnetworksms.net";
//    public static final String AUTH_KEY = "ivt5y01iwq75sysm7ol7";



    //radiatn
    public static final String PAYMENT_GATEWAY = "sms-radiant.com";
    public static final String BASE_URL = "http://sms-radiant.com:82";
    public static final String BASE_URL_PAYMENT = "http://sms-radiant.com";
    public static final String AUTH_KEY = "ivt5y01iwq75sysm7ol7";


    public  static String getPAYMENT_GATEWAY()
    {
        switch (USER_TYPE)
        {
            case "radiant" :
                return "sms-radiant.com";
            case "jpr" :
                return "jprnetworksms.net";

        }

        return null;
    }


    public static String getBASE_URL()
    {
        switch (USER_TYPE)
        {
            case "radiant" :
                return "http://sms-radiant.com:82";
            case "jpr" :
                return "http://api.jprnetworksms.net";

        }

        return null;
    }


    public static String getBASE_URL_PAYMENT()
    {
        switch (USER_TYPE)
        {
            case "radiant" :
                return "https://sms-radiant.com";
            case "jpr" :
                return "http://jprnetworksms.net";

        }

        return null;
    }
}
