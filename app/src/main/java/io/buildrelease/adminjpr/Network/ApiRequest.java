package io.buildrelease.adminjpr.Network;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONObject;

import java.util.Objects;

import io.buildrelease.adminjpr.Utility.CommonUtils;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiRequest {

    String TAG =  getClass().getSimpleName();
    Context context = null;

    public ApiRequest(Context context) {
        this.context = context;
    }
//    public ApiRequest() {
//    }

    APIInterface apiInterface;

    public void request(Call<JsonObject> call, final FetchDataListener listener)
    {
        apiInterface = APIClient.getClient().create(APIInterface.class);
        listener.onFetchStart();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if(response!=null && response.isSuccessful()) {
                    Headers headers = response.headers();
                    String count =   headers.get("X-Pagination-Total-Count");
                   Log.d("Headers Test",headers.toString());
                   Log.d("Page Count",count+"");
//                    for(Header header : ) {
//                        Log.d(TAG, header.getName() + " " + header.getValue());
//                    }
                    if(response.body()!=null) {
                        Log.d(TAG, "onResponse: " + response.code() + "  " + response.body().toString());
                        listener.onFetchComplete(response.body().toString());
                    }
                }else
                {
                    if(response.code()==401)
                    {
                        if(context!=null) {
                            CommonUtils.clearPrefs(context);
                        }
                        else
                        {
                            Log.d(TAG, "onResponse:");
                        }
                        listener.onFetchFailure("Session Expired");
                    }

                    JsonParser parser = new JsonParser();
                    JsonElement mJson = null;
                    String errorMsg = "Network call failed";
                    try {
                        mJson = parser.parse(Objects.requireNonNull(response.errorBody()).string());
                        Log.d(TAG, "onResponse: "+mJson.toString());
                        JSONObject jsonObject =new JSONObject(mJson.toString());
                        ErrorService errorService =new ErrorService(jsonObject,context);
                        errorMsg = errorService.checkError();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    Log.d(TAG, "onResponseerror: "+response.raw());
//                    ErrorService errorService =new ErrorService()
                    listener.onFetchFailure(errorMsg);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onFetchFailure(t.toString());
            }
        });


    }

    public void request(Call<JsonObject> call, final FetchDataPaginatedListener listener)
    {
        apiInterface = APIClient.getClient().create(APIInterface.class);
        listener.onFetchStart();
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                if(response!=null && response.isSuccessful()) {
                    Headers headers = response.headers();
                    String count =   headers.get("X-Pagination-Total-Count");
                    Log.d("Headers Test",headers.toString());
                    Log.d("Page Count",count+"");
//                    for(Header header : ) {
//                        Log.d(TAG, header.getName() + " " + header.getValue());
//                    }
                    if(response.body()!=null) {
                        Log.d(TAG, "onResponse: " + response.code() + "  " + response.body().toString());
                        listener.onFetchComplete(response.body().toString(), count);
                    }
                }else
                {
                    if(response.code()==401)
                    {
                        if(context!=null) {
                            CommonUtils.clearPrefs(context);
                        }
                        listener.onFetchFailure("Session Expired");
                    }
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = null;
                    String errorMsg = "Network call failed";
                    try {
                        mJson = parser.parse(Objects.requireNonNull(response.errorBody()).string());
                        Log.d(TAG, "onResponse: "+mJson.toString());
                        JSONObject jsonObject =new JSONObject(mJson.toString());
                        ErrorService errorService =new ErrorService(jsonObject,context);
                        errorMsg = errorService.checkError();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    Log.d(TAG, "onResponseerror: "+response.raw());
//                    ErrorService errorService =new ErrorService()
                    listener.onFetchFailure(errorMsg);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onFetchFailure(t.toString());
            }
        });


    }
}
