package io.buildrelease.adminjpr.Network;

import android.content.Context;
import android.util.Log;
import android.view.View;

import org.json.JSONObject;

import io.buildrelease.adminjpr.Utility.CommonUtils;

public class ErrorService {
    private final JSONObject obj;
    private final Context context;
    View view = null;

    /*For checkErrorOld
        public ErrorService(JSONObject data,Context context){
            this.obj=data;
            this.context=context;
        }*/
    public ErrorService(JSONObject data, Context context, View view) {
        this.obj = data;
        this.context = context;
        this.view = view;
    }

    public ErrorService(JSONObject data, Context context) {
        this.obj = data;
        this.context = context;
    }

    public String checkError() {
        String errorMsg = "";
        try {
            if (obj.has("status")) {
                String status = obj.getString("status");
                Log.d("", "checkError: " + status);
                switch (status) {
                    case "401":
                        //initiate logout
                        CommonUtils.clearPrefs(context);
                        break;
                    default:
                        errorMsg = CommonUtils.getResponseMessage(obj.toString());
//                        if (obj.has("data")) {
//                            JSONObject data = obj.getJSONObject("data");
//                            if (data.has("message")) {
//                                Object message = data.get("message");
//                                if(message instanceof String){
//                                    errorMsg = (String)message;
//                                }else{
//                                    JSONObject jsonObjectMessage = (JSONObject)message ;
//                                    if(jsonObjectMessage.has("password")){
//                                        JSONArray jsonArray = jsonObjectMessage.getJSONArray("password");
//                                        errorMsg = jsonArray.getString(0);
//                                    }
//                                }
//
//                            }
//                        }
                        break;

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return errorMsg;
    }
}
