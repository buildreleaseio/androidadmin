package io.buildrelease.adminjpr.Network;


public interface FetchDataListener {

        void onFetchComplete(String data);

        void onFetchFailure(String msg);

        void onFetchStart();
    }


