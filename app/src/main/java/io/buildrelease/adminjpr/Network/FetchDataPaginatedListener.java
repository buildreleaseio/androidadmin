package io.buildrelease.adminjpr.Network;


public interface FetchDataPaginatedListener {

        void onFetchComplete(String data, String pageCount);

        void onFetchFailure(String msg);

        void onFetchStart();
    }


