package io.buildrelease.adminjpr.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import io.buildrelease.adminjpr.Activity.LoginActivity;
import io.buildrelease.adminjpr.R;

public class CommonUtils {
    public  final static String pending = "pending" ,closed = "closed",invalid = "invalid",open = "open",active ="active",inactive = "inactive";

    public static boolean isNonEmpty(String data)
    {
        if(!data.isEmpty() && data!=null && !data.trim().equalsIgnoreCase(""))
        {
            return true;
        }
        return  false;
    }


    public static void hideKeyboardOnTouchOutside(View view, final Activity activity) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                public boolean onTouch(View v, MotionEvent event) {
                    if(activity!=null)
                        hideSoftKeyboard(activity);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                hideKeyboardOnTouchOutside(innerView,activity);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(inputMethodManager).hideSoftInputFromWindow(
                Objects.requireNonNull(activity.getCurrentFocus()).getWindowToken(), 0);
    }


    public static void showToasty(Context context, String msg, String type) {
        Toasty.Config.getInstance()
                .setErrorColor(context.getResources().getColor(R.color.colorRed))
                .setInfoColor(context.getResources().getColor(R.color.grey_text))
                .setSuccessColor(context.getResources().getColor(R.color.colorGreen))
                .setWarningColor(context.getResources().getColor(R.color.order_history_processed))
//                .setToastTypeface(Typeface.createFromAsset(context.getAssets(),"font/muli_regular.ttf"))
                .apply();
        type = type.toLowerCase();
        if(context!=null) {
            switch (type) {
                case "info":
                    try {
                        Toasty.info(context, msg, Toast.LENGTH_SHORT, true).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case "error":
                    try {
                        Toasty.error(context, msg, Toast.LENGTH_SHORT, true).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case "success":
                    try {
                        Toasty.success(context, msg, Toast.LENGTH_SHORT, true).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case "warning":
                    try {
                        Toasty.warning(context, msg, Toast.LENGTH_SHORT, true).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    }
                    break;
                default:
//                    Toasty.info(context,msg,Toast.LENGTH_SHORT,true).show();
                    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                    break;

            }
        }
    }


    public static String getCurrentDateTime(){
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
//        2018-12-31 00:00:00
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Log.d("???", "getCurrentDateTime "+df.format(c));
        return df.format(c);
    }
    public static String getCurrentDate(){
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
//        2018-12-31 00:00:00
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Log.d("???", "getCurrentDate: "+df.format(c));
        return df.format(c);
    }
    public static String getCurrentDate(int additonalDays,String type){
        Date c = Calendar.getInstance().getTime();
        if(type.equalsIgnoreCase("future")){
            Calendar cal = Calendar.getInstance();
            cal.setTime(c);
            cal.add(Calendar.DATE, additonalDays);
            c = cal.getTime();
        }else{
            Calendar cal = Calendar.getInstance();
            cal.setTime(c);
            cal.add(Calendar.DATE, -additonalDays);
            c = cal.getTime();
        }
        System.out.println("Current time => " + c);
//        2018-12-31 00:00:00
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c);
    }
    public static String getCurrentDateTime(int additonalDays,String type){
        Date c = Calendar.getInstance().getTime();
        if(type.equalsIgnoreCase("future")){
            Calendar cal = Calendar.getInstance();
            cal.setTime(c);
            cal.add(Calendar.DATE, additonalDays);
            c = cal.getTime();
        }else{
            Calendar cal = Calendar.getInstance();
            cal.setTime(c);
            cal.add(Calendar.DATE, -additonalDays);
            c = cal.getTime();
        }
        System.out.println("Current time => " + c);
//        2018-12-31 00:00:00
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Log.d("???", "getCurrentDate:++ "+df.format(c));
        return df.format(c);
    }

    public static void displayImageResource(int id, Context context, ImageView imageView){
        Glide.with(context)
                .fromResource()
                .asBitmap()
                .error(R.color.colorWhite70)
                .placeholder(R.color.colorWhite70)
                .load(id)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);
    }


    public static void logout(Context context){
          UtilPref.logOut(context);
        CommonUtils.showToasty(context,"Logged out",Params.TOASTY_SUCCESS);
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    public static void clearPrefs(Context context){
//        UtilPref.setUserName(context,"");
//        UtilPref.setPassword(context,"");
        UtilPref.clearOtherPref(context);
        CommonUtils.showToasty(context,"Logged out",Params.TOASTY_SUCCESS);
        Intent intent = new Intent(context, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static boolean checkNetStatusResult(final Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Objects.requireNonNull(cm).getActiveNetworkInfo() == null) {
            return false;
        }
        return true;
    }

    public static String dateFormat(String input) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = simpleDateFormat.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
            return input;
        }
        DateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = format2.format(date);
        return dateString;
    }

    public static String fullDateFormat(String input) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(input);
        } catch (ParseException e) {
            e.printStackTrace();
            return input;
        }
        DateFormat format2 = new SimpleDateFormat("dd-MM-yyyy");
        String dateString = format2.format(date);
        return dateString;
    }

    
    public static String formatAmountComma(String amount)
    {
        Format format = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
        Log.d("comma", "formatAmountComma: "+format.format(new BigDecimal(amount)));
        return (format.format(new BigDecimal(amount))).toString();
    }

    public static  int statusColor(String oldstatus)
    {
        String newstatus = oldstatus.toLowerCase().trim();
        switch (newstatus)
        {
            case open:
                return R.color.label_red_text;
            case inactive:
                return R.color.label_red_text;
            case pending:
                return R.color.label_yellow_text;
            case closed:
                return R.color.label_green_text;
            case active:
                return R.color.label_green_text;
            case invalid :
                return R.color.label_blue_text;
        }

        return R.color.label_blue_text;
    }


    public static String getResponseMessage(String json)
    {

        String message = null;
        JSONObject dataObject = null;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);


            if(jsonObject.has("data"))
            {
                dataObject =  jsonObject.getJSONObject("data");
            }

            if(dataObject!=null)
            {
                if(dataObject.has("msg"))
                {
                    message = dataObject.getString("msg");
                    return  message;
                }
                else if(dataObject.has("msg_jobs"))
                {
                    message = dataObject.getString("msg_jobs");
                    return  message;
                }else if(dataObject.has("message")) {
                    Object messageString = dataObject.get("message");
                    if (messageString instanceof String) {
                        message = (String) messageString;
                        return message;
                    } else if (messageString instanceof JSONObject) {
                        JSONObject messageObject = dataObject.getJSONObject("message");
                        if (messageObject.has("msg")) {
                            message = messageObject.getString("msg");
                            return message;
                        } else if (messageObject.has("d_message")) {
                            JSONObject d_messageObject = messageObject.getJSONObject("d_message");
                            Iterator<String> keys = d_messageObject.keys();
                            Log.d("d_message keys", "getResponseMessage: "+keys);
                            while (keys.hasNext()) {
                                String keyValue = (String) keys.next();
                                message = d_messageObject.getString(keyValue);
                                return message;

                            }
                        } else if (messageObject.has("password")) {
                            JSONArray jsonArray = messageObject.getJSONArray("password");
                            message = jsonArray.getString(0);
                            return message;
                        }
                    }
                }else{
                    Iterator<String> keys= dataObject.keys();
                    while (keys.hasNext())
                    {
                        String keyValue = (String)keys.next();
                        message = dataObject.getString(keyValue);
                        return  message;

                    }
                }
            }

            return message;
        } catch (JSONException e) {
            e.printStackTrace();
            return  null;
        }


    }

    public static void showSuccessMessage(Context context, String data,String defaultMessage)
    {
        String message =  CommonUtils.getResponseMessage(data);
        if(message!=null && !message.equalsIgnoreCase("null") && !message.isEmpty())
        {
            CommonUtils.showToasty(context,message, Params.TOASTY_SUCCESS);
        }else
        {
            CommonUtils.showToasty(context,defaultMessage, Params.TOASTY_SUCCESS);
        }
    }

    public static  void showFailureMessage(Context context, String message,String defaultMessage)
    {
        if(message!=null && !message.isEmpty() && !message.equalsIgnoreCase("null"))
        {
            CommonUtils.showToasty(context,message, Params.TOASTY_ERROR);
        }else
        {
            CommonUtils.showToasty(context,defaultMessage, Params.TOASTY_ERROR);
        }
    }

}
