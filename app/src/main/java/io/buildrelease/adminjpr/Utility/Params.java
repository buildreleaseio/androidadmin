package io.buildrelease.adminjpr.Utility;

public class Params {


    public static final String TOASTY_SUCCESS = "success";
    public static final String TOASTY_WARN = "warning";
    public static final String TOASTY_ERROR = "error";
    public static final String TOASTY_INFO = "info";
    public static final String TAG = "XXXXXXXXXX";

    public static final String LIST_ACTIVE = "LIST_ACTIVE";
    public static final String LIST_INACTIVE = "LIST_INACTIVE";
    public static final String LIST_EXPIREDTODAY = "LIST_EXPIREDTODAY";
    public static final String LIST_EXPIRED7DAYS = "LIST_EXPIRED7DAYS";
    public static final String LIST_SUSPENDED = "LIST_SUSPENDED";
    public static final String LIST_OPEN_COMPLAINTS = "LIST_OPEN_COMPLAINTS";
    public static final String LIST_CLOSED_COMPLAINTS = "LIST_CLOSED_COMPLAINTS";
    public static String SEARCH_FILTER_KEY = "";

    public static final int CAMERA_REQUEST_EDIT_PROFILE = 111;
    public static final int GALLERY_PICTURE_EDIT_PROFILE = 112;
    public static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 117;
    public static final String CHANGE_BASE_PLAN  = "CHANGE_BASE_PLAN";
    public static final String RENEWAL  = "RENEWAL";
    public static final String BULKACCOUNTRENEWAL  = "BULKACCOUNTRENEWAL";
    public static final String BULKBOUQUETSRENEWAL = "BULKBOUQUETSRENEWAL";

    public static final String BULKBOUQUETREMOVAL = "BULKBOUQUETREMOVAL";
    public static final String BULKBOUQUETSREPLACEMENT = "BULKBOUQUETSREPLACEMENT";
    public static final String BULKBOUQUETSASSIGNMENT = "BULKBOUQUETSASSIGNMENT";


}
