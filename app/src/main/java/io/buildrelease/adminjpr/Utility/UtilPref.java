package io.buildrelease.adminjpr.Utility;

import android.content.Context;
import android.content.SharedPreferences;

public class UtilPref {

    static final String SHARED_PREF_KEY = "PREF";
    static final String GLOBAL_VC = "GLOBAL_VC";
    static final String USERNAME = "USERNAME";
    static final String ACCOUNT_ID = "ACCOUNT_ID";
    static final String NAME = "NAME";
    static final String EMAIL = "EMAIL";
    static final String PASSWORD = "PASSWORD";
    static final String AUTH = "AUTH";
    static final String SUB_ID = "SUB_ID";
    static final String OP_ID = "OP_ID";
    static final String BAL = "BALANCE";




    static public SharedPreferences getSharedPref(Context context) {
        return context.getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
    }

    static public void setEmail(Context context, String email) {
        getSharedPref(context).edit().putString(EMAIL, email).apply();
    }

    static public String getEmail(Context context) {
        return getSharedPref(context).getString(EMAIL, null);
    }

    static public void setPassword(Context context, String password) {
        getSharedPref(context).edit().putString(PASSWORD, password).apply();
    }

    static public String getPassword(Context context) {
        return getSharedPref(context).getString(PASSWORD, null);
    }

    static public void setAuth(Context context, String auth) {
        getSharedPref(context).edit().putString(AUTH, auth).apply();
    }

    static public String getAuth(Context context) {
        return getSharedPref(context).getString(AUTH, "");
    }


    static public void setSubID(Context context, String subid) {
        getSharedPref(context).edit().putString(SUB_ID, subid).apply();
    }

    static public String getSubId(Context context) {
        return getSharedPref(context).getString(SUB_ID, "");
    }
static public void setOperatorID(Context context, String subid) {
        getSharedPref(context).edit().putString(OP_ID, subid).apply();
    }

    static public String getOperatorID(Context context) {
        return getSharedPref(context).getString(OP_ID, "");
    }


    static public void setName(Context context, String name) {
        getSharedPref(context).edit().putString(NAME, name).apply();
    }

    static public String getName(Context context) {
        return getSharedPref(context).getString(NAME, "");
    }

    static public void setUserName(Context context, String username) {
        getSharedPref(context).edit().putString(USERNAME, username).apply();
    }

    static public String getUserName(Context context) {
        return getSharedPref(context).getString(USERNAME, "");
    }


    static public void setVC(Context context, String vc) {
        getSharedPref(context).edit().putString(GLOBAL_VC, vc).apply();
    }

    static public String getVC(Context context) {
        return getSharedPref(context).getString(GLOBAL_VC, null);
    }

    static public void setACCID(Context context, String id) {
        getSharedPref(context).edit().putString(ACCOUNT_ID, id).apply();
    }

    static public String getACCID(Context context) {
        return getSharedPref(context).getString(ACCOUNT_ID, null);
    }

    static public void setBalance(Context context, float balance) {
        getSharedPref(context).edit().putFloat(BAL, balance).apply();
    }

    static public String getBalance(Context context) {
        return getSharedPref(context).getString(BAL, null);
    }


    static public void logOut(Context context)
    {
        SharedPreferences settings = context.getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
        settings.edit().clear().commit();
    }



    static public void clearOtherPref(Context context)
    {

        SharedPreferences mySPrefs = getSharedPref(context);
        SharedPreferences.Editor editor = mySPrefs.edit();
        editor.remove(EMAIL).remove(NAME).remove(BAL).remove(ACCOUNT_ID).remove(GLOBAL_VC).remove(OP_ID).remove(SUB_ID).remove(AUTH).apply();

    }

    static public void removeKey(Context context,String key)
    {
        SharedPreferences mySPrefs = getSharedPref(context);
        SharedPreferences.Editor editor = mySPrefs.edit();
        editor.remove(key);
        editor.apply();
    }

}
