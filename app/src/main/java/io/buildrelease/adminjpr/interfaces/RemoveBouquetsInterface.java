package io.buildrelease.adminjpr.interfaces;

public interface RemoveBouquetsInterface {

    public void remove(String subs_tran_ids);
}
