package io.buildrelease.adminjpr.interfaces;

import java.util.List;

public interface SelectedAddonInterface {
    public void selected(List<String> dataList);
}
