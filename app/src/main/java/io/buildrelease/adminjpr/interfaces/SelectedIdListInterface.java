package io.buildrelease.adminjpr.interfaces;

import java.util.List;

public interface SelectedIdListInterface {

    public void selectedIds(List<String> ids);
}
