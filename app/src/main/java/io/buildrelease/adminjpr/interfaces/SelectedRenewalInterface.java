package io.buildrelease.adminjpr.interfaces;

import java.util.List;

public interface SelectedRenewalInterface {
    public void selected(List<String> dataList);
}
